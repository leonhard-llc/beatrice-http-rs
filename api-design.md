# beatrice_http API Design

## Streaming Response Body
beatrice_http must be "easy to use and learn."
I think async fn request handlers are the easiest way to write a server.

Example:

```rust
struct State {}

impl State {
    pub async fn next_update(&self) -> String {
        tokio::time::sleep(Duration::from_secs(1)).await;
        chrono::Utc::now().to_rfc3339()
    }
}

async fn handle_request(state: State, req: HttpRequest, writer: &mut HttpWriter)
-> Result<(),HttpError> {
    req.check_method(HttpMethod::GET, writer).await?;
    req.check_path("/stream_updates", writer).await?;
    writer.start_chunked_response(HttpStatus::Ok200).await?;
    loop {
        writer.send_chunk(state.next_update().as_bytes()).await?;
    }
}

async fn async_main() {
    beatrice_http::run_server(
        443, "cert.pem", State{}, handle_request).await.unwrap();
}
```

But we get an error passing `handle_request` to `beatrice_http::run_server`.
This is because Rust 1.48 cannot pass a generic async fn to a function.

Example:
```rust
async fn apply<Fut>(f: fn(&mut u8) -> Fut, mut value: &mut u8)
where
    Fut: std::future::Future<Output = ()>,
{
    for _ in 0..10u8 {
        f(&mut value).await;
    }
}

async fn add1<'r>(value: &'r mut u8) {
    //                          -----^
    // the `Output` of this `async fn`'s found opaque type
    *value += 1;
}

async fn async_main() {
    let mut value = 0u8;
    apply(add1, &mut value).await;
    //    ^^^^ one type is more general than the other
    // note: expected fn pointer `for<'r> fn(&'r mut u8) -> impl Future`
    //          found fn pointer `for<'r> fn(&'r mut u8) -> impl Future`
    // error[E0308]: mismatched types
}
```

This happens because async functions are syntactic sugar.
The compiler transforms each async fn into a non-async fn and a unique
struct that implements
[`std::future::Future`](https://doc.rust-lang.org/stable/std/future/trait.Future.html).
The struct contains the fn's arguments and their lifetimes.
The struct's [`poll`](https://doc.rust-lang.org/stable/std/future/trait.Future.html)
function executes the original async fn's body.

The compiler does closures the same way.
See [Closure: Magic Functions](https://rustyyato.github.io/rust/syntactic/sugar/2019/01/17/Closures-Magic-Functions.html).

When you compile this:
```rust
async fn add1(value: &mut u8) {
    *value += 1;
}
```

The compiler generates code like this:
```rust
struct Add1Future<'a>(&'a mut u8);

impl<'a> std::future::Future for Add1Future<'a> {
    type Output = ();

    fn poll(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>
    ) -> std::task::Poll<Self::Output> {
        let mut value: &mut u8 = self.get_mut().0;
        *value += 1;
        std::task::Poll::Ready(())
    }
}

fn add1<'a>(value: &'a mut u8) -> Add1Future<'a> {
    Add1Future(value)
}
```

When we try to use this version, we get a more useful error message:
```rust
async fn async_main() {
    let mut value = 0u8;
    apply(add1, &mut value).await;
    //    ^^^^ one type is more general than the other
    // expected fn pointer `for<'r> fn(&'r mut u8) -> Add1Future<'_>`
    //    found fn pointer `for<'r> fn(&'r mut u8) -> Add1Future<'r>`
}
```

It wants the function to make Futures with unconstrained lifetimes.
But `add1` returns Futures that are constrained.
The problem is that we cannot specify the lifetime of the generic
`Fut` type parameter.

```rust
async fn apply<Fut>(f: for<'r> fn(&mut u8) -> Fut, mut value: &mut u8)
where Fut: std::future::Future<'r, Output = ()> {}
//                             ^^ unexpected lifetime argument
//                             ^^ undeclared lifetime
```

One workaround makes the futures 'static by putting them on the heap.
Thanks to [u/Darksonn](https://www.reddit.com/user/Darksonn/)
for [teaching me this](https://www.reddit.com/r/rust/comments/jqrkpa/hey_rustaceans_got_an_easy_question_ask_here/gbt3klq/?utm_source=reddit&utm_medium=web2x&context=3).
 
```rust
async fn apply(f: fn(&mut u8) -> futures::future::BoxFuture<()>, mut value: &mut u8) {
    for _ in 0..10u8 {
        f(&mut value).await;
    }
}

async fn add1(value: &mut u8) {
    *value += 1;
}

async fn async_main() {
    let mut value = 0u8;
    apply(|v| Box::pin(add1(v)), &mut value).await;
}
```

Another workaround uses a trait to put a lifetime on the future.
It doesn't support closures.
Thanks to [u/Patryk27](https://www.reddit.com/user/Patryk27/)
for [teaching me this](https://www.reddit.com/r/rust/comments/jqrkpa/hey_rustaceans_got_an_easy_question_ask_here/gbuk4y0/?utm_source=reddit&utm_medium=web2x&context=3).

```rust
trait ActionFn<'a> {
    type Fut: std::future::Future<Output = ()> + 'a;

    fn call(&self, value: &'a mut u8) -> Self::Fut;
}

impl<'a, F, Fut2> ActionFn<'a> for F
where
    F: Fn(&'a mut u8) -> Fut2,
    Fut2: std::future::Future<Output = ()> + 'a,
{
    type Fut = Fut2;

    fn call(&self, value: &'a mut u8) -> Self::Fut {
        self(value)
    }
}

async fn apply(f: impl for<'a> ActionFn<'a>, mut value: &mut u8) {
    for _ in 0..10u8 {
        f.call(&mut value).await;
    }
}

async fn add1(value: &mut u8) {
    *value += 1;
}

async fn async_main() {
    let mut value = 0u8;
    apply(add1, &mut value).await;
    // apply(|v| add1(v), &mut value).await;
    // ^^^^^ implementation of `ActionFn` is not general enough
    // closure must implement `ActionFn<'0>`, for any lifetime `'0`...
    // but it actually implements `ActionFn<'1>`, for some specific lifetime `'1`
}
```

We can use that workaround to implement our original API design.

Except I couldn't get it to work.
I spent several hours on it and gave up.
Other people [struggle with this same problem](https://users.rust-lang.org/t/accepting-an-async-closure-lifetimes-stumped/39204/8).

There is a [Rust compiler issue tracking it](https://github.com/rust-lang/rust/issues/41078).

```rust
pub trait HandlerFn<'a, S: Clone + Send + Sync> {
    type Fut: std::future::Future<Output = Result<(), HttpError>> + Send + 'a;

    fn call(&self, state: &'a S, req: &'a HttpRequest, writer: &'a mut HttpWriter<'_>)
        -> Self::Fut;
}

impl<'a, S, F, Fut2> HandlerFn<'a, S> for F
where
    S: Clone + Send + Sync,
    F: Fn(&'a S, &'a HttpRequest, &'a mut HttpWriter<'_>) -> Fut2,
    Fut2: std::future::Future<Output = Result<(), HttpError>> + Send + 'a,
{
    type Fut = Fut2;

    fn call(
        &self,
        state: &'a S,
        req: &'a HttpRequest,
        writer: &'a mut HttpWriter<'_>,
    ) -> Self::Fut {
        self(state, req, writer)
    }
}

pub async fn start_server<S: 'static, F: 'static>(
    stopper: Stopper,
    state: S,
    request_handler: F,
) -> Result<(), std::io::Error>
where
    S: Clone + Send + Sync,
    F: for<'a> HandlerFn<'a, S> + Send + Sync + Copy,
{ ... }

async fn async_main() {
    beatrice_http::start_server(stopper, (), handle_request).await;
    //^^^^^^^^^^^^^^^^^^^^^^^^^ implementation of `HandlerFn` is not general enough
    // note: `for<'_, '_, '_, '_> fn(&(), &HttpRequest, &mut HttpWriter<'_>) -> impl Future {handle_request}` must implement `HandlerFn<'0, ()>`, for any lifetime `'0`...
    // note: ...but `HandlerFn<'_, ()>` is actually implemented for the type `for<'_, '_, '_, '_> fn(&(), &HttpRequest, &mut HttpWriter<'_>) -> impl Future {handle_request}`
}
```

I also tried using [async_trait](https://crates.io/crates/async-trait).
It implements the BoxFuture workaround above.
And it has the same inscrutable type mismatch errors when trying to pass anything
to an async fn by reference.  I gave up on the callback model.

I decided to invert the API.
The user code will perform loops, spawn tasks, call server functions, and call handlers.
This will be some boilerplate code,
but it makes the server's execution and allocation very clear.
I'm happy with it.  And it compiles:

```rust
async fn handle_request(req: &HttpRequest, rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    match req.method {
        HttpMethod::GET => rw.send_text(HttpStatus::Ok200, "body1").await,
        _ => rw.send(HttpStatus::MethodNotAllowed405).await,
    }
}

async fn async_main() {
    let (mut accepter, mut stopper_ctl) = beatrice_http::listen("127.0.0.1:1690").await;
    tokio::spawn(async move {
        while let Some((conn, addr, stopper, now)) = accepter.next().await {
            tokio::spawn(async move {
                let mut rw = HttpReaderWriter::new(stopper, conn, addr, now);
                while let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                    let result = handle_request(&req, &mut rw).await;
                    beatrice_http::log_result(&req, &rw, result);
                }
            });
        }
    });
    beatrice_http::wait_for_shutdown_signal().await;
    stopper_ctl.graceful_shutdown().await;
}
```
