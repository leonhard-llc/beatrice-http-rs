use crate::{is_hex_digit, write_hex, HttpError, ParsingError};
use fixed_buffer::FixedBuf;

/// Makes a new buffer containing
/// a valid HTTP chunk head encoding `chunk_len`.
pub fn chunk_head(chunk_len: u64) -> FixedBuf<[u8; 18]> {
    let mut buf = FixedBuf::new([0u8; 18]);
    write_hex(&mut buf, chunk_len).unwrap();
    buf.write_str("\r\n").unwrap();
    buf
}

// Makes a new buffer containing a valid HTTP chunk foot.
pub fn chunk_foot() -> FixedBuf<&'static [u8]> {
    FixedBuf::filled(b"\r\n")
}

/// Reads an HTTP chunk head from the buffer and returns Some(chunk_len).
///
/// Returns None if the buffer contains an incomplete valid chunk head
/// or is empty.
///
/// Returns `HttpError::Parsing(ParsingError::ChunkHeadInvalid)`
/// if the buffer is an invalid chunk head.
pub fn read_chunk_head<B: AsRef<[u8]>>(buf: &mut FixedBuf<B>) -> Result<Option<u64>, HttpError> {
    // chunk          = chunk-size [ chunk-ext ] CRLF
    //                  chunk-data CRLF
    // chunk-size     = 1*HEXDIG
    // chunk-ext      = *( ";" chunk-ext-name [ "=" chunk-ext-val ] )
    // chunk-ext-name = token
    // chunk-ext-val  = token / quoted-string
    // quoted-string  = DQUOTE *( qdtext / quoted-pair ) DQUOTE
    // qdtext         = HTAB / SP /%x21 / %x23-5B / %x5D-7E / obs-text
    // obs-text       = %x80-FF
    // quoted-pair    = "\" ( HTAB / SP / VCHAR / obs-text )
    // chunk-data     = 1*OCTET ; a sequence of chunk-size octets
    // https://tools.ietf.org/html/rfc7230#section-4.1
    if buf.is_empty() {
        return Ok(None);
    }
    let readable = buf.readable();
    let mut n = 0;
    // Skip past hex digits.
    while n < readable.len() && is_hex_digit(readable[n]) {
        n += 1;
    }
    if n == 0 {
        // Buffer starts with a non-hex digit.
        return Err(HttpError::Parsing(ParsingError::ChunkHeadInvalid));
    }
    if n > 64 {
        // Buffer starts with too many hex digits.
        return Err(HttpError::Parsing(ParsingError::ChunkHeadInvalid));
    }
    let end_of_len = n;
    if n < readable.len() && readable[n] == b';' {
        n += 1;
        // Buffer contains extensions.  Skip over them.
        while n < readable.len() && readable[n] != b'\r' {
            n += 1;
        }
    }
    let mut bytes = buf.readable().iter().skip(n);
    match (bytes.next(), bytes.next()) {
        (None, None) => Ok(None),
        (Some(b'\r'), None) => Ok(None),
        (Some(b'\r'), Some(b'\n')) => {
            let chunk_len_str = std::str::from_utf8(&readable[..end_of_len]).unwrap();
            let chunk_len: u64 = u64::from_str_radix(chunk_len_str, 16)
                .map_err(|_| HttpError::Parsing(ParsingError::ChunkHeadInvalid))?;
            buf.read_bytes(n + 2);
            Ok(Some(chunk_len))
        }
        _ => Err(HttpError::Parsing(ParsingError::ChunkHeadInvalid)),
    }
}

/// Reads an HTTP chunk foot (CRLF) from the buffer and returns true.
///
/// Returns false if the buffer contains an incomplete valid chunk foot
/// or is empty.
///
/// Returns `HttpError::Parsing(ParsingError::ChunkMissingFinalCRLF)`
/// if the buffer is an invalid chunk foot.
pub fn read_chunk_final_crlf<B: AsRef<[u8]>>(buf: &mut FixedBuf<B>) -> Result<bool, HttpError> {
    // chunk      = chunk-size [ chunk-ext ] CRLF
    //              chunk-data CRLF
    // chunk-data = 1*OCTET ; a sequence of chunk-size octets
    // https://tools.ietf.org/html/rfc7230#section-4.1
    let mut bytes = buf.readable().iter();
    match (bytes.next(), bytes.next()) {
        (None, None) => Ok(false),
        (Some(b'\r'), None) => Ok(false),
        (Some(b'\r'), Some(b'\n')) => {
            buf.read_bytes(2);
            Ok(true)
        }
        _ => Err(HttpError::Parsing(ParsingError::ChunkMissingFinalCRLF)),
    }
}

/// Reads an HTTP chunked body foot from the buffer and returns true.
///
/// Returns false if the buffer contains an incomplete valid chunked body foot
/// or is empty.
pub fn read_chunked_body_foot<B: AsRef<[u8]>>(buf: &mut FixedBuf<B>) -> bool {
    // chunked-body   = *chunk
    //                  last-chunk
    //                  trailer-part
    //                  CRLF
    // trailer-part   = *( header-field CRLF )
    // https://tools.ietf.org/html/rfc7230#section-4.1
    if buf.len() < 2 {
        return false;
    }
    let readable = buf.readable();
    if readable[0] == b'\r' && readable[1] == b'\n' {
        // No trailers.
        buf.read_bytes(2);
        return true;
    }
    // Trailers present.  Search for final trailer's CRLF and final CRLF.
    if buf.len() < 4 {
        return false;
    }
    for n in 0..(buf.len() - 3) {
        if readable[n] == b'\r'
            && readable[n + 1] == b'\n'
            && readable[n + 2] == b'\r'
            && readable[n + 3] == b'\n'
        {
            buf.read_bytes(n + 4);
            return true;
        }
    }
    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_chunk_head() {
        assert_eq!(b"0\r\n", chunk_head(0).readable());
        assert_eq!(b"2A\r\n", chunk_head(42).readable());
        assert_eq!(
            b"FFFFFFFFFFFFFFFF\r\n",
            chunk_head(0xFFFFFFFFFFFFFFFF).readable()
        );
    }

    #[test]
    fn test_chunk_foot() {
        assert_eq!(b"\r\n", chunk_foot().readable());
    }

    #[test]
    fn test_read_chunk_head() {
        // Incomplete headers
        assert_eq!(Ok(None), read_chunk_head(&mut FixedBuf::filled(b"")));
        assert_eq!(Ok(None), read_chunk_head(&mut FixedBuf::filled(b"0")));
        assert_eq!(Ok(None), read_chunk_head(&mut FixedBuf::filled(b"0;")));
        assert_eq!(Ok(None), read_chunk_head(&mut FixedBuf::filled(b"0;x")));
        assert_eq!(Ok(None), read_chunk_head(&mut FixedBuf::filled(b"0\r")));
        assert_eq!(Ok(None), read_chunk_head(&mut FixedBuf::filled(b"0;\r")));
        assert_eq!(
            Ok(None),
            read_chunk_head(&mut FixedBuf::filled(b"0;a=\"b\"\r"))
        );
        // Shortest chunk length
        assert_eq!(
            Ok(Some(0)),
            read_chunk_head(&mut FixedBuf::filled(b"0\r\n"))
        );
        // Longest chunk length
        assert_eq!(
            Ok(Some(0)),
            read_chunk_head(&mut FixedBuf::filled(
                /* 64 zeros */
                b"0000000000000000000000000000000000000000000000000000000000000000\r\n"
            ))
        );
        // Chunk length numerical values
        assert_eq!(
            Ok(Some(0)),
            read_chunk_head(&mut FixedBuf::filled(b"0\r\n"))
        );
        assert_eq!(
            Ok(Some(42)),
            read_chunk_head(&mut FixedBuf::filled(b"2a\r\n"))
        );
        assert_eq!(
            Ok(Some(0xFFFFFFFFFFFFFFFF)),
            read_chunk_head(&mut FixedBuf::filled(b"FFFFFFFFFFFFFFFF\r\n"))
        );
        // Leading zeros ignored
        assert_eq!(
            Ok(Some(1)),
            read_chunk_head(&mut FixedBuf::filled(b"01\r\n"))
        );
        assert_eq!(
            Ok(Some(0x123456789abcdef0)),
            read_chunk_head(&mut FixedBuf::filled(b"0123456789abcdef0\r\n"))
        );
        // Chunk length hexadecimal symbols
        assert_eq!(
            Ok(Some(0x1234567890)),
            read_chunk_head(&mut FixedBuf::filled(b"1234567890\r\n"))
        );
        assert_eq!(
            Ok(Some(0xABCDEF)),
            read_chunk_head(&mut FixedBuf::filled(b"ABCDEF\r\n"))
        );
        assert_eq!(
            Ok(Some(0xABCDEF)),
            read_chunk_head(&mut FixedBuf::filled(b"abcdef\r\n"))
        );
        // Ignores extensions
        assert_eq!(
            Ok(Some(1)),
            read_chunk_head(&mut FixedBuf::filled(b"1;a=\"b\";c=\"d\"\r\n"))
        );
        // Ignores malformed extensions
        assert_eq!(
            Ok(Some(1)),
            read_chunk_head(&mut FixedBuf::filled(b"1;a=b\r\n"))
        );
        // Removes the processed data and leaves following data intact
        {
            let mut buf = FixedBuf::filled(b"0\r\nX");
            assert_eq!(Ok(Some(0)), read_chunk_head(&mut buf));
            assert_eq!(b"X", buf.readable());
        }
        {
            let mut buf = FixedBuf::filled(b"0;ext=\"val\"\r\nX");
            assert_eq!(Ok(Some(0)), read_chunk_head(&mut buf));
            assert_eq!(b"X", buf.readable());
        }
        {
            let mut buf = FixedBuf::filled(b"0\r\n\r\n");
            assert_eq!(Ok(Some(0)), read_chunk_head(&mut buf));
            assert_eq!(b"\r\n", buf.readable());
        }
        {
            let mut buf = FixedBuf::filled(b"0\r\n0\r\n");
            assert_eq!(Ok(Some(0)), read_chunk_head(&mut buf));
            assert_eq!(b"0\r\n", buf.readable());
        }

        const ERR: Result<Option<u64>, HttpError> =
            Err(HttpError::Parsing(ParsingError::ChunkHeadInvalid));
        // Missing chunk length
        assert_eq!(ERR, read_chunk_head(&mut FixedBuf::filled(b"\r\n")));
        // Chunk length too long
        assert_eq!(
            ERR,
            read_chunk_head(&mut FixedBuf::filled(
                /* 65 zeros */
                b"00000000000000000000000000000000000000000000000000000000000000000\r\n"
            ))
        );
        // Invalid chunk length symbol
        assert_eq!(ERR, read_chunk_head(&mut FixedBuf::filled(b"g\r\n")));
        assert_eq!(ERR, read_chunk_head(&mut FixedBuf::filled(b"0g\r\n")));
        assert_eq!(ERR, read_chunk_head(&mut FixedBuf::filled(b"g0\r\n")));
        assert_eq!(ERR, read_chunk_head(&mut FixedBuf::filled(b"0g0\r\n")));
        // Chunk length too large
        assert_eq!(
            ERR,
            read_chunk_head(&mut FixedBuf::filled(
                /* 1 and 16 zeros */
                b"10000000000000000\r\n"
            ))
        );
    }

    #[test]
    fn test_read_chunk_final_crlf() {
        // Incomplete foot
        assert_eq!(Ok(false), read_chunk_final_crlf(&mut FixedBuf::filled(b"")));
        assert_eq!(
            Ok(false),
            read_chunk_final_crlf(&mut FixedBuf::filled(b"\r"))
        );
        // Complete
        assert_eq!(
            Ok(true),
            read_chunk_final_crlf(&mut FixedBuf::filled(b"\r\n"))
        );
        // Removes the processed data and leaves following data intact
        {
            let mut buf = FixedBuf::filled(b"\r\nX");
            assert_eq!(Ok(true), read_chunk_final_crlf(&mut buf));
            assert_eq!(b"X", buf.readable());
        }

        const ERR: Result<bool, HttpError> =
            Err(HttpError::Parsing(ParsingError::ChunkMissingFinalCRLF));
        // Invalid symbol
        assert_eq!(ERR, read_chunk_final_crlf(&mut FixedBuf::filled(b"x\r\n")));
        assert_eq!(ERR, read_chunk_final_crlf(&mut FixedBuf::filled(b"x\n")));
        assert_eq!(ERR, read_chunk_final_crlf(&mut FixedBuf::filled(b"\r\r")));
        assert_eq!(ERR, read_chunk_final_crlf(&mut FixedBuf::filled(b"\n\n")));
        assert_eq!(ERR, read_chunk_final_crlf(&mut FixedBuf::filled(b"\n\r")));
    }

    #[test]
    fn test_read_chunked_body_foot() {
        // Incomplete
        assert!(!read_chunked_body_foot(&mut FixedBuf::filled(b"")));
        assert!(!read_chunked_body_foot(&mut FixedBuf::filled(b"\r")));
        assert!(!read_chunked_body_foot(&mut FixedBuf::filled(b"t")));
        assert!(!read_chunked_body_foot(&mut FixedBuf::filled(b"tr")));
        assert!(!read_chunked_body_foot(&mut FixedBuf::filled(
            b"trailer1: value1\r\n\r"
        )));
        assert!(!read_chunked_body_foot(&mut FixedBuf::filled(
            b"t1:v1\r\nt2:line1\r\n line2\r\n\r"
        )));
        // Complete
        assert!(read_chunked_body_foot(&mut FixedBuf::filled(b"\r\n")));
        assert!(read_chunked_body_foot(&mut FixedBuf::filled(
            b"trailer1: value1\r\n\r\n"
        )));
        assert!(read_chunked_body_foot(&mut FixedBuf::filled(
            b"t1:v1\r\nmulti-line-trailer:line1\r\n line2\r\n\r\n"
        )));
        // Ignores malformed trailers
        assert!(read_chunked_body_foot(&mut FixedBuf::filled(
            b"invalid\r\nbad trailer\r\nbad=trailer\r\n\r\n"
        )));
        // Removes the processed data and leaves following data intact
        {
            let mut buf = FixedBuf::filled(b"\r\n\r\n");
            assert!(read_chunked_body_foot(&mut buf));
            assert_eq!(b"\r\n", buf.readable());
        }
        {
            let mut buf = FixedBuf::filled(b"t1:v1\r\nt2:line1\r\n line2\r\n\r\n\r\n");
            assert!(read_chunked_body_foot(&mut buf));
            assert_eq!(b"\r\n", buf.readable());
        }
        {
            let mut buf = FixedBuf::filled(b"t1:v1\r\nt2:line1\r\n line2\r\n\r\nX");
            assert!(read_chunked_body_foot(&mut buf));
            assert_eq!(b"X", buf.readable());
        }
        {
            let mut buf = FixedBuf::filled(b"t1:v1\r\nt2:line1\r\n line2\r\n\r\nX\r\n\r\n");
            assert!(read_chunked_body_foot(&mut buf));
            assert_eq!(b"X\r\n\r\n", buf.readable());
        }
    }
}
