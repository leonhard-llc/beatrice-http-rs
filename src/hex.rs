/// Converts a number into the corresponding upper-case hexadecimal digit, in UTF-8/ASCII.
/// Panics when `n` is larger than 15.
///
/// Example:
/// ```
/// # use beatrice_http::hex_digit;
/// assert_eq!(b'F', hex_digit(15));
/// ```
pub fn hex_digit(n: u8) -> u8 {
    match n {
        0 => b'0',
        1 => b'1',
        2 => b'2',
        3 => b'3',
        4 => b'4',
        5 => b'5',
        6 => b'6',
        7 => b'7',
        8 => b'8',
        9 => b'9',
        10 => b'A',
        11 => b'B',
        12 => b'C',
        13 => b'D',
        14 => b'E',
        15 => b'F',
        _ => {
            panic!("hex digit out of range: {}", n);
        }
    }
}

/// Returns true when `byte` encodes
/// an upper-case or lower-case hexadecimal digit in UTF-8/ASCII.
pub fn is_hex_digit(byte: u8) -> bool {
    // HEXDIG (hexadecimal 0-9/A-F/a-f), https://tools.ietf.org/html/rfc7230#section-1.2
    #[allow(clippy::match_like_matches_macro)]
    match byte {
        b'0'..=b'9' | b'A'..=b'F' | b'a'..=b'f' => true,
        _ => false,
    }
}

/// Converts `n` to an upper-case hexadecimal string, encodes it as UTF-8/ASCII,
/// and writes it to `w`.
pub fn write_hex<W: std::io::Write>(w: &mut W, n: u64) -> Result<(), std::io::Error> {
    if n & 0xF000000000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 60) & 0xF) as u8)])?;
    }
    if n & 0xFF00000000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 56) & 0xF) as u8)])?;
    }
    if n & 0xFFF0000000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 52) & 0xF) as u8)])?;
    }
    if n & 0xFFFF000000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 48) & 0xF) as u8)])?;
    }
    if n & 0xFFFFF00000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 44) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFF0000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 40) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFF000000000 != 0 {
        w.write_all(&[hex_digit(((n >> 36) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFF00000000 != 0 {
        w.write_all(&[hex_digit(((n >> 32) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFF0000000 != 0 {
        w.write_all(&[hex_digit(((n >> 28) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFFF000000 != 0 {
        w.write_all(&[hex_digit(((n >> 24) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFFFF00000 != 0 {
        w.write_all(&[hex_digit(((n >> 20) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFFFFF0000 != 0 {
        w.write_all(&[hex_digit(((n >> 16) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFFFFFF000 != 0 {
        w.write_all(&[hex_digit(((n >> 12) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFFFFFFF00 != 0 {
        w.write_all(&[hex_digit(((n >> 8) & 0xF) as u8)])?;
    }
    if n & 0xFFFFFFFFFFFFFFF0 != 0 {
        w.write_all(&[hex_digit(((n >> 4) & 0xF) as u8)])?;
    }
    w.write_all(&[hex_digit((n & 0xF) as u8)])?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::unwrap_panic;
    use fixed_buffer::FixedBuf;

    #[test]
    fn test_hex_digit() {
        assert_eq!(b'0', hex_digit(0));
        assert_eq!(b'1', hex_digit(1));
        assert_eq!(b'2', hex_digit(2));
        assert_eq!(b'3', hex_digit(3));
        assert_eq!(b'4', hex_digit(4));
        assert_eq!(b'5', hex_digit(5));
        assert_eq!(b'6', hex_digit(6));
        assert_eq!(b'7', hex_digit(7));
        assert_eq!(b'8', hex_digit(8));
        assert_eq!(b'9', hex_digit(9));
        assert_eq!(b'A', hex_digit(10));
        assert_eq!(b'B', hex_digit(11));
        assert_eq!(b'C', hex_digit(12));
        assert_eq!(b'D', hex_digit(13));
        assert_eq!(b'E', hex_digit(14));
        assert_eq!(b'F', hex_digit(15));
    }

    #[test]
    fn test_hex_digit_out_of_range() {
        assert_eq!("hex digit out of range: 16", unwrap_panic(|| hex_digit(16)));
    }

    #[test]
    fn test_is_hex_digit() {
        assert!(!is_hex_digit(b'/')); // '/' comes before '0'.
        assert!(is_hex_digit(b'0'));
        assert!(is_hex_digit(b'1'));
        assert!(is_hex_digit(b'2'));
        assert!(is_hex_digit(b'3'));
        assert!(is_hex_digit(b'4'));
        assert!(is_hex_digit(b'5'));
        assert!(is_hex_digit(b'6'));
        assert!(is_hex_digit(b'7'));
        assert!(is_hex_digit(b'8'));
        assert!(is_hex_digit(b'9'));
        assert!(!is_hex_digit(b':')); // ':' comes after '9'.

        assert!(!is_hex_digit(b'`')); // '`' comes before 'a'.
        assert!(is_hex_digit(b'a'));
        assert!(is_hex_digit(b'b'));
        assert!(is_hex_digit(b'c'));
        assert!(is_hex_digit(b'd'));
        assert!(is_hex_digit(b'e'));
        assert!(is_hex_digit(b'f'));
        assert!(!is_hex_digit(b'g'));

        assert!(!is_hex_digit(b'@')); // '@' comes before 'A'.
        assert!(is_hex_digit(b'A'));
        assert!(is_hex_digit(b'B'));
        assert!(is_hex_digit(b'C'));
        assert!(is_hex_digit(b'D'));
        assert!(is_hex_digit(b'E'));
        assert!(is_hex_digit(b'F'));
        assert!(!is_hex_digit(b'G'));
    }

    #[test]
    fn test_write_hex() {
        {
            let mut buf = FixedBuf::new([0u8; 20]);
            write_hex(&mut buf, 0).unwrap();
            assert_eq!("0", buf.escape_ascii());
        }
        {
            let mut buf = FixedBuf::new([0u8; 20]);
            write_hex(&mut buf, 42).unwrap();
            assert_eq!("2A", buf.escape_ascii());
        }
        {
            let mut buf = FixedBuf::new([0u8; 16]);
            write_hex(&mut buf, 0xFFFFFFFFFFFFFFFF).unwrap();
            assert_eq!("FFFFFFFFFFFFFFFF", buf.escape_ascii());
        }
        write_hex(&mut FixedBuf::new([0u8; 0]), 0x1).unwrap_err();
    }
}
