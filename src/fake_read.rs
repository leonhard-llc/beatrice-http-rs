pub enum FakeReadAction {
    SleepMillis(std::time::Duration),
    Data(Vec<u8>),
    Error(std::io::Error),
}

pub struct FakeRead(Vec<FakeReadAction>);
impl FakeRead {
    pub fn new(actions: Vec<FakeReadAction>) -> Self {
        Self(actions)
    }
}

impl std::io::Read for FakeRead {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, std::io::Error> {
        while !self.0.is_empty() {
            match self.0.remove(0) {
                FakeReadAction::SleepMillis(duration) => {
                    std::thread::sleep(duration);
                }
                FakeReadAction::Data(ref mut bytes) => {
                    let num_to_copy = bytes.len().min(buf.len());
                    let src = bytes.get(..num_to_copy).unwrap();
                    let dest = &mut buf[..num_to_copy];
                    dest.copy_from_slice(src);
                    bytes.copy_within(num_to_copy.., 0);
                    bytes.truncate(bytes.len() - num_to_copy);
                    return Ok(num_to_copy);
                }
                FakeReadAction::Error(e) => return Err(e),
            }
        }
        Ok(0)
    }
}
