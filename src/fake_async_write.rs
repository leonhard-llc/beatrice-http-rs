use std::pin::Pin;
use std::task::{Context, Poll};

#[derive(Debug)]
pub enum FakeAsyncWriteAction<'a> {
    Write(&'a mut Vec<u8>),
    WriteExact(&'a mut [u8]),
    WriteError(std::io::Error),
    WritePending,
    Flush,
    FlushError(std::io::Error),
    FlushPending,
    Shutdown,
    ShutdownError(std::io::Error),
    ShutdownPending,
}

pub struct FakeAsyncWrite<'a>(Vec<FakeAsyncWriteAction<'a>>);

impl<'a> FakeAsyncWrite<'a> {
    pub fn new(actions: Vec<FakeAsyncWriteAction<'a>>) -> Self {
        Self(actions)
    }

    pub fn unwrap_empty(&self) {
        if !self.0.is_empty() {
            panic!("FakeAsyncWrite has unconsumed actions");
        }
    }
}

impl<'a> tokio::io::AsyncWrite for FakeAsyncWrite<'a> {
    fn poll_write(
        self: Pin<&mut Self>,
        _cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, std::io::Error>> {
        let mut_self = self.get_mut();
        match mut_self.0.remove(0) {
            FakeAsyncWriteAction::Write(dest) => {
                dest.extend_from_slice(buf);
                Poll::Ready(Ok(buf.len()))
            }
            FakeAsyncWriteAction::WriteExact(dest) => {
                dest.copy_from_slice(buf);
                Poll::Ready(Ok(buf.len()))
            }
            FakeAsyncWriteAction::WriteError(e) => Poll::Ready(Err(e)),
            FakeAsyncWriteAction::WritePending => Poll::Pending,
            other => panic!("poll_write called when next action is {:?}", other),
        }
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), std::io::Error>> {
        let mut_self = self.get_mut();
        match mut_self.0.remove(0) {
            FakeAsyncWriteAction::Flush => Poll::Ready(Ok(())),
            FakeAsyncWriteAction::FlushError(e) => Poll::Ready(Err(e)),
            FakeAsyncWriteAction::FlushPending => Poll::Pending,
            other => panic!("poll_flush called when next action is {:?}", other),
        }
    }

    fn poll_shutdown(
        self: Pin<&mut Self>,
        _cx: &mut Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        let mut_self = self.get_mut();
        match mut_self.0.remove(0) {
            FakeAsyncWriteAction::Shutdown => Poll::Ready(Ok(())),
            FakeAsyncWriteAction::ShutdownError(e) => Poll::Ready(Err(e)),
            FakeAsyncWriteAction::ShutdownPending => Poll::Pending,
            other => panic!("poll_shutdown called when next action is {:?}", other),
        }
    }
}
