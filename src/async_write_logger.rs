use crate::wire;

use std::task::Context;

use tokio::io::AsyncWrite;
use tokio::macros::support::{Pin, Poll};

pub struct AsyncWriteLogger<'a> {
    inner: &'a mut (dyn tokio::io::AsyncWrite + Send + Unpin),
}

impl<'a> AsyncWriteLogger<'a> {
    pub fn new(inner: &'a mut (dyn tokio::io::AsyncWrite + Send + Unpin)) -> AsyncWriteLogger<'a> {
        AsyncWriteLogger { inner }
    }
}

impl<'a> AsyncWrite for AsyncWriteLogger<'a> {
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, std::io::Error>> {
        let mut_self = self.get_mut();
        match Pin::new(&mut mut_self.inner).poll_write(cx, buf) {
            Poll::Ready(Ok(n)) => {
                wire::write::trace(&buf[..n]);
                Poll::Ready(Ok(n))
            }
            Poll::Ready(Err(e)) => {
                wire::write::trace_err(&e);
                Poll::Ready(Err(e))
            }
            other => other,
        }
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), std::io::Error>> {
        let mut_self = self.get_mut();
        wire::write::trace_flush();
        match Pin::new(&mut mut_self.inner).poll_flush(cx) {
            Poll::Ready(Err(e)) => {
                wire::write::trace_err(&e);
                Poll::Ready(Err(e))
            }
            other => other,
        }
    }

    fn poll_shutdown(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        wire::write::trace_shutdown();
        Pin::new(&mut self.get_mut().inner).poll_shutdown(cx)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{capture_thread_logs, filter_log_entries, FakeAsyncWrite, FakeAsyncWriteAction};

    #[test]
    fn test_poll_write() {
        let mut write1: Vec<u8> = Vec::new();
        let mut inner = FakeAsyncWrite::new(vec![
            FakeAsyncWriteAction::WritePending,
            FakeAsyncWriteAction::Write(&mut write1),
        ]);
        let mut write_logger = AsyncWriteLogger::new(&mut inner);
        let (_, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncWriteExt::write_all(
                &mut write_logger,
                b"abc",
            ))
            .unwrap()
        });
        inner.unwrap_empty();
        assert_eq!(b"abc", write1.as_slice());
        //println!("entries={:?}", entries);
        assert_eq!(
            &["write \"abc\""],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }

    #[test]
    fn test_poll_write_error() {
        let mut inner = FakeAsyncWrite::new(vec![
            FakeAsyncWriteAction::WritePending,
            FakeAsyncWriteAction::WriteError(std::io::Error::new(
                std::io::ErrorKind::Other,
                "err1",
            )),
        ]);
        let mut write_logger = AsyncWriteLogger::new(&mut inner);
        let (err, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncWriteExt::write_all(
                &mut write_logger,
                b"abc",
            ))
            .unwrap_err()
        });
        inner.unwrap_empty();
        assert_eq!(std::io::ErrorKind::Other, err.kind());
        assert_eq!("err1", err.to_string());
        assert_eq!(
            &["write error: err1"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }

    #[test]
    fn test_poll_flush() {
        let mut inner = FakeAsyncWrite::new(vec![
            FakeAsyncWriteAction::FlushPending,
            FakeAsyncWriteAction::Flush,
        ]);
        let mut write_logger = AsyncWriteLogger::new(&mut inner);
        let (_, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncWriteExt::flush(&mut write_logger)).unwrap()
        });
        inner.unwrap_empty();
        assert_eq!(
            &["write flush", "write flush"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }

    #[test]
    fn test_poll_flush_error() {
        let mut inner = FakeAsyncWrite::new(vec![
            FakeAsyncWriteAction::FlushPending,
            FakeAsyncWriteAction::FlushError(std::io::Error::new(
                std::io::ErrorKind::Other,
                "err1",
            )),
        ]);
        let mut write_logger = AsyncWriteLogger::new(&mut inner);
        let (err, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncWriteExt::flush(&mut write_logger)).unwrap_err()
        });
        inner.unwrap_empty();
        assert_eq!(std::io::ErrorKind::Other, err.kind());
        assert_eq!("err1", err.to_string());
        assert_eq!(
            &["write flush", "write flush", "write error: err1"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }

    #[test]
    fn test_poll_shutdown() {
        let mut inner = FakeAsyncWrite::new(vec![
            FakeAsyncWriteAction::ShutdownPending,
            FakeAsyncWriteAction::Shutdown,
        ]);
        let mut write_logger = AsyncWriteLogger::new(&mut inner);
        let (_, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncWriteExt::shutdown(&mut write_logger)).unwrap()
        });
        inner.unwrap_empty();
        assert_eq!(
            &["write shutdown", "write shutdown"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }

    #[test]
    fn test_poll_shutdown_error() {
        let mut inner = FakeAsyncWrite::new(vec![
            FakeAsyncWriteAction::ShutdownPending,
            FakeAsyncWriteAction::ShutdownError(std::io::Error::new(
                std::io::ErrorKind::Other,
                "err1",
            )),
        ]);
        let mut write_logger = AsyncWriteLogger::new(&mut inner);
        let (err, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncWriteExt::shutdown(&mut write_logger)).unwrap_err()
        });
        inner.unwrap_empty();
        assert_eq!(std::io::ErrorKind::Other, err.kind());
        assert_eq!("err1", err.to_string());
        assert_eq!(
            &["write shutdown", "write shutdown"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }
}
