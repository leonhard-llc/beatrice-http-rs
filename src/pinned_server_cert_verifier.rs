/// A struct for TLS clients to verify the server's certificate.
/// Implements certificate pinning.
/// It accepts the server's certificate if it is identical to any of the certificates in the struct.
///
/// The rustls library has an open issue to add something like this:
/// "Implement support for certificate pinning" https://github.com/ctz/rustls/issues/227
pub struct PinnedServerCertVerifier {
    certs: Vec<rustls::Certificate>,
}

impl PinnedServerCertVerifier {
    pub fn new(certs: Vec<rustls::Certificate>) -> Self {
        Self { certs }
    }
}

impl rustls::ServerCertVerifier for PinnedServerCertVerifier {
    fn verify_server_cert(
        &self,
        _roots: &rustls::RootCertStore,
        presented_certs: &[rustls::Certificate],
        _dns_name: webpki::DNSNameRef,
        _ocsp_response: &[u8],
    ) -> Result<rustls::ServerCertVerified, rustls::TLSError> {
        // If the server sends several certificates (a certificate chain), we expect
        // the leaf certificate to be first.
        let presented_cert = &presented_certs[0];
        for cert in &self.certs {
            if presented_cert == cert {
                return Ok(rustls::ServerCertVerified::assertion());
            }
        }
        return Err(rustls::TLSError::WebPKIError(webpki::Error::UnknownIssuer));
    }
}

/// An arbitrary `DNSName` struct, for passing to [`tokio_rustls::TlsConnector::connect`].
/// `PinnedServerCertVerifier::verify_server_cert1` receives the value and ignores it.
pub fn arbitrary_dns_name() -> webpki::DNSName {
    webpki::DNSNameRef::try_from_ascii_str("arbitrary1")
        .unwrap()
        .to_owned()
}
