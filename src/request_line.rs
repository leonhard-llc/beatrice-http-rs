#![forbid(unsafe_code)]
use crate::{HttpError, ParsingError};
use once_cell::sync::Lazy;

#[derive(Clone, Debug, PartialEq)]
pub struct HttpRequestLine<'a> {
    pub method: &'a str,
    pub raw_path: &'a str,
}

impl<'a> HttpRequestLine<'a> {
    pub fn parse(line_bytes: &[u8]) -> Result<HttpRequestLine, HttpError> {
        // HTTP/1.1 Request Line https://tools.ietf.org/html/rfc7230#section-3.1.1
        let line = std::str::from_utf8(line_bytes)
            .map_err(|_| HttpError::Parsing(ParsingError::RequestLineInvalid))?;
        static REQUEST_LINE_RE: Lazy<regex::Regex> =
            Lazy::new(|| regex::Regex::new("^([^ ]+) (/[^ ]*) HTTP/1.1$").unwrap());
        let captures: regex::Captures = REQUEST_LINE_RE
            .captures(line)
            .ok_or(HttpError::Parsing(ParsingError::RequestLineInvalid))?;
        let method = captures.get(1).unwrap().as_str();
        let raw_path = captures.get(2).unwrap().as_str();
        Ok(HttpRequestLine { method, raw_path })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_derived() {
        let req_line = HttpRequestLine::parse(b"METHOD1 /path1 HTTP/1.1").unwrap();
        // Clone
        let _ = req_line.clone();
        // Debug
        assert_eq!(
            "HttpRequestLine { method: \"METHOD1\", raw_path: \"/path1\" }",
            format!("{:?}", req_line)
        );
        // PartialEq
        assert_eq!(
            req_line,
            HttpRequestLine::parse(b"METHOD1 /path1 HTTP/1.1").unwrap()
        );
    }

    #[test]
    fn test_parse() {
        assert_eq!(
            HttpError::Parsing(ParsingError::RequestLineInvalid),
            HttpRequestLine::parse(b"").unwrap_err()
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::RequestLineInvalid),
            HttpRequestLine::parse(b"not-a-request-line").unwrap_err()
        );
        {
            let line_string = format!("METHOD1 /{} HTTP/1.1", "x".repeat(1024));
            let line = HttpRequestLine::parse(line_string.as_bytes()).unwrap();
            assert_eq!("METHOD1", line.method);
            let expected_raw_path = format!("/{}", "x".repeat(1024));
            assert_eq!(expected_raw_path, line.raw_path);
        }
    }
}
