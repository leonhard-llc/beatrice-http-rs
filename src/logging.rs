// TODO(mleonhard) Move this to logsley and remove logging deps.
use rand::Rng;

pub fn random_id(len: usize) -> String {
    // Alphabet has 27 characters. Each randomly-selected character adds 4.75 bits of entropy.
    // Selecting 8 with replacement yields a random string with 38 bits of entropy.
    // At one request-per-second, duplicate request ids will occur once every 74 days, on average.
    // https://en.wikipedia.org/wiki/Birthday_problem
    // http://davidjohnstone.net/pages/hash-collision-probability
    let rng = rand::thread_rng();
    const ALPHABET: [char; 27] = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M',
        'N', 'P', 'Q', 'R', 'T', 'V', 'W', 'X', 'Z',
    ];
    let distribution = rand::distributions::Uniform::from(0..ALPHABET.len());
    rng.sample_iter(distribution)
        .take(len)
        .map(|n| ALPHABET[n])
        .collect()
}

pub fn id_task_scope<F>(f: F) -> slog_scope_futures::SlogScope<slog::Logger, F>
where
    F: std::future::Future,
{
    slog_scope_futures::SlogScope::new(slog_scope::logger().new(slog::o!("id" => random_id(8))), f)
}
