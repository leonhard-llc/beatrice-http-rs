#![forbid(unsafe_code)]
use crate::{Stopper, StopperController};
use log::{info, warn};

/// Listens for incoming TCP connections to the specified address
/// and creates an Accepter and a StopperController.
pub async fn listen(addr: &str) -> Result<(Accepter, StopperController), std::io::Error> {
    let (stopper_ctl, stopper) = StopperController::new();
    Ok((
        Accepter::new(tokio::net::TcpListener::bind(addr).await?, stopper),
        stopper_ctl,
    ))
}

/// Ignores common client-caused errors like connection reset.
///
/// Logs other errors and sleeps for 1 second.
///
/// This handles the situation when the server process uses
/// the maximum number of file descriptors
/// and the OS refuses to open sockets for new connections.
pub async fn handle_accept_error(e: std::io::Error, now: tokio::time::Instant) {
    match e.kind() {
        // Connection error.
        // The client closed the connection before we could accept it.
        // Do nothing.
        std::io::ErrorKind::ConnectionAborted
        | std::io::ErrorKind::ConnectionRefused
        | std::io::ErrorKind::ConnectionReset => {}
        // Error accepting the connection.
        // This usually means the process is using the maximum number of file
        // descriptors.
        _ => {
            warn!("error accepting connection: {:?}", e);
            // Sleep to avoid spamming the logs and wasting cpu time.
            tokio::time::sleep_until(now + tokio::time::Duration::from_secs(1)).await;
        }
    }
}

/// Holds a TCP listening socket and accepts new connections on it.
pub struct Accepter {
    listener: tokio::net::TcpListener,
    stopper: Stopper,
}

impl Accepter {
    pub fn new(listener: tokio::net::TcpListener, stopper: Stopper) -> Accepter {
        info!("listening on {}", listener.local_addr().unwrap());
        Accepter { listener, stopper }
    }

    pub fn local_addr(&self) -> Result<std::net::SocketAddr, std::io::Error> {
        self.listener.local_addr()
    }
    
    /// Waits for a new connection and accepts it.
    ///
    /// Returns `None` if the Stopper has been triggered.
    pub async fn next(
        &mut self,
    ) -> Option<(
        tokio::net::TcpStream,
        std::net::SocketAddr,
        Stopper,
        tokio::time::Instant,
    )> {
        loop {
            tokio::select! {
                result = self.listener.accept() => {
                    match result {
                        Err(e) => handle_accept_error(e, tokio::time::Instant::now()).await,
                        Ok((tcp_stream, addr)) => {
                            return Some((tcp_stream, addr, self.stopper.clone(), tokio::time::Instant::now()));
                        }
                    }
                }
                _ = self.stopper.wait() => {
                    return None;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_handle_accept_error() {
        tokio_test::block_on(async {
            let error_kinds = [
                std::io::ErrorKind::ConnectionAborted,
                std::io::ErrorKind::ConnectionRefused,
                std::io::ErrorKind::ConnectionReset,
            ];
            for error_kind in error_kinds.iter() {
                let start = tokio::time::Instant::now();
                handle_accept_error(std::io::Error::new(*error_kind, "err1"), start).await;
                let elapsed = tokio::time::Instant::now() - start;
                if elapsed > tokio::time::Duration::from_millis(100) {
                    panic!(
                        "handle_accept_error({:?}) took too long, {:?}",
                        error_kind, elapsed
                    );
                }
            }
            {
                let start = tokio::time::Instant::now();
                handle_accept_error(
                    std::io::Error::new(std::io::ErrorKind::Other, "err1"),
                    start - tokio::time::Duration::from_millis(980),
                )
                .await;
                let elapsed = tokio::time::Instant::now() - start;
                if elapsed < tokio::time::Duration::from_millis(20)
                    || elapsed > tokio::time::Duration::from_millis(100)
                {
                    panic!(
                        "handle_accept_error({:?}) completed in {:?}",
                        std::io::ErrorKind::Other,
                        elapsed
                    );
                }
            }
        });
    }

    fn spawn_connect_and_drop(addr: std::net::SocketAddr) {
        tokio::spawn(async move {
            let _tcp_stream = tokio::net::TcpStream::connect(addr).await.unwrap();
            tokio::time::sleep(tokio::time::Duration::from_millis(10)).await;
        });
    }

    #[test]
    fn test_handle_accepter() {
        tokio_test::block_on(async {
            let (mut accepter, mut stopper_ctl) = listen("127.0.0.1:0").await.unwrap();
            let addr = accepter.local_addr().unwrap();
            spawn_connect_and_drop(addr);
            accepter.next().await.unwrap();
            spawn_connect_and_drop(addr);
            accepter.next().await.unwrap();
            stopper_ctl.signal_stop();
            assert!(accepter.next().await.is_none());
        });
    }
}
