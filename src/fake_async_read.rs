use std::pin::Pin;
use std::task::{Context, Poll};

pub enum FakeAsyncReadAction {
    Data(Vec<u8>),
    Error(std::io::Error),
    Pending,
    EOF,
}

pub struct FakeAsyncRead(Vec<FakeAsyncReadAction>);
impl FakeAsyncRead {
    pub fn new(actions: Vec<FakeAsyncReadAction>) -> Self {
        for action in &actions {
            if let FakeAsyncReadAction::Data(ref bytes) = action {
                if bytes.is_empty() {
                    panic!("FakeAsyncReadAction::Data with empty vector, use FakeAsyncReadAction::EOF instead");
                }
            }
        }
        Self(actions)
    }
}

impl tokio::io::AsyncRead for FakeAsyncRead {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        let mut_self = self.get_mut();
        match mut_self.0.get_mut(0) {
            Some(FakeAsyncReadAction::Data(ref mut bytes)) => {
                let num_to_copy = buf.remaining().min(bytes.len());
                let src = bytes.get(..num_to_copy).unwrap();
                buf.put_slice(src);
                bytes.drain(..num_to_copy);
                if bytes.is_empty() {
                    mut_self.0.remove(0);
                }
                Poll::Ready(Ok(()))
            }
            Some(FakeAsyncReadAction::Error(_)) => {
                if let FakeAsyncReadAction::Error(e) = mut_self.0.remove(0) {
                    Poll::Ready(Err(e))
                } else {
                    unreachable!()
                }
            }
            Some(FakeAsyncReadAction::Pending) => {
                mut_self.0.remove(0);
                cx.waker().clone().wake();
                Poll::Pending
            }
            Some(FakeAsyncReadAction::EOF) | None => Poll::Ready(Ok(())),
        }
    }
}
