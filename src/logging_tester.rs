use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::sync::atomic::AtomicBool;

#[derive(Clone)]
pub struct LogEntry {
    pub level: log::Level,
    pub target: String,
    pub msg: String,
}

impl std::fmt::Debug for LogEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "LogEntry{{{},{},{:?}}}",
            self.level, self.target, self.msg
        )
    }
}

thread_local! {
    static THREAD_LOG_ENTRIES: RefCell<Option<Vec<LogEntry>>> = RefCell::new(None);
}

struct LoggingTesterLogger {}

impl log::Log for LoggingTesterLogger {
    fn enabled(&self, _metadata: &log::Metadata<'_>) -> bool {
        true
    }

    fn log(&self, record: &log::Record<'_>) {
        let log_entry = LogEntry {
            level: record.level(),
            target: record.target().to_string(),
            // See [std::fmt::Arguments](https://doc.rust-lang.org/std/fmt/struct.Arguments.html).
            msg: format!("{}", record.args()),
        };
        //println!("{} {}", log_entry.level, log_entry.msg);
        THREAD_LOG_ENTRIES
            .try_with(|entries_ref| {
                if let Some(ref mut entries) = *entries_ref.borrow_mut() {
                    entries.push(log_entry);
                }
            })
            .unwrap();
    }

    fn flush(&self) {}
}

static INSTALLED: AtomicBool = AtomicBool::new(false);

static LOGGER: Lazy<LoggingTesterLogger> = Lazy::new(|| LoggingTesterLogger {});

pub fn install_logging_tester_logger() {
    let already_installed = INSTALLED.swap(
        true,
        /* requires only atomicity */ std::sync::atomic::Ordering::Relaxed,
    );
    if !already_installed {
        log::set_logger(&*LOGGER).expect("no other logger is installed");
        log::set_max_level(log::LevelFilter::Trace);
    }
}

pub fn capture_thread_logs<R, F: FnOnce() -> R>(f: F) -> (R, Vec<LogEntry>) {
    install_logging_tester_logger();
    THREAD_LOG_ENTRIES
        .try_with(|entries_ref| {
            let mut entries = entries_ref.borrow_mut();
            if !entries.is_none() {
                panic!("already capturing thread logs");
            }
            *entries = Some(Vec::new());
        })
        .unwrap();
    let r = f();
    let entries = THREAD_LOG_ENTRIES
        .try_with(|entries_ref| entries_ref.borrow_mut().take())
        .unwrap()
        .unwrap();
    (r, entries)
}

pub fn filter_log_entries(
    entries: &[LogEntry],
    level: log::Level,
    target_prefix: &str,
) -> Vec<String> {
    let mut result: Vec<String> = Vec::with_capacity(entries.len());
    result.extend(
        entries
            .iter()
            .filter(|e| e.level >= level && e.target.starts_with(target_prefix))
            .map(|e| e.msg.clone()),
    );
    result
}
