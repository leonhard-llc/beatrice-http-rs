#![forbid(unsafe_code)]
use crate::percent_decode::percent_decode;
use crate::{
    save_header_value, split_iterate, Header, HeaderReceiver, HttpError, HttpMethod,
    HttpRequestLine, HttpStatus, ParsingError,
};
use std::convert::TryFrom;
use string_wrapper::StringWrapper;

#[derive(Clone, PartialEq)]
pub struct HttpRequest {
    pub addr: std::net::SocketAddr,
    pub method: HttpMethod,
    pub path: StringWrapper<[u8; 256]>,
    pub expect_100: bool,
    pub content_length: u64,
    pub chunked: bool,
}

impl HttpRequest {
    pub fn new(
        addr: std::net::SocketAddr,
        method: HttpMethod,
        path: StringWrapper<[u8; 256]>,
        expect_100: bool,
        content_length: u64,
        chunked: bool,
    ) -> Self {
        Self {
            addr,
            method,
            path,
            expect_100,
            content_length,
            chunked,
        }
    }

    pub fn parse(
        addr: std::net::SocketAddr,
        data: &[u8],
        extra_headers: &mut [&mut HeaderReceiver<'_>],
    ) -> Result<Self, HttpError> {
        // "HTTP/1.1 Message Syntax and Routing" https://tools.ietf.org/html/rfc7230
        let mut lines = split_iterate(data, b"\r\n");

        let line_bytes = lines
            .next()
            .ok_or(HttpError::Parsing(ParsingError::RequestLineMissing))?;
        let request_line = HttpRequestLine::parse(line_bytes)?;
        let mut path = StringWrapper::new([0u8; 256]);
        percent_decode(request_line.raw_path, &mut path)
            .map_err(|_| HttpError::Parsing(ParsingError::PathTooLong))?;

        let mut content_length = HeaderReceiver::new("content-length");
        let mut expect = HeaderReceiver::new("expect");
        let mut transfer_encoding = HeaderReceiver::new("transfer-encoding");

        for line_bytes in lines {
            let header = Header::parse(line_bytes)?;
            save_header_value(
                &header,
                &mut [&mut content_length, &mut expect, &mut transfer_encoding],
            )?;
            save_header_value(&header, extra_headers)?;
        }
        Ok(Self {
            addr,
            method: HttpMethod::new(request_line.method)?,
            path,
            expect_100: expect.is_100_continue()?,
            content_length: content_length.parse_content_length()?,
            chunked: transfer_encoding.is_chunked()?,
        })
    }

    pub fn has_body(&self) -> bool {
        // The presence of a message body in a request is signaled by a Content-Length or
        // Transfer-Encoding header field.
        self.chunked || self.content_length > 0
    }

    pub fn content_length_usize(&self) -> Result<usize, HttpError> {
        usize::try_from(self.content_length)
            .or(Err(HttpError::Handling(HttpStatus::PayloadTooLarge413)))
    }

    pub fn check_method(&self, m: HttpMethod) -> Result<(), HttpError> {
        if self.method == m {
            Ok(())
        } else {
            Err(HttpError::Handling(HttpStatus::MethodNotAllowed405))
        }
    }

    pub fn check_path(&self, p: &str) -> Result<(), HttpError> {
        if &*self.path == p {
            Ok(())
        } else {
            Err(HttpError::Handling(HttpStatus::NotFound404))
        }
    }
}

impl std::fmt::Debug for HttpRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "HttpRequest{{{:?}, {:?}, {:?}",
            &self.addr, &self.method, &self.path
        )?;
        if self.expect_100 {
            write!(f, ", expect_100")?;
        }
        if self.chunked {
            write!(f, ", chunked")?;
        }
        if self.content_length > 0 {
            write!(f, ", content_length={}", self.content_length)?;
        }
        write!(f, "}}")
    }
}

impl std::default::Default for HttpRequest {
    fn default() -> Self {
        Self::new(
            std::net::SocketAddr::new(std::net::IpAddr::V4(std::net::Ipv4Addr::new(0, 0, 0, 0)), 0),
            HttpMethod::GET,
            StringWrapper::from_str("/"),
            false,
            0,
            false,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn make_addr() -> std::net::SocketAddr {
        std::net::SocketAddr::new(std::net::IpAddr::V4(std::net::Ipv4Addr::new(1, 2, 3, 4)), 5)
    }

    fn make_http_request() -> HttpRequest {
        HttpRequest::new(
            make_addr(),
            HttpMethod::Other(StringWrapper::from_str("METHOD1")),
            StringWrapper::from_str("/path1"),
            true,
            42,
            true,
        )
    }

    #[test]
    fn test_derived() {
        // Clone
        #[allow(clippy::redundant_clone)]
        let _ = HttpRequest::default().clone();
        // PartialEq
        assert_eq!(HttpRequest::default(), HttpRequest::default());
    }

    #[test]
    fn test_new() {
        let req = make_http_request();
        assert_eq!("1.2.3.4:5", req.addr.to_string());
        assert_eq!("METHOD1", req.method.as_str());
        assert_eq!("/path1", &*req.path);
        assert!(req.expect_100);
        assert_eq!(42, req.content_length);
        assert!(req.chunked);
    }

    #[test]
    fn test_parse() {
        assert_eq!(
            HttpError::Parsing(ParsingError::RequestLineMissing),
            HttpRequest::parse(make_addr(), b"", &mut []).unwrap_err()
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::RequestLineInvalid),
            HttpRequest::parse(make_addr(), b"\r\n", &mut []).unwrap_err()
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::RequestLineInvalid),
            HttpRequest::parse(make_addr(), b"not-a-request-line\r\n", &mut []).unwrap_err()
        );
        // Path max length
        HttpRequest::parse(
            make_addr(),
            format!("GET /{} HTTP/1.1\r\n", "x".repeat(255)).as_bytes(),
            &mut [],
        )
        .unwrap();
        // Path too long
        assert_eq!(
            HttpError::Parsing(ParsingError::PathTooLong),
            HttpRequest::parse(
                make_addr(),
                format!("GET /{} HTTP/1.1\r\n", "x".repeat(256)).as_bytes(),
                &mut []
            )
            .unwrap_err()
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::HeaderLineInvalid),
            HttpRequest::parse(
                make_addr(),
                b"GET /{} HTTP/1.1\r\nNot a valid header line\r\n",
                &mut []
            )
            .unwrap_err()
        );
        // Test header capture
        let mut header1 = HeaderReceiver::new("header1");
        header1.value.push_str("default1");
        let mut header2 = HeaderReceiver::new("header2");
        let mut header3 = HeaderReceiver::new("header3");
        HttpRequest::parse(
            make_addr(),
            b"GET / HTTP/1.1\r\nheader2: value2",
            &mut [&mut header1, &mut header2, &mut header3],
        )
        .unwrap();
        assert_eq!("default1", &*header1.value);
        assert_eq!("value2", &*header2.value);
        assert_eq!("", &*header3.value);
        // Unspecified headers
        {
            let mut header1 = HeaderReceiver::new("header1");
            header1.value.push_str("default1");
            let mut header2 = HeaderReceiver::new("header2");
            let header3 = HeaderReceiver::new("header3");
            let req = HttpRequest::parse(
                make_addr(),
                b"GET / HTTP/1.1\r\nheader2:value2\r\n",
                &mut [&mut header1, &mut header2],
            )
            .unwrap();
            assert_eq!("1.2.3.4:5", req.addr.to_string());
            assert_eq!(HttpMethod::GET, req.method);
            assert_eq!("/", &*req.path);
            assert!(!req.expect_100);
            assert_eq!(0, req.content_length);
            assert!(!req.chunked);
            assert_eq!("default1", &*header1.value);
            assert_eq!("value2", &*header2.value);
            assert_eq!("", &*header3.value);
        }
        // All headers specified
        {
            let mut header1 = HeaderReceiver::new("header1");
            header1.value.push_str("default1");
            let mut header2 = HeaderReceiver::new("header2");
            let header3 = HeaderReceiver::new("header3");
            let req = HttpRequest::parse(make_addr(), b"METHOD2 /path2 HTTP/1.1\r\ntransfer-encoding:chunked\r\nexpect:100-continue\r\nheader2:value2\r\ncontent-length:5\r\n", &mut [&mut header1, &mut header2]).unwrap();
            assert_eq!("1.2.3.4:5", req.addr.to_string());
            assert_eq!("METHOD2", req.method.as_str());
            assert_eq!("/path2", &*req.path);
            assert!(req.expect_100);
            assert_eq!(5, req.content_length);
            assert!(req.chunked);
            assert_eq!("default1", &*header1.value);
            assert_eq!("value2", &*header2.value);
            assert_eq!("", &*header3.value);
        }
        // Check individual headers
        {
            let req = HttpRequest::parse(
                make_addr(),
                b"GET / HTTP/1.1\r\ncontent-length: 5\r\n",
                &mut [],
            )
            .unwrap();
            assert!(!req.expect_100);
            assert_eq!(5, req.content_length);
            assert!(!req.chunked);
        }
        {
            let req = HttpRequest::parse(
                make_addr(),
                b"GET / HTTP/1.1\r\nexpect: 100-continue\r\n",
                &mut [],
            )
            .unwrap();
            assert!(req.expect_100);
            assert_eq!(0, req.content_length);
            assert!(!req.chunked);
        }
        {
            let req = HttpRequest::parse(
                make_addr(),
                b"GET / HTTP/1.1\r\ntransfer-encoding: chunked\r\n",
                &mut [],
            )
            .unwrap();
            assert!(!req.expect_100);
            assert_eq!(0, req.content_length);
            assert!(req.chunked);
        }
    }

    #[test]
    fn test_has_body() {
        assert!(make_http_request().has_body());
        assert!(!HttpRequest::default().has_body());
    }

    #[test]
    fn test_content_length_usize() {
        assert_eq!(
            0usize,
            HttpRequest::default().content_length_usize().unwrap()
        );
        assert_eq!(42usize, make_http_request().content_length_usize().unwrap());
        if std::mem::size_of::<usize>() < std::mem::size_of::<u64>() {
            let mut req = make_http_request();
            req.content_length = u64::MAX;
            assert_eq!(
                HttpError::Handling(HttpStatus::PayloadTooLarge413),
                req.content_length_usize().unwrap_err()
            );
        }
    }

    #[test]
    fn test_check_method() {
        HttpRequest::default()
            .check_method(HttpMethod::GET)
            .unwrap();
        make_http_request()
            .check_method(HttpMethod::new("METHOD1").unwrap())
            .unwrap();
        assert_eq!(
            HttpError::Handling(HttpStatus::MethodNotAllowed405),
            make_http_request()
                .check_method(HttpMethod::GET)
                .unwrap_err()
        );
    }

    #[test]
    fn test_check_path() {
        make_http_request().check_path("/path1").unwrap();
        assert_eq!(
            HttpError::Handling(HttpStatus::NotFound404),
            make_http_request().check_path("").unwrap_err()
        );
        assert_eq!(
            HttpError::Handling(HttpStatus::NotFound404),
            make_http_request().check_path("abc/path1").unwrap_err()
        );
        assert_eq!(
            HttpError::Handling(HttpStatus::NotFound404),
            make_http_request().check_path("/path1abc").unwrap_err()
        );
        assert_eq!(
            HttpError::Handling(HttpStatus::NotFound404),
            make_http_request().check_path("/path2").unwrap_err()
        );
        assert_eq!(
            HttpError::Handling(HttpStatus::NotFound404),
            make_http_request()
                .check_path(&"/".repeat(1024))
                .unwrap_err()
        );
    }

    #[test]
    fn test_debug() {
        assert_eq!(
            "HttpRequest{0.0.0.0:0, GET, \"/\"}",
            format!("{:?}", HttpRequest::default())
        );
        assert_eq!(
            "HttpRequest{1.2.3.4:5, Other(\"METHOD1\"), \"/path1\", expect_100, chunked, content_length=42}",
            format!("{:?}", make_http_request())
        );
    }

    #[test]
    fn test_default() {
        let req = HttpRequest::default();
        assert_eq!("0.0.0.0:0", req.addr.to_string());
        assert_eq!("GET", req.method.as_str());
        assert_eq!("/", &*req.path);
        assert!(!req.expect_100);
        assert_eq!(0, req.content_length);
        assert!(!req.chunked);
    }
}
