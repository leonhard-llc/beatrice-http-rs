#![forbid(unsafe_code)]
use crate::{HttpError, ParsingError};
use once_cell::sync::Lazy;
use string_wrapper::StringWrapper;

/// Returns true if the header name matches headers that cannot carry PII:
/// `transfer-encoding`, `content-length`, `content-type`, and `content-encoding`.
pub fn is_non_pii_header(name: &str) -> bool {
    name.eq_ignore_ascii_case("transfer-encoding")
        || name.eq_ignore_ascii_case("content-length")
        || name.eq_ignore_ascii_case("content-type")
        || name.eq_ignore_ascii_case("content-encoding")
}

fn fmt(f: &mut std::fmt::Formatter<'_>, name: &str, value: &str) -> std::fmt::Result {
    if is_non_pii_header(name) {
        write!(f, "{}:{}", name.to_ascii_lowercase(), value)
    } else {
        write!(f, "{}:...", name.to_ascii_lowercase())
    }
}

/// A header with name and value.
///
/// [`std::fmt::Display`] trait impl includes the header value only
/// for headers that cannot carry PII:
/// - `transfer-encoding`
/// - `content-length`
/// - `content-type`
/// - `content-encoding`
///
/// For all other headers, the value is displayed as "...".
///
/// Example:
/// ```
/// # use beatrice_http::Header;
/// assert_eq!(
///     "content-type:text/text",
///     format!("{}", Header::new("Content-Type", "text/text"))
/// );
/// assert_eq!(
///     "cookie:...",
///     format!("{}", Header::new("Cookie", "Secret1"))
/// );
/// ```
///
/// [`std::fmt::Debug`] trait impl includes header values.
/// ```
/// # use beatrice_http::Header;
/// assert_eq!(
///     "Header{cookie:Secret1}",
///     format!("{:?}", Header::new("Cookie", "Secret1"))
/// );
/// ```
#[derive(Clone, PartialEq)]
pub struct Header<'a> {
    pub name: &'a str,
    pub value: &'a str,
}

impl<'a> Header<'a> {
    pub fn new<'b>(name: &'b str, value: &'b str) -> Header<'b> {
        Header { name, value }
    }

    pub fn parse(line_bytes: &'a [u8]) -> Result<Header<'a>, HttpError> {
        // header-field   = field-name ":" OWS field-value OWS
        // field-name     = token
        // field-value    = * field-content
        // field-content  = field-vchar [ 1*( SP / HTAB ) field-vchar ]
        // field-vchar    = VCHAR / obs-text
        // VCHAR is any visible US-ASCII character
        // obs-text       = %x80-FF
        // HTTP/1.1 Header Fields https://tools.ietf.org/html/rfc7230#section-3.2
        let line = std::str::from_utf8(line_bytes)
            .or(Err(HttpError::Parsing(ParsingError::HeaderLineInvalid)))?;
        static LINE_RE: Lazy<regex::Regex> =
            Lazy::new(|| regex::Regex::new("^([^:]+):\\s*(.*?)\\s*$").unwrap());
        let captures: regex::Captures = LINE_RE
            .captures(line)
            .ok_or(HttpError::Parsing(ParsingError::HeaderLineInvalid))?;
        let name = captures.get(1).unwrap().as_str();
        let value = captures.get(2).unwrap().as_str();
        Ok(Header::new(name, value))
    }
}

impl<'a> std::fmt::Display for Header<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fmt(f, self.name, self.value)
    }
}

impl<'a> std::fmt::Debug for Header<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Header{{{}:{}}}",
            self.name.to_ascii_lowercase(),
            self.value
        )
    }
}

/// Formats the slice using [`std::fmt::Display`].
pub fn headers_to_string(headers: &[&Header]) -> String {
    let header_strings: Vec<String> = headers.iter().map(|&h| h.to_string()).collect();
    "[".to_string() + &header_strings.join(", ") + "]"
}

/// Receives a header value.
///
/// [`std::fmt::Display`] trait impl includes the header value only
/// for headers that cannot carry PII:
/// - `transfer-encoding`
/// - `content-length`
/// - `content-type`
/// - `content-encoding`
///
/// For all other headers, the value is displayed as "...".
///
/// Example:
/// ```
/// # use beatrice_http::{save_header_value, Header, HeaderReceiver};
/// let mut content_type = HeaderReceiver::new("Content-Type");
/// save_header_value(
///     &Header::new("content-Type", "text/text"),
///     &mut [&mut content_type]).unwrap();
/// assert_eq!("content-type:text/text", format!("{}", content_type));
///
/// let mut cookie = HeaderReceiver::new("Cookie");
/// assert_eq!("cookie:...", format!("{}", cookie)); // empty
/// save_header_value(
///     &Header::new("cookie", "secret1"),
///     &mut [&mut cookie]).unwrap();
/// assert_eq!("cookie:...", format!("{}", cookie));
/// ```
///
/// [`std::fmt::Debug`] trait impl includes header values.
/// ```
/// # use beatrice_http::{save_header_value, Header, HeaderReceiver};
/// let mut cookie = HeaderReceiver::new("Cookie");
/// save_header_value(
///     &Header::new("cookie", "secret1"),
///     &mut [&mut cookie]
/// ).unwrap();
/// assert_eq!("HeaderReceiver{cookie:secret1}", format!("{:?}", cookie));
/// ```
pub struct HeaderReceiver<'a> {
    pub name: &'a str,
    pub value: StringWrapper<[u8; 256]>,
}

impl<'a> HeaderReceiver<'a> {
    /// Creates a new empty receiver.
    pub fn new(name: &str) -> HeaderReceiver {
        HeaderReceiver {
            name,
            value: StringWrapper::from_str(""),
        }
    }

    /// Returns true if the header contains the value "chunked".
    ///
    /// Returns false if the header value is empty.
    ///
    /// Returns `HttpError::Parsing(ParsingError::TransferEncodingHeaderInvalid)`
    /// if the value is not empty and is not "chunked".
    ///
    /// Panics if the header name is not "transfer-encoding".
    pub fn is_chunked(&self) -> Result<bool, HttpError> {
        if !self.name.eq_ignore_ascii_case("transfer-encoding") {
            panic!(
                "is_chunked() called on {:?} header",
                self.name.to_ascii_lowercase()
            );
        }
        if self.value.is_empty() {
            return Ok(false);
        }
        if self.value.eq_ignore_ascii_case("chunked") {
            return Ok(true);
        }
        Err(HttpError::Parsing(
            ParsingError::TransferEncodingHeaderInvalid,
        ))
    }

    /// Returns true if the header contains the value "100-continue".
    ///
    /// Returns false if the header value is empty.
    ///
    /// Returns `HttpError::Parsing(ParsingError::TransferEncodingHeaderInvalid)`
    /// if the value is not empty and is not "100-continue".
    ///
    /// Panics if the header name is not "expect".
    pub fn is_100_continue(&self) -> Result<bool, HttpError> {
        // HTTP/1.1 Expect https://tools.ietf.org/html/rfc7231#section-5.1.1
        if !self.name.eq_ignore_ascii_case("expect") {
            panic!(
                "is_100_continue() called on {:?} header",
                self.name.to_ascii_lowercase()
            );
        }
        if self.value.is_empty() {
            return Ok(false);
        }
        if self.value.eq_ignore_ascii_case("100-continue") {
            return Ok(true);
        }
        Err(HttpError::Parsing(ParsingError::ExpectHeaderInvalid))
    }

    /// Parses the header value as an decimal integer.
    ///
    /// Returns 0 if the header value is empty.
    ///
    /// Returns `HttpError::Parsing(ParsingError::ContentLengthHeaderInvalid)`
    /// if the value is not empty and is not a valid decimal integer.
    ///
    /// Panics if the header name is not "content-length".
    pub fn parse_content_length(&self) -> Result<u64, HttpError> {
        if !self.name.eq_ignore_ascii_case("content-length") {
            panic!(
                "parse_content_length() called on {:?} header",
                self.name.to_ascii_lowercase()
            );
        }
        if self.value.is_empty() {
            return Ok(0);
        }
        let content_length: u64 = std::str::FromStr::from_str(&self.value)
            .map_err(|_e| HttpError::Parsing(ParsingError::ContentLengthHeaderInvalid))?;
        Ok(content_length)
    }
}

impl<'a> std::fmt::Display for HeaderReceiver<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fmt(f, self.name, &*self.value)
    }
}

impl<'a> std::fmt::Debug for HeaderReceiver<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "HeaderReceiver{{{}:{}}}",
            self.name.to_ascii_lowercase(),
            self.value
        )
    }
}

/// Formats the slice using [`std::fmt::Display`].
pub fn header_receivers_to_string(header_receivers: &[&HeaderReceiver]) -> String {
    let header_strings: Vec<String> = header_receivers.iter().map(|&hr| hr.to_string()).collect();
    "[".to_string() + &header_strings.join(", ") + "]"
}

/// Looks in `receivers` for a header with `name` and then overwrites its value with `value`.
///
/// If `value` is too long to fit, copies the initial part that does fit, and then returns
/// `HttpError::Parsing(ParsingError::HeaderValueTooLong))`.
pub fn save_header_value(
    header: &Header<'_>,
    receivers: &mut [&mut HeaderReceiver],
) -> Result<(), HttpError> {
    // For-loops call .iter() and cannot mutate the returned reference:
    // ```
    // for header in headers {...}  // error[E0382]: use of moved value: `headers`
    // ```
    // So we use .iter_mut() instead:
    if let Some(receiver) = receivers
        .iter_mut()
        .find(|r| header.name.eq_ignore_ascii_case(r.name))
    {
        receiver.value.truncate(0);
        receiver
            .value
            .push_partial_str(header.value)
            .or(Err(HttpError::Parsing(ParsingError::HeaderValueTooLong)))?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::unwrap_panic;

    #[test]
    fn test_header_parse() {
        assert_eq!(
            HttpError::Parsing(ParsingError::HeaderLineInvalid),
            Header::parse(b"").unwrap_err()
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::HeaderLineInvalid),
            Header::parse(b"Header1: \xC2 is a an incomplete UTF-8 symbol").unwrap_err()
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::HeaderLineInvalid),
            Header::parse(b"Not a valid header line").unwrap_err()
        );
        assert_eq!(
            Header::new("Name1", "Value1"),
            Header::parse(b"Name1:Value1").unwrap()
        );
        assert_eq!(
            Header::new("Name1", "Value1"),
            Header::parse(b"Name1: \tValue1 ").unwrap()
        );
    }

    #[test]
    fn test_is_non_pii_header() {
        assert!(is_non_pii_header("transfer-encoding"));
        assert!(is_non_pii_header("content-type"));
        assert!(is_non_pii_header("content-length"));
        assert!(is_non_pii_header("content-encoding"));
        assert!(is_non_pii_header("Content-Encoding"));
    }

    #[test]
    fn test_header_display_and_debug() {
        // Non-PII header
        let empty_content_type = Header::new("Content-Type", "");
        assert_eq!("content-type:", format!("{}", empty_content_type));
        assert_eq!("Header{content-type:}", format!("{:?}", empty_content_type));
        let content_type = Header::new("Content-Type", "Text/text");
        assert_eq!("content-type:Text/text", format!("{}", content_type));
        assert_eq!(
            "Header{content-type:Text/text}",
            format!("{:?}", content_type)
        );

        // PII header
        let empty_cookie = Header::new("Cookie", "");
        assert_eq!("cookie:...", format!("{}", empty_cookie));
        assert_eq!("Header{cookie:}", format!("{:?}", empty_cookie));
        let cookie = Header::new("Cookie", "Secret1");
        assert_eq!("cookie:...", format!("{}", cookie));
        assert_eq!("Header{cookie:Secret1}", format!("{:?}", cookie));
    }

    #[test]
    fn test_headers_to_string() {
        assert_eq!("[]", headers_to_string(&[]));
        assert_eq!(
            "[content-type:Text/text, name1:..., name2:...]",
            headers_to_string(&[
                &Header::new("Content-Type", "Text/text"),
                &Header::new("name1", "value1"),
                &Header::new("name2", "value2")
            ])
        );
    }

    #[test]
    fn test_header_receiver_is_chunked() {
        let mut cookie_hr = HeaderReceiver::new("Cookie");
        assert_eq!(
            "is_chunked() called on \"cookie\" header",
            unwrap_panic(|| cookie_hr.is_chunked())
        );
        let mut transfer_encoding_hr = HeaderReceiver::new("Transfer-Encoding");
        assert!(!transfer_encoding_hr.is_chunked().unwrap()); // empty
        save_header_value(
            &Header::new("transfer-Encoding", "chunked"),
            &mut [&mut cookie_hr, &mut transfer_encoding_hr],
        )
        .unwrap();
        assert!(transfer_encoding_hr.is_chunked().unwrap()); // "chunked"
        save_header_value(
            &Header::new("transfer-Encoding", "not-chunked"),
            &mut [&mut cookie_hr, &mut transfer_encoding_hr],
        )
        .unwrap();
        assert_eq!(
            HttpError::Parsing(ParsingError::TransferEncodingHeaderInvalid,),
            transfer_encoding_hr.is_chunked().unwrap_err() // not "chunked"
        );
    }

    #[test]
    fn test_header_receiver_is_100_continue() {
        let mut cookie_hr = HeaderReceiver::new("Cookie");
        assert_eq!(
            "is_100_continue() called on \"cookie\" header",
            unwrap_panic(|| cookie_hr.is_100_continue())
        );
        let mut expect_hr = HeaderReceiver::new("Expect");
        assert!(!expect_hr.is_100_continue().unwrap()); // empty
        save_header_value(
            &Header::new("Expect", "100-Continue"),
            &mut [&mut cookie_hr, &mut expect_hr],
        )
        .unwrap();
        assert!(expect_hr.is_100_continue().unwrap()); // valid
        save_header_value(
            &Header::new("Expect", "100-continue"),
            &mut [&mut cookie_hr, &mut expect_hr],
        )
        .unwrap();
        assert!(expect_hr.is_100_continue().unwrap()); // valid
        save_header_value(
            &Header::new("Expect", "not-100-continue"),
            &mut [&mut cookie_hr, &mut expect_hr],
        )
        .unwrap();
        assert_eq!(
            HttpError::Parsing(ParsingError::ExpectHeaderInvalid,),
            expect_hr.is_100_continue().unwrap_err() // invalid
        );
    }

    #[test]
    fn test_parse_content_length() {
        let mut cookie_hr = HeaderReceiver::new("Cookie");
        assert_eq!(
            "parse_content_length() called on \"cookie\" header",
            unwrap_panic(|| cookie_hr.parse_content_length())
        );
        let mut content_length_hr = HeaderReceiver::new("Content-Length");

        assert_eq!(0, content_length_hr.parse_content_length().unwrap());

        save_header_value(
            &Header::new("Content-LENGTH", "0"),
            &mut [&mut cookie_hr, &mut content_length_hr],
        )
        .unwrap();
        assert_eq!(0, content_length_hr.parse_content_length().unwrap());

        save_header_value(
            &Header::new("content-length", "42"),
            &mut [&mut cookie_hr, &mut content_length_hr],
        )
        .unwrap();
        assert_eq!(42, content_length_hr.parse_content_length().unwrap());

        // 0xFFFF_FFFF_FFFF_FFFF, max u64 value
        save_header_value(
            &Header::new("content-length", "18446744073709551615"),
            &mut [&mut cookie_hr, &mut content_length_hr],
        )
        .unwrap();
        assert_eq!(
            0xFFFF_FFFF_FFFF_FFFF,
            content_length_hr.parse_content_length().unwrap()
        );

        save_header_value(
            &Header::new("content-length", "not-a-decimal-number"),
            &mut [&mut cookie_hr, &mut content_length_hr],
        )
        .unwrap();
        assert_eq!(
            HttpError::Parsing(ParsingError::ContentLengthHeaderInvalid),
            content_length_hr.parse_content_length().unwrap_err()
        );
    }

    #[test]
    fn test_header_receiver_display_and_debug() {
        // Non-PII header
        let mut content_type_hr = HeaderReceiver::new("Content-Type");
        assert_eq!("content-type:", format!("{}", content_type_hr));
        assert_eq!(
            "HeaderReceiver{content-type:}",
            format!("{:?}", content_type_hr)
        );
        save_header_value(
            &Header::new("content-Type", "Text/text"),
            &mut [&mut content_type_hr],
        )
        .unwrap();
        assert_eq!("content-type:Text/text", format!("{}", content_type_hr));
        assert_eq!(
            "HeaderReceiver{content-type:Text/text}",
            format!("{:?}", content_type_hr)
        );

        // PII header
        let mut cookie_hr = HeaderReceiver::new("Cookie");
        assert_eq!("cookie:...", format!("{}", cookie_hr));
        assert_eq!("HeaderReceiver{cookie:}", format!("{:?}", cookie_hr));
        save_header_value(&Header::new("cookie", "secret1"), &mut [&mut cookie_hr]).unwrap();
        assert_eq!("cookie:...", format!("{}", cookie_hr));
        assert_eq!("HeaderReceiver{cookie:secret1}", format!("{:?}", cookie_hr));
    }

    #[test]
    fn test_header_receivers_to_string() {
        assert_eq!("[]", header_receivers_to_string(&[]));

        let mut content_type_hr = HeaderReceiver::new("Content-Type");
        save_header_value(
            &Header::new("content-Type", "Text/text"),
            &mut [&mut content_type_hr],
        )
        .unwrap();
        let mut cookie_hr = HeaderReceiver::new("Cookie");
        save_header_value(&Header::new("cookie", "secret1"), &mut [&mut cookie_hr]).unwrap();
        let mut header1 = HeaderReceiver::new("header1");
        assert_eq!(
            "[content-type:Text/text, cookie:..., header1:...]",
            header_receivers_to_string(&[&mut content_type_hr, &mut cookie_hr, &mut header1])
        );
    }

    #[test]
    fn test_save_header_value() {
        // Empty receiver list
        save_header_value(&Header::new("header1", "value1"), &mut []).unwrap();
        let mut header1 = HeaderReceiver::new("Header1");
        let mut header2 = HeaderReceiver::new("header2");
        // Header not found
        save_header_value(
            &Header::new("header3", "value3"),
            &mut [&mut header1, &mut header2],
        )
        .unwrap();
        assert_eq!("", &*header1.value);
        assert_eq!("", &*header2.value);
        // Header found
        save_header_value(
            &Header::new("header2", "value2"),
            &mut [&mut header1, &mut header2],
        )
        .unwrap();
        assert_eq!("", &*header1.value);
        assert_eq!("value2", &*header2.value);
        // Header found
        save_header_value(
            &Header::new("header1", "value1"),
            &mut [&mut header1, &mut header2],
        )
        .unwrap();
        assert_eq!("value1", &*header1.value);
        assert_eq!("value2", &*header2.value);
        // Replace existing value
        save_header_value(
            &Header::new("header1", "value4"),
            &mut [&mut header1, &mut header2],
        )
        .unwrap();
        assert_eq!("value4", &*header1.value);
        assert_eq!("value2", &*header2.value);
        // Replace existing value with empty value
        save_header_value(
            &Header::new("header1", ""),
            &mut [&mut header1, &mut header2],
        )
        .unwrap();
        assert_eq!("", &*header1.value);
        assert_eq!("value2", &*header2.value);
        // Value max length
        let max_len = "x".repeat(256);
        save_header_value(
            &Header::new("header1", &max_len),
            &mut [&mut header1, &mut header2],
        )
        .unwrap();
        assert_eq!(&max_len, &*header1.value);
        assert_eq!("value2", &*header2.value);
        // Value too long
        let too_long = "x".repeat(257);
        assert_eq!(
            HttpError::Parsing(ParsingError::HeaderValueTooLong),
            save_header_value(
                &Header::new("header1", &too_long),
                &mut [&mut header1, &mut header2]
            )
            .unwrap_err()
        );
        assert_eq!(&max_len, &*header1.value); // Unchanged
        assert_eq!("value2", &*header2.value);
    }
}
