#![forbid(unsafe_code)]
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::time::Duration;
use tokio::signal::unix;
use tokio::sync::mpsc;

/// Stopper helps you gracefully shut down a server that is serving clients.
///
/// Killing a server process has downsides:
/// - Requests with multiple steps will stop in the middle.
///   - The server may leave data in an inconsistent state.
///   - After restart, the server may repeat some operations, like sending an email.
///   - When there are bugs, the restarted server may skip operations or lose data.
/// - Clients doing big uploads or downloads get errors and will have to start over.
/// - Log messages and metrics get lost.
///
/// Avoid those problems by shutting down your server gracefully, like this:
/// 1. First stop accepting new connections.
/// 1. Signal all request handlers and background jobs to stop.
///    They can reach a safe stopping place and then stop.
/// 1. Wait a bit to give them time to see the signal and respond.
/// 1. Finally, kill the server process.
///
/// Call `stopper::new` and get a `Stopper` and `StopperController`.
///
/// Clone the `Stopper` and give one to every task.
/// Tasks can call [`Stopper.signalled`] to see if they need to shut down.
/// Tasks can use [`Stopper.wait`] in a `tokio::select!` block to wait for
/// long-running operations and abort immediately when the `Stopper` is signalled.
///
/// Tasks drop the `Stopper` struct to signal that they are shutting down.
/// This usually happens when the task's async fn returns.
///
/// The server's main task can wait for a shutdown signal (`SIGTERM`),
/// call `StopperController.graceful_shutdown`,
/// and then stop the server process.
///
/// Example:
/// ```
/// # use beatrice_http::Stopper;
/// # async fn server_loop(_: tokio::net::TcpListener, _: Stopper,) {}
/// # async fn f(listener: tokio::net::TcpListener) {
/// let (mut stopper_ctl, stopper) = beatrice_http::StopperController::new();
/// tokio::spawn(server_loop(listener, stopper));
/// beatrice_http::wait_for_shutdown_signal().await;
/// stopper_ctl.graceful_shutdown().await;
/// # }
/// ```
///
/// [`Stopper.signalled`]: #method.signalled
/// [`Stopper.wait`]: #method.wait
#[derive(Clone)]
pub struct Stopper {
    // Closed channel means "stop".
    // Open channel means "don't stop".
    // Constructor fills the channel.
    signal_channel_closed_means_stop: mpsc::Sender<()>,
    // Each Stopper has a clone of this.
    running_task_counter: mpsc::Sender<()>,
}

impl Stopper {
    /// Waits until the stopper is signalled.
    /// Returns immediately if the stopper is already signalled.
    pub async fn wait(&mut self) {
        // The channel is always full, so send() waits until the channel closes.
        let _ = self.signal_channel_closed_means_stop.send(()).await;
    }

    /// Returns true if the stopper is signalled, otherwise false.
    pub fn signalled(&mut self) -> bool {
        #[allow(clippy::match_like_matches_macro)]
        match self.signal_channel_closed_means_stop.try_send(()) {
            Err(mpsc::error::TrySendError::Closed(_)) => true,
            _ => false,
        }
    }
}

#[derive(Debug)]
pub struct TimeoutError;

impl Display for TimeoutError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "TimeoutError")
    }
}

impl Error for TimeoutError {}

/// A struct to signal associated `Stopper` structs and wait for them to be dropped.
pub struct StopperController {
    signal_channel_closed_means_stop: mpsc::Receiver<()>,
    running_task_counter: mpsc::Receiver<()>,
}

impl StopperController {
    /// Makes a new linked `StopperController` and cloneable `Stopper`.
    pub fn new() -> (StopperController, Stopper) {
        let (signal_tx, signal_rx) = mpsc::channel(1);
        signal_tx.try_send(()).unwrap(); // Fill the channel so senders wait.
        let (running_tx, running_rx) = mpsc::channel(1);
        (
            StopperController {
                signal_channel_closed_means_stop: signal_rx,
                running_task_counter: running_rx,
            },
            Stopper {
                signal_channel_closed_means_stop: signal_tx,
                running_task_counter: running_tx,
            },
        )
    }

    /// Signals all associated `Stopper` structs and then waits for them to be dropped.
    /// Returns after 1 second if not all `Stopper` structs were dropped.
    pub async fn graceful_shutdown(&mut self) {
        self.signal_stop();
        let _ = self
            .wait_until_all_stoppers_dropped(Some(Duration::from_secs(1)))
            .await;
    }

    /// Signals all associated `Stopper` structs.
    /// Calls to `Stopper.wait` return right away.
    /// `Stopper.signalled` returns true.
    pub fn signal_stop(&mut self) {
        self.signal_channel_closed_means_stop.close();
    }

    /// Waits for all associated `Stopper` structs to be dropped.
    /// Use this to wait for tasks to notice the "stop" signal and shut down.
    pub async fn wait_until_all_stoppers_dropped(
        &mut self,
        timeout: Option<Duration>,
    ) -> Result<(), TimeoutError> {
        if let Some(duration) = timeout {
            tokio::select! {
                _ = tokio::time::sleep(duration) => Err(TimeoutError),
                // No message is ever sent on the channel,
                // so recv() waits until the channel closes and returns None.
                // The channel automatically closes when the last Sender is dropped.
                // This happens when the last Stopper is dropped.
                _ = self.running_task_counter.recv() => Ok(()),
            }
        } else {
            // Wait without timeout.
            self.running_task_counter.recv().await;
            Ok(())
        }
    }
}

#[cfg(unix)]
/// Wait for process shutdown signals:
/// - TERM signal from process managers like Docker, Kubernetes, supervisord, etc.
/// - INT signal from CTRL-C in terminal.
///
/// Panics if it fails to install the signal handlers.
///
/// On Windows, call `tokio::signal::ctrl_c` instead of this function.
pub async fn wait_for_shutdown_signal() {
    let sigterm_stream = unix::signal(unix::SignalKind::terminate()).unwrap();
    let sigint_stream = unix::signal(unix::SignalKind::interrupt()).unwrap();
    let mut sig_stream = tokio::stream::StreamExt::merge(sigterm_stream, sigint_stream);
    tokio::stream::StreamExt::next(&mut sig_stream).await;
}
