#[cfg(test)]
pub fn unwrap_panic<R: std::fmt::Debug, F: FnOnce() -> R + std::panic::UnwindSafe>(f: F) -> String {
    let any = std::panic::catch_unwind(f).unwrap_err();
    if let Some(s) = any.downcast_ref::<String>() {
        s.clone()
    } else if let Some(s) = any.downcast_ref::<&str>() {
        String::from(*s)
    } else {
        format!("{:?}", any)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_unwrap_panic() {
        assert_eq!("msg1", unwrap_panic(|| panic!("msg1")));
        assert_eq!(
            "msg1 value1",
            unwrap_panic(|| panic!(format!("msg1 {}", "value1")))
        );
        assert_eq!("Any", unwrap_panic(|| panic!(42u8)));
    }
}
