use crate::wire;

use std::task::Context;

use tokio::io::{AsyncRead, ReadBuf};
use tokio::macros::support::{Pin, Poll};

pub struct AsyncReadLogger<'a> {
    inner: &'a mut (dyn tokio::io::AsyncRead + Send + Unpin),
}

impl<'a> AsyncReadLogger<'a> {
    pub fn new(inner: &'a mut (dyn tokio::io::AsyncRead + Send + Unpin)) -> AsyncReadLogger<'a> {
        AsyncReadLogger { inner }
    }
}

impl<'a> AsyncRead for AsyncReadLogger<'a> {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        let filled_before = buf.filled().len();
        match Pin::new(&mut self.get_mut().inner).poll_read(cx, buf) {
            Poll::Ready(Ok(())) => {
                let bytes_read = &buf.filled()[filled_before..];
                wire::read::trace(bytes_read);
                Poll::Ready(Ok(()))
            }
            Poll::Ready(Err(e)) => {
                wire::read::trace_err(&e);
                Poll::Ready(Err(e))
            }
            other => other,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{capture_thread_logs, filter_log_entries, FakeAsyncRead, FakeAsyncReadAction};

    #[test]
    fn test_poll_read() {
        let mut inner = FakeAsyncRead::new(vec![
            FakeAsyncReadAction::Data(b"abc".to_vec()),
            FakeAsyncReadAction::Pending,
            FakeAsyncReadAction::Data(b"def".to_vec()),
        ]);
        let mut read_logger = AsyncReadLogger::new(&mut inner);
        let mut dest = String::new();
        let (_, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncReadExt::read_to_string(
                &mut read_logger,
                &mut dest,
            ))
            .unwrap()
        });
        assert_eq!("abcdef", dest);
        //println!("entries={:?}", entries);
        assert_eq!(
            &["read \"abc\"", "read \"def\"", "read EOF"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }

    #[test]
    fn test_poll_read_error() {
        let mut inner = FakeAsyncRead::new(vec![FakeAsyncReadAction::Error(std::io::Error::new(
            std::io::ErrorKind::Other,
            "err1",
        ))]);
        let mut read_logger = AsyncReadLogger::new(&mut inner);
        let (err, entries) = capture_thread_logs(|| {
            tokio_test::block_on(tokio::io::AsyncReadExt::read_to_string(
                &mut read_logger,
                &mut String::new(),
            ))
            .unwrap_err()
        });
        assert_eq!(std::io::ErrorKind::Other, err.kind());
        assert_eq!("err1", err.to_string());
        assert_eq!(
            &["read error: err1"],
            filter_log_entries(&entries, log::Level::Trace, "beatrice_http::wire").as_slice()
        );
    }
}
