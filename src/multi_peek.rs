#![forbid(unsafe_code)]
/// Iterator wrapper that lets you `peek` multiple times.
/// Calling `next` resets the peeking.

pub struct MultiPeek<T: Copy, I: Iterator<Item = T>, B: AsRef<[T]> + AsMut<[T]>> {
    inner: I,
    buffer: B,
    len: usize,
    next_peek: usize,
}

impl<T, I, B> MultiPeek<T, I, B>
where
    T: Copy,
    I: Iterator<Item = T>,
    B: AsRef<[T]> + AsMut<[T]> + Clone,
{
    /// Creates a new iterator wrapper using `buffer`.
    /// The struct ignores all inital values in `buffer`.
    ///
    /// Example:
    /// ```
    /// # let inner_iter = [1u8].iter().cloned();
    /// use beatrice_http::MultiPeek;
    /// let mut mp = MultiPeek::new(inner_iter, [0u8; 3]);
    /// ```
    pub fn new(inner: I, buffer: B) -> MultiPeek<T, I, B> {
        Self {
            inner,
            buffer,
            // The number of valid items in `buffer`.
            len: 0,
            // The index of the next item to return from `peek`.
            next_peek: 0,
        }
    }

    /// Returns the next value from the inner iterator, saving a copy.
    /// You can call this multiple times and will receive subsequent values.
    /// Returns None when the buffer is full or the inner iterator is exhausted.
    ///
    /// Calls to `peek` do not affect the results of subsequent calls to `Iterator::next`.
    ///
    /// Call `Iterator::next` to reset the iterator as if `peek` had never been called.
    ///
    /// # Example
    /// ```
    /// # use beatrice_http::MultiPeek;
    /// let mut mp = MultiPeek::new(['a', 'b', 'c', 'd'].iter().cloned(), ['.'; 3]);
    /// assert_eq!(Some('a'), mp.peek());
    /// assert_eq!(Some('b'), mp.peek());
    /// assert_eq!(Some('c'), mp.peek());
    /// assert_eq!(None, mp.peek()); // Buffer is full.
    ///
    /// assert_eq!(Some('a'), mp.next()); // Resets the peeking.
    /// assert_eq!(Some('b'), mp.peek());
    ///
    /// assert_eq!(Some('b'), mp.next());
    /// assert_eq!(Some('c'), mp.peek());
    /// assert_eq!(Some('d'), mp.peek());
    /// assert_eq!(None, mp.peek()); // Inner iterator is empty.
    ///
    /// assert_eq!(Some('c'), mp.next());
    /// assert_eq!(Some('d'), mp.next());
    /// assert_eq!(None, mp.next());
    /// assert_eq!(None, mp.peek());
    /// ```
    pub fn peek(&mut self) -> Option<T> {
        if self.next_peek < self.len {
            // Buffer has an un-peeked item.
            let item = self.buffer.as_ref()[self.next_peek];
            self.next_peek += 1;
            Some(item)
        } else if self.len == self.buffer.as_ref().len() {
            // Buffer is full.
            None
        } else {
            assert_eq!(self.len, self.next_peek);
            assert!(self.len < self.buffer.as_ref().len());
            let item = self.inner.next()?;
            self.buffer.as_mut()[self.len] = item;
            self.len += 1;
            self.next_peek += 1;
            Some(item)
        }
    }
}

impl<T, I, B> Iterator for MultiPeek<T, I, B>
where
    T: Copy,
    I: Iterator<Item = T>,
    B: AsRef<[T]> + AsMut<[T]> + Clone,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.len == 0 {
            // Buffer is empty.
            assert_eq!(self.len, self.next_peek);
            self.inner.next()
        } else {
            // Remove the first item from the buffer and reset `next_peek`.
            let item = self.buffer.as_ref()[0];
            self.buffer.as_mut().copy_within(1..self.len, 0);
            self.len -= 1;
            self.next_peek = 0;
            Some(item)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty() {
        let mut mp = MultiPeek::new(std::iter::empty(), [(); 3]);
        assert_eq!(None, mp.peek());
        assert_eq!(None, mp.next());
    }

    #[test]
    fn test_zero_len_buffer() {
        let mut mp = MultiPeek::new([1u8, 2u8, 3u8].iter().cloned(), [0u8; 0]);
        assert_eq!(None, mp.peek());
        assert_eq!(Some(1u8), mp.next());
        assert_eq!(None, mp.peek());
        assert_eq!(Some(2u8), mp.next());
        assert_eq!(Some(3u8), mp.next());
        assert_eq!(None, mp.next());
        assert_eq!(None, mp.peek());
    }

    #[test]
    fn test_peek() {
        let mut mp = MultiPeek::new([1u8, 2u8, 3u8, 4u8].iter().cloned(), [0u8; 3]);
        assert_eq!(Some(1u8), mp.peek());
        assert_eq!(Some(2u8), mp.peek());
        assert_eq!(Some(3u8), mp.peek()); // buffered: [], peeked [1,2,3]

        assert_eq!(Some(1u8), mp.next()); // buffered: [2,3], peeked []
        assert_eq!(Some(2u8), mp.peek()); // buffered: [3], peeked [2]

        assert_eq!(Some(2u8), mp.next()); // buffered: [2,3] then [3], peeked []
        assert_eq!(Some(3u8), mp.peek()); // buffered: [], peeked [3]
    }

    #[test]
    fn test_next_restarts() {
        let mut mp = MultiPeek::new([1u8, 2u8].iter().cloned(), [0u8; 3]);
        assert_eq!(Some(1u8), mp.peek());
        assert_eq!(Some(2u8), mp.peek());
        assert_eq!(Some(1u8), mp.next());
        assert_eq!(Some(2u8), mp.peek());
    }

    #[test]
    fn test_peek_all() {
        let mut mp = MultiPeek::new([1u8, 2u8, 3u8, 4u8, 5u8].iter().cloned(), [0u8; 3]);
        assert_eq!(Some(1u8), mp.peek());
        assert_eq!(Some(2u8), mp.peek());
        assert_eq!(Some(3u8), mp.peek());
        assert_eq!(None, mp.peek());

        assert_eq!(Some(1u8), mp.next());
        assert_eq!(Some(2u8), mp.peek());
        assert_eq!(Some(3u8), mp.peek());
        assert_eq!(Some(4u8), mp.peek());
        assert_eq!(None, mp.peek());

        assert_eq!(Some(2u8), mp.next());
        assert_eq!(Some(3u8), mp.peek());
        assert_eq!(Some(4u8), mp.peek());
        assert_eq!(Some(5u8), mp.peek());
        assert_eq!(None, mp.peek());

        assert_eq!(Some(3u8), mp.next());
        assert_eq!(Some(4u8), mp.peek());
        assert_eq!(Some(5u8), mp.peek());
        assert_eq!(None, mp.peek());

        assert_eq!(Some(4u8), mp.next());
        assert_eq!(Some(5u8), mp.peek());
        assert_eq!(None, mp.peek());

        assert_eq!(Some(5u8), mp.next());
        assert_eq!(None, mp.peek());

        assert_eq!(None, mp.next());
        assert_eq!(None, mp.peek());
    }
}
