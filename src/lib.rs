#![forbid(unsafe_code)]
//! Detailed docs are in `Readme.md`.
mod array_queue;
pub use array_queue::{ArrayQueue, ArrayQueueFullError};

mod async_read_logger;
pub use async_read_logger::AsyncReadLogger;

mod async_write_logger;
pub use async_write_logger::AsyncWriteLogger;

mod chain;
pub use chain::ChainAsyncRead;

mod chunked;
pub use chunked::{
    chunk_foot, chunk_head, read_chunk_final_crlf, read_chunk_head, read_chunked_body_foot,
};

mod error;
pub use error::{HttpError, ParsingError};

mod fake_read;
pub use fake_read::{FakeRead, FakeReadAction};

mod header;
pub use header::{
    header_receivers_to_string, headers_to_string, is_non_pii_header, save_header_value, Header,
    HeaderReceiver,
};

mod hex;
pub use hex::{hex_digit, is_hex_digit, write_hex};

mod http;
pub use http::{connection_reuse_deadline, log_and_finish, log_error, log_if_err, log_result};

mod logging;
pub use logging::{id_task_scope, random_id};

mod method;
pub use method::HttpMethod;

mod multi_peek;
pub use multi_peek::MultiPeek;

mod percent_decode;
pub use percent_decode::{
    decode_uppercase_hex_byte, percent_decode, percent_decode_to_string, uppercase_hex_digit,
};

mod pinned_server_cert_verifier;
pub use pinned_server_cert_verifier::{arbitrary_dns_name, PinnedServerCertVerifier};

mod request;
pub use request::HttpRequest;

mod request_line;
pub use request_line::HttpRequestLine;

mod split_iterate;
pub use split_iterate::{split_iterate, SplitIterator};

mod status;
pub use status::HttpStatus;

mod stopper;
#[cfg(unix)]
pub use stopper::wait_for_shutdown_signal;
pub use stopper::{Stopper, StopperController};

mod tcp;
pub use tcp::{handle_accept_error, listen, Accepter};

mod tls;
pub use tls::{
    listen_tls_any_client, listen_tls_self_signed, parse_cert_chain_pem, parse_key_pem,
    read_cert_chain_pem_file, read_file_to_string, read_key_pem_file, tls_handshake,
};

mod try_push;
pub use try_push::TryPush;

mod reader_writer;
pub use reader_writer::{Conn, HttpReaderWriter};

mod headers {
    pub mod read {
        pub fn trace(data: &[u8]) {
            log::trace!("read headers {:?}", fixed_buffer::escape_ascii(data));
        }
    }
    pub mod write {
        pub fn trace(data: &[u8]) {
            log::trace!("write headers {:?}", fixed_buffer::escape_ascii(data));
        }
    }
}

mod body {
    pub mod read {
        pub fn trace(data: &[u8]) {
            if data.is_empty() {
                log::trace!("read body EOF");
            } else {
                log::trace!("read body {:?}", fixed_buffer::escape_ascii(data));
            }
        }
    }
    pub mod write {
        pub fn trace(data: &[u8]) {
            log::trace!("write body {:?}", fixed_buffer::escape_ascii(data));
        }
    }
}

mod wire {
    pub mod read {
        pub fn trace(data: &[u8]) {
            if data.is_empty() {
                log::trace!("read EOF");
            } else {
                log::trace!("read {:?}", fixed_buffer::escape_ascii(data));
            }
        }
        pub fn trace_err(e: &std::io::Error) {
            log::trace!("read error: {}", e);
        }
    }
    pub mod write {
        pub fn trace(data: &[u8]) {
            log::trace!("write {:?}", fixed_buffer::escape_ascii(data));
        }
        pub fn trace_err(e: &std::io::Error) {
            log::trace!("write error: {}", e);
        }
        pub fn trace_flush() {
            log::trace!("write flush");
        }
        pub fn trace_shutdown() {
            log::trace!("write shutdown");
        }
    }
}

#[cfg(test)]
mod fake_async_read;
#[cfg(test)]
pub use fake_async_read::{FakeAsyncRead, FakeAsyncReadAction};

#[cfg(test)]
mod fake_async_write;
#[cfg(test)]
pub use fake_async_write::{FakeAsyncWrite, FakeAsyncWriteAction};

mod logging_tester;
pub use logging_tester::{
    capture_thread_logs, filter_log_entries, install_logging_tester_logger, LogEntry,
};

#[cfg(test)]
mod test_utils;
#[cfg(test)]
pub use test_utils::unwrap_panic;
