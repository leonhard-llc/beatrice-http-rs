#![forbid(unsafe_code)]

/// An HTTP status code to send back to the client and log.
#[derive(Debug, Clone, PartialEq)]
pub enum HttpStatus {
    Continue100,
    Ok200,
    Created201,
    BadRequest400,
    NotFound404,
    MethodNotAllowed405,
    LengthRequired411,
    PayloadTooLarge413,
    UriTooLong414,
    RequestHeaderFieldsTooLarge431,
    InternalServerError500(String),
    ServiceUnavailable503,
}

impl HttpStatus {
    pub fn as_str(&self) -> &'static str {
        match self {
            HttpStatus::Continue100 => "HTTP/1.1 100 Continue",
            HttpStatus::Ok200 => "HTTP/1.1 200 OK",
            HttpStatus::Created201 => "HTTP/1.1 201 Created",
            HttpStatus::BadRequest400 => "HTTP/1.1 400 Bad Request",
            HttpStatus::NotFound404 => "HTTP/1.1 404 Not Found",
            HttpStatus::MethodNotAllowed405 => "HTTP/1.1 405 Method Not Allowed",
            HttpStatus::LengthRequired411 => "HTTP/1.1 411 Length Required",
            HttpStatus::PayloadTooLarge413 => "HTTP/1.1 413 Payload Too Large",
            HttpStatus::UriTooLong414 => "HTTP/1.1 414 URI Too Long",
            HttpStatus::RequestHeaderFieldsTooLarge431 => {
                "HTTP/1.1 431 Request Header Fields Too Large"
            }
            HttpStatus::InternalServerError500(_) => "HTTP/1.1 500 Internal Server Error",
            HttpStatus::ServiceUnavailable503 => "HTTP/1.1 503 Service Unavailable",
        }
    }

    pub fn code(&self) -> u16 {
        match self {
            HttpStatus::Continue100 => 100,
            HttpStatus::Ok200 => 200,
            HttpStatus::Created201 => 201,
            HttpStatus::BadRequest400 => 400,
            HttpStatus::NotFound404 => 404,
            HttpStatus::MethodNotAllowed405 => 405,
            HttpStatus::LengthRequired411 => 411,
            HttpStatus::PayloadTooLarge413 => 413,
            HttpStatus::UriTooLong414 => 414,
            HttpStatus::RequestHeaderFieldsTooLarge431 => 431,
            HttpStatus::InternalServerError500(_) => 500,
            HttpStatus::ServiceUnavailable503 => 503,
        }
    }

    pub fn reason(&self) -> &'static str {
        match self {
            HttpStatus::Continue100 => "continue",
            HttpStatus::Ok200 => "ok",
            HttpStatus::Created201 => "created",
            HttpStatus::BadRequest400 => "bad request",
            HttpStatus::NotFound404 => "not found",
            HttpStatus::MethodNotAllowed405 => "method not allowed",
            HttpStatus::LengthRequired411 => "length required",
            HttpStatus::PayloadTooLarge413 => "payload too large",
            HttpStatus::UriTooLong414 => "uri too long",
            HttpStatus::RequestHeaderFieldsTooLarge431 => "request header fields too large",
            HttpStatus::InternalServerError500(_) => "internal server error",
            HttpStatus::ServiceUnavailable503 => "service unavailable",
        }
    }
}

impl std::fmt::Display for HttpStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{} {:?}", self.code(), self.reason())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_display() {
        assert_eq!("100 \"continue\"", format!("{}", HttpStatus::Continue100));
        assert_eq!("200 \"ok\"", format!("{}", HttpStatus::Ok200));
        assert_eq!("201 \"created\"", format!("{}", HttpStatus::Created201));
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", HttpStatus::BadRequest400)
        );
        assert_eq!("404 \"not found\"", format!("{}", HttpStatus::NotFound404));
        assert_eq!(
            "405 \"method not allowed\"",
            format!("{}", HttpStatus::MethodNotAllowed405)
        );
        assert_eq!(
            "411 \"length required\"",
            format!("{}", HttpStatus::LengthRequired411)
        );
        assert_eq!(
            "413 \"payload too large\"",
            format!("{}", HttpStatus::PayloadTooLarge413)
        );
        assert_eq!(
            "414 \"uri too long\"",
            format!("{}", HttpStatus::UriTooLong414)
        );
        assert_eq!(
            "431 \"request header fields too large\"",
            format!("{}", HttpStatus::RequestHeaderFieldsTooLarge431)
        );
        assert_eq!(
            "500 \"internal server error\"",
            format!("{}", HttpStatus::InternalServerError500("err1".to_string()))
        );
        assert_eq!(
            "503 \"service unavailable\"",
            format!("{}", HttpStatus::ServiceUnavailable503)
        );
    }
}
