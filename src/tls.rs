#![forbid(unsafe_code)]

use crate::{listen, log_if_err, Accepter, HttpError, StopperController};

fn invalid_data(msg: &str) -> std::io::Error {
    std::io::Error::new(std::io::ErrorKind::InvalidData, msg)
}

fn prefix_error(e: std::io::Error, prefix: &str) -> std::io::Error {
    std::io::Error::new(e.kind(), format!("{}: {}", prefix, e))
}

pub async fn read_file_to_string(path: &str) -> Result<String, std::io::Error> {
    use tokio::io::AsyncReadExt;
    let mut result = String::new();
    tokio::fs::File::open(path)
        .await
        .map_err(|e| prefix_error(e, path))?
        .take(10 * 1024 * 1024) // Don't OOM.
        .read_to_string(&mut result)
        .await
        .map_err(|e| prefix_error(e, path))?;
    Ok(result)
}

pub async fn read_cert_chain_pem_file(
    path: &str,
) -> Result<Vec<rustls::Certificate>, std::io::Error> {
    parse_cert_chain_pem(read_file_to_string(path).await?).map_err(|e| prefix_error(e, path))
}

pub fn parse_cert_chain_pem(pem_data: String) -> Result<Vec<rustls::Certificate>, std::io::Error> {
    let certs =
        rustls::internal::pemfile::certs(&mut std::io::Cursor::new(pem_data.as_bytes())).unwrap();
    if certs.is_empty() {
        Err(invalid_data("Found no certificates in file"))
    } else {
        Ok(certs)
    }
}

pub async fn read_key_pem_file(path: &str) -> Result<rustls::PrivateKey, std::io::Error> {
    parse_key_pem(read_file_to_string(path).await?).map_err(|e| prefix_error(e, path))
}

pub fn parse_key_pem(pem_data: String) -> Result<rustls::PrivateKey, std::io::Error> {
    let mut rsa_keys =
        rustls::internal::pemfile::rsa_private_keys(&mut std::io::Cursor::new(pem_data.as_bytes()))
            .unwrap();
    let mut p8_keys = rustls::internal::pemfile::pkcs8_private_keys(&mut std::io::Cursor::new(
        pem_data.as_bytes(),
    ))
    .unwrap();
    if rsa_keys.len() + p8_keys.len() > 1 {
        Err(invalid_data("Found multiple keys in file"))
    } else if !rsa_keys.is_empty() {
        Ok(rsa_keys.remove(0))
    } else if !p8_keys.is_empty() {
        Ok(p8_keys.remove(0))
    } else {
        Err(invalid_data("Found no keys in file"))
    }
}

pub async fn listen_tls_any_client(
    addr: &str,
    cert_pem_file_path: &str,
    key_pem_file_path: &str,
) -> (
    Accepter,
    StopperController,
    std::sync::Arc<rustls::ServerConfig>,
) {
    let cert = read_cert_chain_pem_file(cert_pem_file_path).await.unwrap();
    let key = read_key_pem_file(key_pem_file_path).await.unwrap();
    let mut server_config = rustls::ServerConfig::new(rustls::NoClientAuth::new());
    server_config
        .set_single_cert(cert, key)
        .map_err(|e| {
            invalid_data(&format!(
                "Error loading certs {:?} and key {:?}: {}",
                cert_pem_file_path, key_pem_file_path, e
            ))
        })
        .unwrap();
    let (accepter, stopper_ctl) = listen(addr).await.unwrap();
    (accepter, stopper_ctl, std::sync::Arc::new(server_config))
}

pub async fn listen_tls_self_signed(
    addr: &str,
    server_name: &str,
) -> (
    Accepter,
    StopperController,
    std::sync::Arc<rustls::ServerConfig>,
) {
    let (accepter, stopper_ctl) = listen(addr).await.unwrap();
    let rcgen_cert = rcgen::generate_simple_self_signed([String::from(server_name)]).unwrap();
    let cert = rustls::Certificate(rcgen_cert.serialize_der().unwrap());
    let key = rustls::PrivateKey(rcgen_cert.serialize_private_key_der());
    let mut server_config = rustls::ServerConfig::new(rustls::NoClientAuth::new());
    server_config.set_single_cert(vec![cert], key).unwrap();
    (accepter, stopper_ctl, std::sync::Arc::new(server_config))
}

pub async fn tls_handshake(
    tcp_stream: tokio::net::TcpStream,
    cfg: std::sync::Arc<rustls::ServerConfig>,
) -> Result<tokio_rustls::server::TlsStream<tokio::net::TcpStream>, HttpError> {
    log_if_err(
        tokio_rustls::TlsAcceptor::from(cfg)
            .accept(tcp_stream)
            .await
            .map_err(HttpError::from_io_err),
    )
}
