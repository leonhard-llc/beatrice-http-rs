#![forbid(unsafe_code)]
use crate::{HttpError, HttpReaderWriter, HttpRequest, HttpStatus};
use log::{debug, error, info, trace, warn};
use std::ops::Add;

pub async fn connection_reuse_deadline(start: &tokio::time::Instant) {
    tokio::time::sleep_until(start.add(std::time::Duration::from_secs(60))).await;
}

pub fn log_error(e: &HttpError) {
    match e {
        HttpError::Handling(HttpStatus::InternalServerError500(ref msg)) => {
            warn!("{}: {}", e, msg);
        }
        HttpError::Handling(ref e) => {
            warn!("{}", e);
        }
        HttpError::Parsing(ref e) => {
            debug!("{}", e);
        }
        HttpError::Io(ref e) => {
            debug!("{:?}{{{}}}", e.kind(), e);
        }
        HttpError::Stopped => {
            debug!("Stopped");
        }
        HttpError::Closed => {
            trace!("Closed");
        }
    }
}

pub fn log_result(_req: &HttpRequest, rw: &HttpReaderWriter, result: Result<(), HttpError>) {
    match result {
        Err(e) => log_error(&e),
        Ok(_) => {
            info!("{:?}", rw.sent_status());
        }
    }
}

pub fn log_if_err<T>(result: Result<T, HttpError>) -> Result<T, HttpError> {
    match result {
        Err(e) => {
            log_error(&e);
            Err(e)
        }
        Ok(result) => Ok(result),
    }
}

/// Logs the request and sends any unsent error response.
///
/// Sends a 500 error if the request body was not completely read.
///
/// Logs an error and closes the connection if the response body was not completely sent.
pub async fn log_and_finish(
    _req: &HttpRequest,
    rw: &mut HttpReaderWriter,
    handler_result: Result<(), HttpError>,
) {
    match handler_result {
        Ok(_) => {
            let mut logged_error = false;
            if rw.ready_to_send_response() {
                let reason = "response not sent";
                error!("{}", reason);
                let _ = rw
                    .send(HttpStatus::InternalServerError500(reason.to_string()))
                    .await;
                logged_error = true;
            }
            if let Err(reason) = rw.check_ready_to_write_next_response_or_closed() {
                error!("{}", reason);
                rw.close();
                return;
            }
            if let Err(reason) = rw.check_ready_to_read_next_request_or_closed() {
                error!("{}", reason);
                rw.close();
                return;
            }
            if !logged_error {
                info!("{}", rw.sent_status().as_ref().unwrap());
            }
        }
        Err(http_error) => {
            log_error(&http_error);
            if let Some(status) = rw.take_unsent_status() {
                // Try to send unsent status, ignoring errors.
                let _ = rw.send(status).await;
            }
            match http_error {
                HttpError::Io(_) => {}
                HttpError::Parsing(_) => {
                    let _ = rw.send(HttpStatus::BadRequest400).await;
                }
                HttpError::Handling(status) => {
                    let _ = rw.send(status).await;
                }
                HttpError::Closed => {}
                HttpError::Stopped => {
                    let _ = rw.send(HttpStatus::ServiceUnavailable503).await;
                }
            }
            if rw.check_ready_to_write_next_response_or_closed().is_err()
                || rw.check_ready_to_read_next_request_or_closed().is_err()
            {
                rw.close();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::reader_writer::StatusMode;
    use crate::{capture_thread_logs, filter_log_entries, ParsingError};
    use log::Level::Warn;

    #[test]
    fn test_connection_reuse_deadline() {
        tokio_test::block_on(async {
            let now = tokio::time::Instant::now();
            tokio::select! {
                _ = connection_reuse_deadline(&now) => panic!("hit deadline"),
                _ = tokio::time::sleep(std::time::Duration::from_millis(10)) => {}
            };
            let now = tokio::time::Instant::now();
            let deadline = now - std::time::Duration::from_secs(60);
            tokio::select! {
                _ = connection_reuse_deadline(&deadline) => {},
                _ = tokio::time::sleep_until(now.add(std::time::Duration::from_millis(100))) =>
                    panic!("timer expired")
            };
        });
    }

    fn capture<F: FnOnce()>(level: log::Level, f: F) -> String {
        let (_, entries) = capture_thread_logs(f);
        let mut result = filter_log_entries(&entries, level, "beatrice_http::http");
        if result.len() != 1 {
            panic!("too many log entries: {:?}", result);
        }
        result.remove(0)
    }

    #[test]
    fn test_log_error() {
        assert_eq!(
            "500 \"internal server error\": err1",
            capture(Warn, || log_error(&HttpError::Handling(
                HttpStatus::InternalServerError500("err1".to_string())
            )))
        );
        assert_eq!(
            "405 \"method not allowed\"",
            capture(Warn, || log_error(&HttpError::Handling(
                HttpStatus::MethodNotAllowed405
            )))
        );
        assert_eq!(
            "405 \"method not allowed\"",
            capture(Warn, || log_error(&HttpError::Parsing(
                ParsingError::MethodInvalid
            )))
        );
        assert_eq!(
            "Other{err1}",
            capture(Warn, || log_error(&HttpError::Io(std::io::Error::new(
                std::io::ErrorKind::Other,
                "err1"
            ))))
        );
        assert_eq!("Stopped", capture(Warn, || log_error(&HttpError::Stopped)));
        assert_eq!("Closed", capture(Warn, || log_error(&HttpError::Closed)));
    }

    #[test]
    fn test_log_result() {
        assert_eq!(
            "405 \"method not allowed\"",
            capture(Warn, || log_result(
                &HttpRequest::default(),
                &HttpReaderWriter::default(),
                Err(HttpError::Handling(HttpStatus::MethodNotAllowed405))
            ))
        );
        assert_eq!(
            "None",
            capture(Warn, || log_result(
                &HttpRequest::default(),
                &HttpReaderWriter::default(),
                Ok(())
            ))
        );
        let mut rw = HttpReaderWriter::default();
        rw.set_status(StatusMode::Sent(HttpStatus::Created201));
        assert_eq!(
            "Some(Created201)",
            capture(Warn, || log_result(&HttpRequest::default(), &rw, Ok(())))
        );
    }
}
