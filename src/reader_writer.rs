#![forbid(unsafe_code)]
use crate::{
    body, chunk_foot, chunk_head, connection_reuse_deadline, headers, read_chunk_final_crlf,
    read_chunk_head, read_chunked_body_foot, AsyncReadLogger, AsyncWriteLogger, ChainAsyncRead,
    Header, HttpError, HttpRequest, HttpStatus, Stopper, StopperController,
};
use fixed_buffer::FixedBuf;
use log::{debug, trace};
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::{AsyncRead, AsyncWrite};

// Remove this and use std::task::ready once it is stable.
// https://github.com/rust-lang/rust/issues/70922
macro_rules! ready {
    ( $e:expr ) => {
        match $e {
            std::task::Poll::Ready(t) => t,
            std::task::Poll::Pending => {
                return std::task::Poll::Pending;
            }
        };
    };
}

fn reject_header(name: &str, headers: &[&Header]) -> Result<(), HttpError> {
    for &header in headers {
        if header.name.eq_ignore_ascii_case(name) {
            return Err(HttpError::Handling(HttpStatus::InternalServerError500(
                format!("invalid extra header {:?}", header),
            )));
        }
    }
    Ok(())
}

fn write_str<B: AsMut<[u8]>>(buf: &mut FixedBuf<B>, s: &str) -> Result<(), HttpError> {
    buf.write_str(s)
        .map_err(|e| HttpError::Handling(HttpStatus::InternalServerError500(e.to_string())))
}

fn write_content_length_header<B: AsMut<[u8]>>(
    mut buf: &mut FixedBuf<B>,
    len: u64,
) -> Result<(), HttpError> {
    write_str(&mut buf, "content-length: ")?;
    // Write num without allocating.
    itoa::write(&mut buf, len)
        .map_err(|e| HttpError::Handling(HttpStatus::InternalServerError500(e.to_string())))?;
    write_str(&mut buf, "\r\n")
}

fn write_header<B: AsMut<[u8]>>(buf: &mut FixedBuf<B>, header: &Header) -> Result<(), HttpError> {
    write_str(buf, header.name)?;
    write_str(buf, ": ")?;
    write_str(buf, header.value)?;
    write_str(buf, "\r\n")
}

fn write_headers<B: AsMut<[u8]>>(
    buf: &mut FixedBuf<B>,
    headers: &[&Header],
) -> Result<(), HttpError> {
    for &header in headers {
        write_header(buf, header)?;
    }
    Ok(())
}

#[allow(clippy::large_enum_variant)]
pub enum Conn {
    Tcp(tokio::net::TcpStream),
    Tls(tokio_rustls::server::TlsStream<tokio::net::TcpStream>),
    Mem(tokio::io::DuplexStream),
}

impl Conn {
    pub fn mut_read(&mut self) -> AsyncReadLogger<'_> {
        match self {
            Conn::Tcp(ref mut tcp_stream) => AsyncReadLogger::new(tcp_stream),
            Conn::Tls(ref mut tls_stream) => AsyncReadLogger::new(tls_stream),
            Conn::Mem(ref mut duplex_stream) => AsyncReadLogger::new(duplex_stream),
        }
    }

    pub fn mut_write(&mut self) -> AsyncWriteLogger<'_> {
        match self {
            Conn::Tcp(ref mut tcp_stream) => AsyncWriteLogger::new(tcp_stream),
            Conn::Tls(ref mut tls_stream) => AsyncWriteLogger::new(tls_stream),
            Conn::Mem(ref mut duplex_stream) => AsyncWriteLogger::new(duplex_stream),
        }
    }
}

// # HttpReaderWriter Design Notes
//
// The HttpReaderWriter struct is both AsyncRead and AsyncWrite.
// Both of these traits are like futures.
// Tokio calls the poll_* functions repeatedly, with the same arguments,
// until they return Poll::Ready(_).
//
// For a poll_* function to call an async fn, it would need to save the returned future struct
// so it will be available on the next call.
// This is impossible because Rust (as of 2012-11-17)
// has no way to write the type of an async fn's compiler-generated future.
// Therefore the poll_* functions cannot simply delegate their work to async fns.
//
// HTTP session scenarios:
// - Read head, no payload, write head, no payload
// - Read head, no payload, write head, write payload
// - Read head, no payload, write head, write chunks
// - Read head, ignore "Expect: 100-Continue", write head, no payload
// - Read head, ignore "Expect: 100-Continue", write head, write payload
// - Read head, ignore "Expect: 100-Continue", write head, write chunks
// - Read head, read payload, write head, no payload
// - Read head, read payload, write head, write payload
// - Read head, read payload, write head, write chunks
// - Read head, read chunks, write head, no payload
// - Read head, read chunks, write head, write payload
// - Read head, read chunks, write head, read and write chunks
//
// Graphically:
// | Read head, send continue, read payload ----------------------------------|
// |                           read chunks -----------------------------------|
// |                                        write head, write payload --------|
// |                                                    write chunks ---------|
// |                                                                          |
// ^-- session start                                            session end --^
//
//
// Just like a compiler-generated future struct,
// the HttpReaderWriter keeps its state in fields.

/// Represents the state of the HTTP protocol on the input stream.
#[derive(Debug, PartialEq, Copy, Clone)]
enum ReadState {
    Ready,
    ReadingHead,
    MustSendContinueForChunks,
    MustSendContinueForPayload(u64),
    ReadingChunks,
    ReadingChunk(u64),
    ReadingChunkedBodyFoot,
    /// Currently reading a request payload.
    /// The value is the number of bytes left to read.
    ReadingPayload(u64),
    Closed,
}

/// Represents the state of the HTTP protocol on the output stream.
#[derive(Debug, PartialEq, Copy, Clone)]
enum WriteState {
    /// Ready to read a new HTTP request.
    Ready,
    /// Currently writing the "100 Continue" status line.
    /// The buffer contains unwritten bytes.
    WritingContinue(FixedBuf<[u8; 25]>),
    WritingHeadAndPayload,
    WritingChunks,
    /// Currently writing a chunk header with hex-encoded chunk length.
    /// The buffer contains unwritten bytes.
    WritingChunkHead(FixedBuf<[u8; 18]>),
    WritingChunkBody(usize),
    /// Currently writing the "\r\n" footer after a chunk.
    /// The buffer contains unwritten bytes.
    WritingChunkFoot(FixedBuf<&'static [u8]>),
    WroteResponse,
    Closed,
}

impl ReadState {
    pub fn name(&self) -> &'static str {
        match self {
            ReadState::Ready => "Ready",
            ReadState::ReadingHead => "ReadingHead",
            ReadState::MustSendContinueForChunks => "MustSendContinueForChunks",
            ReadState::MustSendContinueForPayload(_) => "MustSendContinueForPayload",
            ReadState::ReadingChunks => "ReadingChunks",
            ReadState::ReadingChunk(_) => "ReadingChunk",
            ReadState::ReadingChunkedBodyFoot => "ReadingChunkedBodyFoot",
            ReadState::ReadingPayload(_) => "ReadingPayload",
            ReadState::Closed => "Closed",
        }
    }
    pub fn error(&self, name: &'static str) -> HttpError {
        HttpError::internal500(format!("{} called in ReadState::{:?}", name, self))
    }

    pub fn expect(&self, name: &'static str, state: ReadState) -> Result<(), HttpError> {
        if *self != state {
            Err(self.error(name))
        } else {
            Ok(())
        }
    }

    pub fn set(&mut self, new_state: ReadState) {
        trace!("{:?} -> {:?}", self, new_state);
        *self = new_state;
    }

    pub fn skip_sending_100_continue(&mut self) {
        if let ReadState::MustSendContinueForChunks | ReadState::MustSendContinueForPayload(_) =
            self
        {
            *self = ReadState::Ready;
        }
    }
}

impl std::fmt::Display for ReadState {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.name())
    }
}

impl WriteState {
    pub fn name(&self) -> &'static str {
        match self {
            WriteState::Ready => "Ready",
            WriteState::WritingContinue(_) => "WritingContinue",
            WriteState::WritingHeadAndPayload => "WritingHeadAndPayload",
            WriteState::WritingChunks => "WritingChunks",
            WriteState::WritingChunkHead(_) => "WritingChunkHead",
            WriteState::WritingChunkBody(_) => "WritingChunkBody",
            WriteState::WritingChunkFoot(_) => "WritingChunkFoot",
            WriteState::WroteResponse => "WroteResponse",
            WriteState::Closed => "Closed",
        }
    }

    pub fn error(&self, name: &'static str) -> HttpError {
        HttpError::internal500(format!("{} called in WriteState::{:?}", name, self))
    }

    pub fn expect(&self, name: &'static str, state: WriteState) -> Result<(), HttpError> {
        if *self != state {
            Err(self.error(name))
        } else {
            Ok(())
        }
    }

    pub fn set(&mut self, new_state: WriteState) {
        trace!("{:?} -> {:?}", self, new_state);
        *self = new_state;
    }

    pub fn new_writing_continue() -> Self {
        let mut data = FixedBuf::new([0u8; 25]);
        data.write_bytes(b"HTTP/1.1 100 Continue\r\n\r\n").unwrap();
        WriteState::WritingContinue(data)
    }
}

impl std::fmt::Display for WriteState {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.name())
    }
}

#[derive(Debug, PartialEq, Clone)]
pub enum StatusMode {
    None,
    Unsent(HttpStatus),
    Sent(HttpStatus),
}

impl std::default::Default for StatusMode {
    fn default() -> Self {
        StatusMode::None
    }
}

pub struct HttpReaderWriter {
    stopper: Stopper,
    conn: Conn,
    addr: std::net::SocketAddr,
    conn_start: tokio::time::Instant,
    read_buf: FixedBuf<[u8; 4 * 1024]>,
    read_state: ReadState,
    write_state: WriteState,
    bytes_read: u64,
    bytes_written: u64,
    status: StatusMode,
}

impl HttpReaderWriter {
    pub fn new(
        stopper: Stopper,
        conn: Conn,
        addr: std::net::SocketAddr,
        now: tokio::time::Instant,
    ) -> HttpReaderWriter {
        Self {
            stopper,
            conn,
            addr,
            conn_start: now,
            read_buf: FixedBuf::new([0u8; 4 * 1024]),
            read_state: ReadState::Ready,
            write_state: WriteState::WroteResponse,
            bytes_read: 0,
            bytes_written: 0,
            status: StatusMode::None,
        }
    }

    pub fn addr(&self) -> &std::net::SocketAddr {
        &self.addr
    }

    pub fn sent_status(&self) -> Option<&HttpStatus> {
        match self.status {
            StatusMode::None => None,
            StatusMode::Unsent(_) => None,
            StatusMode::Sent(ref status) => Some(status),
        }
    }

    pub fn unsent_status(&self) -> Option<&HttpStatus> {
        match self.status {
            StatusMode::None => None,
            StatusMode::Unsent(ref status) => Some(status),
            StatusMode::Sent(_) => None,
        }
    }

    pub fn set_status(&mut self, new_status: StatusMode) {
        if self.status != StatusMode::None {
            panic!(
                "cannot set status to {:?} since it is already {:?}",
                self.status, new_status
            );
        }
        trace!("-> {:?}", new_status);
        self.status = new_status;
    }

    pub fn take_unsent_status(&mut self) -> Option<HttpStatus> {
        if let StatusMode::Unsent(_) = self.status {
            if let StatusMode::Unsent(http_status) = std::mem::take(&mut self.status) {
                Some(http_status)
            } else {
                unreachable!()
            }
        } else {
            None
        }
    }

    pub fn close(&mut self) {
        self.read_state.set(ReadState::Closed);
        self.write_state.set(WriteState::Closed);
        self.read_buf.clear();
    }

    pub fn check_closed(&self) -> Result<(), HttpError> {
        if ReadState::Closed == self.read_state || WriteState::Closed == self.write_state {
            return Err(HttpError::Closed);
        }
        Ok(())
    }

    pub fn ready_to_read_request(&self) -> bool {
        self.write_state == WriteState::WroteResponse
            && match self.read_state {
                ReadState::Ready => true,
                ReadState::ReadingHead => false,
                ReadState::MustSendContinueForChunks => true,
                ReadState::MustSendContinueForPayload(_) => true,
                ReadState::ReadingChunks => false,
                ReadState::ReadingChunk(_) => false,
                ReadState::ReadingChunkedBodyFoot => false,
                ReadState::ReadingPayload(_) => false,
                ReadState::Closed => false,
            }
    }

    pub fn check_ready_to_read_next_request_or_closed(&self) -> Result<(), &'static str> {
        match self.read_state {
            ReadState::Ready => Ok(()),
            ReadState::ReadingHead => Err("connection in state ReadingHead"),
            ReadState::MustSendContinueForChunks => Ok(()),
            ReadState::MustSendContinueForPayload(_) => Ok(()),
            ReadState::ReadingChunks => Err("connection in state ReadingChunks"),
            ReadState::ReadingChunk(_) => Err("connection in state ReadingChunk"),
            ReadState::ReadingChunkedBodyFoot => Err("connection in state ReadingChunkedBodyFoot"),
            ReadState::ReadingPayload(_) => Err("connection in state ReadingPayload"),
            ReadState::Closed => Ok(()),
        }
    }

    pub fn check_ready_to_write_next_response_or_closed(&self) -> Result<(), &'static str> {
        match self.write_state {
            WriteState::Ready => Err("connection in state WriteState::Ready, no response sent"),
            WriteState::WritingContinue(_) => Err("connection in state WritingContinue"),
            WriteState::WritingHeadAndPayload => Err("connection in state WritingHeadAndPayload"),
            WriteState::WritingChunks => Err("connection in state WritingChunks"),
            WriteState::WritingChunkHead(_) => Err("connection in state WritingChunkHead"),
            WriteState::WritingChunkBody(_) => Err("connection in state WritingChunkBody"),
            WriteState::WritingChunkFoot(_) => Err("connection in state WritingChunkFoot"),
            WriteState::WroteResponse => Ok(()),
            WriteState::Closed => Ok(()),
        }
    }

    pub fn ready_to_send_response(&self) -> bool {
        self.write_state == WriteState::Ready
    }

    pub async fn read_request(&mut self) -> Result<HttpRequest, HttpError> {
        self.check_closed()?;
        self.read_state.expect("read_request", ReadState::Ready)?;
        self.write_state
            .expect("read_request", WriteState::WroteResponse)?;
        self.read_state.set(ReadState::ReadingHead);
        self.write_state.set(WriteState::Ready);
        // "HTTP/1.1 Message Syntax and Routing" https://tools.ietf.org/html/rfc7230
        self.read_buf.shift();
        let head_bytes: &[u8] = tokio::select! {
            result = self.read_buf.read_delimited(self.conn.mut_read(), b"\r\n\r\n") => {
                result.map_err(HttpError::Io)?.ok_or(HttpError::Closed)?
            }
            _ = connection_reuse_deadline(&self.conn_start) => {
                return Err(HttpError::Stopped);
            }
            _ = self.stopper.wait() => {
                return Err(HttpError::Stopped);
            }
        };
        self.bytes_read += head_bytes.len() as u64;
        headers::read::trace(head_bytes);
        let req = HttpRequest::parse(self.addr, head_bytes, &mut [])?;
        self.read_state
            .set(match (req.chunked, req.content_length, req.expect_100) {
                // No payload.  Ignore the expect-100-continue header if there is no payload.
                (false, 0, _) => ReadState::Ready,
                // Ignore content-length header when transfer-encoding is chunked.
                // "A sender MUST NOT send a Content-Length header field in any
                //  message that contains a Transfer-Encoding header field."
                // https://tools.ietf.org/html/rfc7230#section-3.3.2
                (true, _, false) => ReadState::ReadingChunks,
                (true, _, true) => ReadState::MustSendContinueForChunks,
                (false, content_length, false) => ReadState::ReadingPayload(content_length),
                (false, content_length, true) => {
                    ReadState::MustSendContinueForPayload(content_length)
                }
            });
        Ok(req)
    }

    async fn send_head_bytes(&mut self, data: &[u8]) -> Result<(), HttpError> {
        headers::write::trace(data);
        self.write_state
            .expect("send_head_bytes", WriteState::WritingHeadAndPayload)?;
        tokio::io::AsyncWriteExt::write_all(&mut self.conn.mut_write(), data)
            .await
            .map_err(HttpError::from_io_err)?;
        self.bytes_written += data.len() as u64;
        Ok(())
    }

    async fn send_payload(&mut self, body: &[u8]) -> Result<(), HttpError> {
        body::write::trace(body);
        self.write_state
            .expect("send_payload", WriteState::WritingHeadAndPayload)?;
        tokio::io::AsyncWriteExt::write_all(&mut self.conn.mut_write(), body)
            .await
            .map_err(HttpError::from_io_err)?;
        self.bytes_written += body.len() as u64;
        Ok(())
    }

    pub async fn send(&mut self, status: HttpStatus) -> Result<(), HttpError> {
        self.check_closed()?;
        self.write_state.expect("send", WriteState::Ready)?;
        let mut buf = FixedBuf::new([0u8; 256]);
        write_str(&mut buf, status.as_str())?;
        write_str(&mut buf, "\r\n")?;
        write_content_length_header(&mut buf, 0)?;
        write_str(&mut buf, "\r\n")?;
        self.set_status(StatusMode::Sent(status));
        self.read_state.skip_sending_100_continue();
        self.write_state.set(WriteState::WritingHeadAndPayload);
        self.send_head_bytes(buf.read_all()).await?;
        self.write_state.set(WriteState::WroteResponse); // No body.
        Ok(())
    }

    pub async fn send_with_headers(
        &mut self,
        status: HttpStatus,
        extra_headers: &[&Header<'_>],
    ) -> Result<(), HttpError> {
        self.check_closed()?;
        self.write_state
            .expect("send_with_headers", WriteState::Ready)?;
        let mut buf = FixedBuf::new([0u8; 2 * 1024]);
        write_str(&mut buf, status.as_str())?;
        write_str(&mut buf, "\r\n")?;
        write_content_length_header(&mut buf, 0)?;
        // Allow Content-Length and Content-Type headers.  HEAD responses usually include them.
        reject_header("transfer-encoding", extra_headers)?;
        reject_header("connection", extra_headers)?;
        reject_header("content-encoding", extra_headers)?;
        write_headers(&mut buf, extra_headers)?;
        write_str(&mut buf, "\r\n")?;
        self.set_status(StatusMode::Sent(status));
        self.read_state.skip_sending_100_continue();
        self.write_state.set(WriteState::WritingHeadAndPayload);
        self.send_head_bytes(buf.read_all()).await?;
        self.write_state.set(WriteState::WroteResponse); // No body.
        Ok(())
    }

    pub async fn send_text(&mut self, status: HttpStatus, body: &str) -> Result<(), HttpError> {
        self.send_text_with_headers(status, &[], body).await
    }

    pub async fn send_text_with_headers(
        &mut self,
        status: HttpStatus,
        extra_headers: &[&Header<'_>],
        body: &str,
    ) -> Result<(), HttpError> {
        self.check_closed()?;
        if body.is_empty() {
            return self.send_with_headers(status, extra_headers).await;
        }
        self.write_state
            .expect("send_text_with_headers", WriteState::Ready)?;
        let mut buf = FixedBuf::new([0u8; 2 * 1024]);
        write_str(&mut buf, status.as_str())?;
        write_str(&mut buf, "\r\ncontent-type: text/plain; charset=UTF-8\r\n")?;
        write_content_length_header(&mut buf, body.len() as u64)?;
        reject_header("transfer-encoding", extra_headers)?;
        reject_header("connection", extra_headers)?;
        reject_header("content-length", extra_headers)?;
        reject_header("content-type", extra_headers)?;
        reject_header("content-encoding", extra_headers)?;
        write_headers(&mut buf, extra_headers)?;
        write_str(&mut buf, "\r\n")?;
        self.set_status(StatusMode::Sent(status));
        self.read_state.skip_sending_100_continue();
        self.write_state.set(WriteState::WritingHeadAndPayload);
        self.send_head_bytes(buf.read_all()).await?;
        self.send_payload(body.as_bytes()).await?;
        self.write_state.set(WriteState::WroteResponse); // Body sent.
        Ok(())
    }

    pub async fn send_bytes(&mut self, status: HttpStatus, body: &[u8]) -> Result<(), HttpError> {
        self.send_bytes_with_headers(status, &[], body).await
    }

    pub async fn send_bytes_with_headers(
        &mut self,
        status: HttpStatus,
        extra_headers: &[&Header<'_>],
        body: &[u8],
    ) -> Result<(), HttpError> {
        self.check_closed()?;
        if body.is_empty() {
            return self.send_with_headers(status, extra_headers).await;
        }
        self.write_state
            .expect("send_bytes_with_headers", WriteState::Ready)?;
        let mut buf = FixedBuf::new([0u8; 2 * 1024]);
        write_str(&mut buf, status.as_str())?;
        write_str(&mut buf, "\r\n")?;
        write_content_length_header(&mut buf, body.len() as u64)?;
        reject_header("transfer-encoding", extra_headers)?;
        reject_header("connection", extra_headers)?;
        reject_header("content-length", extra_headers)?;
        write_headers(&mut buf, extra_headers)?;
        write_str(&mut buf, "\r\n")?;
        self.set_status(StatusMode::Sent(status));
        self.read_state.skip_sending_100_continue();
        self.write_state.set(WriteState::WritingHeadAndPayload);
        self.send_head_bytes(buf.read_all()).await?;
        self.send_payload(body).await?;
        self.write_state.set(WriteState::WroteResponse); // Body sent.
        Ok(())
    }

    pub async fn send_and_start_streaming(&mut self, status: HttpStatus) -> Result<(), HttpError> {
        self.send_and_start_streaming_with_headers(status, &[])
            .await
    }

    pub async fn send_and_start_streaming_with_headers(
        &mut self,
        status: HttpStatus,
        extra_headers: &[&Header<'_>],
    ) -> Result<(), HttpError> {
        self.check_closed()?;
        self.write_state
            .expect("send_and_start_streaming_with_headers", WriteState::Ready)?;
        let mut buf = FixedBuf::new([0u8; 2 * 1024]);
        write_str(&mut buf, status.as_str())?;
        write_str(&mut buf, "\r\n")?;
        write_str(&mut buf, "transfer-encoding: chunked\r\n")?;
        reject_header("transfer-encoding", extra_headers)?;
        reject_header("connection", extra_headers)?;
        reject_header("content-length", extra_headers)?;
        write_headers(&mut buf, extra_headers)?;
        write_str(&mut buf, "\r\n")?;
        self.set_status(StatusMode::Sent(status));
        self.read_state.skip_sending_100_continue();
        self.write_state.set(WriteState::WritingHeadAndPayload);
        self.send_head_bytes(buf.read_all()).await?;
        self.write_state.set(WriteState::WritingChunks);
        Ok(())
    }

    pub async fn finish_streaming(&mut self) -> Result<(), HttpError> {
        self.check_closed()?;
        self.write_state
            .expect("finish_stream", WriteState::WritingChunks)?;
        let data = b"0\r\n\r\n";
        tokio::io::AsyncWriteExt::write_all(&mut self.conn.mut_write(), data)
            .await
            .map_err(HttpError::from_io_err)?;
        self.bytes_written += data.len() as u64;
        self.write_state.set(WriteState::WroteResponse);
        Ok(())
    }

    fn poll_read_into_read_buf(
        &mut self,
        cx: &mut Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        self.read_buf.shift();
        let dest = match self.read_buf.writable() {
            Some(x) => x,
            None => {
                self.close();
                if self.status == StatusMode::None {
                    self.set_status(StatusMode::Unsent(HttpStatus::BadRequest400));
                }
                return Poll::Ready(Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "buffer full",
                )));
            }
        };
        let mut read_buf = tokio::io::ReadBuf::new(dest);
        ready!(Pin::new(&mut self.conn.mut_read()).poll_read(cx, &mut read_buf))?;
        let num_read = read_buf.filled().len();
        if num_read == 0 {
            return Poll::Ready(Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "eof",
            )));
        }
        self.read_buf.wrote(num_read);
        Poll::Ready(Ok(()))
    }
}

impl AsyncWrite for HttpReaderWriter {
    //noinspection DuplicatedCode
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, std::io::Error>> {
        // https://docs.rs/tokio-util/0.3.1/tokio_util/codec/struct.FramedWrite.html
        let mut_self = self.get_mut();
        trace!(
            "poll_write {:?} {:?}",
            mut_self.read_state,
            mut_self.write_state
        );
        mut_self.check_closed().map_err(HttpError::into_io_err)?;
        if buf.is_empty() {
            return std::task::Poll::Ready(Ok(0));
        }
        if let WriteState::WritingChunks = mut_self.write_state {
            // Start a new chunk.
            mut_self
                .write_state
                .set(WriteState::WritingChunkHead(chunk_head(buf.len() as u64)));
        };
        if let WriteState::WritingChunkHead(ref mut data) = mut_self.write_state {
            while !data.is_empty() {
                let num_bytes_written = ready!(
                    Pin::new(&mut mut_self.conn.mut_write()).poll_write(cx, data.readable())
                )?;
                data.read_bytes(num_bytes_written);
                mut_self.bytes_written += num_bytes_written as u64;
            }
            mut_self
                .write_state
                .set(WriteState::WritingChunkBody(buf.len()));
        }
        if let WriteState::WritingChunkBody(ref mut num_bytes_to_write) = mut_self.write_state {
            while *num_bytes_to_write > 0 {
                let start = buf.len() - *num_bytes_to_write;
                let num_bytes_written =
                    ready!(Pin::new(&mut mut_self.conn.mut_write()).poll_write(cx, &buf[start..]))?;
                let bytes_written = &buf[start..(start + num_bytes_written)];
                body::write::trace(bytes_written);
                *num_bytes_to_write -= num_bytes_written;
                mut_self.bytes_written += num_bytes_written as u64;
            }
            mut_self
                .write_state
                .set(WriteState::WritingChunkFoot(chunk_foot()));
        }
        if let WriteState::WritingChunkFoot(ref mut data) = mut_self.write_state {
            while !data.is_empty() {
                let num_bytes_written = ready!(
                    Pin::new(&mut mut_self.conn.mut_write()).poll_write(cx, data.readable())
                )?;
                data.read_bytes(num_bytes_written);
                mut_self.bytes_written += num_bytes_written as u64;
            }
            mut_self.write_state.set(WriteState::WritingChunks);
            return Poll::Ready(Ok(buf.len()));
        }
        panic!("{:?}", mut_self.write_state);
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), std::io::Error>> {
        trace!("poll_flush {:?} {:?}", self.read_state, self.write_state);
        self.check_closed().map_err(HttpError::into_io_err)?;
        Pin::new(&mut self.get_mut().conn.mut_write()).poll_flush(cx)
    }

    fn poll_shutdown(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        trace!("poll_shutdown {:?} {:?}", self.read_state, self.write_state);
        self.check_closed().map_err(HttpError::into_io_err)?;
        Pin::new(&mut self.get_mut().conn.mut_write()).poll_shutdown(cx)
    }
}

impl AsyncRead for HttpReaderWriter {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        let mut_self = self.get_mut();
        trace!(
            "poll_read {:?} {:?}",
            mut_self.read_state,
            mut_self.write_state
        );
        mut_self.check_closed().map_err(HttpError::into_io_err)?;
        if let ReadState::MustSendContinueForChunks = mut_self.read_state {
            assert_eq!(WriteState::Ready, mut_self.write_state);
            mut_self.write_state.set(WriteState::new_writing_continue());
            mut_self.read_state.set(ReadState::ReadingChunks);
        }
        if let ReadState::MustSendContinueForPayload(payload_len) = mut_self.read_state {
            assert_eq!(WriteState::Ready, mut_self.write_state);
            mut_self.write_state.set(WriteState::new_writing_continue());
            mut_self
                .read_state
                .set(ReadState::ReadingPayload(payload_len));
        }
        while let WriteState::WritingContinue(ref mut data) = mut_self.write_state {
            if data.is_empty() {
                mut_self.write_state.set(WriteState::Ready);
                break;
            }
            let num_bytes_written =
                ready!(Pin::new(&mut mut_self.conn.mut_write()).poll_write(cx, data.readable()))?;
            let bytes_written = data.read_bytes(num_bytes_written);
            headers::write::trace(bytes_written);
            trace!("-> {:?}", mut_self.write_state);
            mut_self.bytes_written += num_bytes_written as u64;
        }
        if let ReadState::ReadingPayload(0) = mut_self.read_state {
            mut_self.read_state.set(ReadState::Ready);
            return Poll::Ready(Ok(())); // EOF
        }
        if let ReadState::ReadingPayload(ref mut num_unread) = mut_self.read_state {
            let filled_before = buf.filled().len();
            let mut reader = mut_self.conn.mut_read();
            let chain = ChainAsyncRead::new(&mut mut_self.read_buf, &mut reader);
            let mut take = tokio::io::AsyncReadExt::take(chain, *num_unread);
            let result = Pin::new(&mut take).poll_read(cx, buf);
            let num_read = buf.filled().len() - filled_before;
            *num_unread -= num_read as u64;
            trace!("-> {:?}", mut_self.read_state);
            if num_read > 0 {
                let bytes_read = &buf.filled()[filled_before..];
                body::read::trace(bytes_read);
            }
            return result;
        }
        // chunked-body   = *chunk
        //                  last-chunk
        //                  trailer-part
        //                  CRLF
        // chunk          = chunk-size [ chunk-ext ] CRLF
        //                  chunk-data CRLF
        // chunk-size     = 1*HEXDIG
        // last-chunk     = 1*("0") [ chunk-ext ] CRLF
        // chunk-data     = 1*OCTET ; a sequence of chunk-size octets
        // chunk-ext      = *( ";" chunk-ext-name [ "=" chunk-ext-val ] )
        // chunk-ext-name = token
        // chunk-ext-val  = token / quoted-string
        // trailer-part   = *( header-field CRLF )
        // https://tools.ietf.org/html/rfc7230#section-4.1
        if let ReadState::ReadingChunk(0) = mut_self.read_state {
            loop {
                match read_chunk_final_crlf(&mut mut_self.read_buf) {
                    Ok(true) => {
                        mut_self.read_state.set(ReadState::ReadingChunks);
                        break;
                    }
                    Ok(false) => {
                        ready!(mut_self.poll_read_into_read_buf(cx))?;
                    }
                    Err(e) => {
                        debug!("{:?}", e);
                        mut_self.close();
                        if mut_self.status == StatusMode::None {
                            mut_self.set_status(StatusMode::Unsent(HttpStatus::BadRequest400));
                        }
                        return Poll::Ready(Err(e.into_io_err()));
                    }
                }
            }
        }
        if let ReadState::ReadingChunks = mut_self.read_state {
            loop {
                match read_chunk_head(&mut mut_self.read_buf) {
                    Ok(Some(0)) => {
                        mut_self.read_state.set(ReadState::ReadingChunkedBodyFoot);
                        break;
                    }
                    Ok(Some(chunk_len)) => {
                        mut_self.read_state.set(ReadState::ReadingChunk(chunk_len));
                        break;
                    }
                    Ok(None) => {
                        ready!(mut_self.poll_read_into_read_buf(cx))?;
                    }
                    Err(e) => {
                        debug!("{:?}", e);
                        mut_self.close();
                        if mut_self.status == StatusMode::None {
                            mut_self.set_status(StatusMode::Unsent(HttpStatus::BadRequest400));
                        }
                        return Poll::Ready(Err(e.into_io_err()));
                    }
                }
            }
        }
        if let ReadState::ReadingChunk(ref mut num_unread) = mut_self.read_state {
            let filled_before = buf.filled().len();
            let mut reader = mut_self.conn.mut_read();
            let chain = ChainAsyncRead::new(&mut mut_self.read_buf, &mut reader);
            let mut take = tokio::io::AsyncReadExt::take(chain, *num_unread);
            ready!(Pin::new(&mut take).poll_read(cx, buf))?;
            let num_read = buf.filled().len() - filled_before;
            *num_unread -= num_read as u64;
            trace!("-> {:?}", mut_self.read_state);
            if num_read > 0 {
                let bytes_read = &buf.filled()[filled_before..];
                body::read::trace(bytes_read);
            }
            return Poll::Ready(Ok(()));
        }
        if let ReadState::ReadingChunkedBodyFoot = mut_self.read_state {
            loop {
                if read_chunked_body_foot(&mut mut_self.read_buf) {
                    mut_self.read_state.set(ReadState::Ready);
                    return Poll::Ready(Ok(()));
                } else {
                    ready!(mut_self.poll_read_into_read_buf(cx))?;
                }
            }
        }
        panic!("ReadState::{:?}", mut_self.read_state);
    }
}

impl std::fmt::Debug for HttpReaderWriter {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "HttpWriter{{{:?}", self.addr)?;
        if self.status != StatusMode::None {
            write!(f, ", {:?}", self.status)?;
        }
        write!(
            f,
            "{:?}, {:?}, bytes_read={}, bytes_written={}}}",
            self.read_state, self.write_state, self.bytes_read, self.bytes_written
        )
    }
}

impl std::default::Default for HttpReaderWriter {
    fn default() -> Self {
        let (_, stopper) = StopperController::new();
        let (_, server_duplex_stream) = tokio::io::duplex(1);
        HttpReaderWriter::new(
            stopper,
            Conn::Mem(server_duplex_stream),
            std::net::SocketAddr::new(std::net::IpAddr::V4(std::net::Ipv4Addr::new(0, 0, 0, 0)), 0),
            tokio::time::Instant::now(),
        )
    }
}
