#![forbid(unsafe_code)]
use crate::HttpStatus;

/// An error handling an HTTP request.
#[derive(Debug)]
pub enum HttpError {
    /// Error reading or writing data on the connection.
    Io(std::io::Error),
    /// Error processing data read from the connection as HTTP protocol.
    Parsing(ParsingError),
    /// Error handling a request.
    Handling(HttpStatus),
    /// Treat the connection as closed.  Do not try to read or write again.
    Closed,
    /// Stop handling requests on this connection.
    Stopped,
}

impl HttpError {
    pub fn from_io_err(e: std::io::Error) -> HttpError {
        HttpError::Io(e)
    }

    pub fn into_io_err(self) -> std::io::Error {
        match self {
            HttpError::Io(e) => e,
            HttpError::Parsing(_) => {
                std::io::Error::new(std::io::ErrorKind::Other, format!("{:?}", self))
            }
            HttpError::Handling(_) => {
                std::io::Error::new(std::io::ErrorKind::Other, format!("{:?}", self))
            }
            HttpError::Stopped => std::io::Error::new(std::io::ErrorKind::Other, "Stopped"),
            HttpError::Closed => std::io::Error::new(std::io::ErrorKind::Other, "Closed"),
        }
    }

    pub fn internal500(msg: String) -> Self {
        HttpError::Handling(HttpStatus::InternalServerError500(msg))
    }
}

impl std::clone::Clone for HttpError {
    fn clone(&self) -> Self {
        match self {
            HttpError::Io(e) => HttpError::Io(std::io::Error::new(e.kind(), e.to_string())),
            HttpError::Parsing(parsing_error) => HttpError::Parsing(parsing_error.clone()),
            HttpError::Handling(status) => HttpError::Handling(status.clone()),
            HttpError::Closed => HttpError::Closed,
            HttpError::Stopped => HttpError::Stopped,
        }
    }
}

impl std::cmp::PartialEq for HttpError {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (HttpError::Io(a), HttpError::Io(b)) => {
                a.kind() == b.kind() && a.to_string() == b.to_string()
            }
            (HttpError::Parsing(a), HttpError::Parsing(b)) => a == b,
            (HttpError::Handling(a), HttpError::Handling(b)) => a == b,
            (HttpError::Closed, HttpError::Closed) => true,
            (HttpError::Stopped, HttpError::Stopped) => true,
            _ => false,
        }
    }
}

impl std::fmt::Display for HttpError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            Self::Io(ref e) => write!(f, "{:?}", e),
            Self::Parsing(ref e) => write!(f, "{}", e.status()),
            Self::Handling(ref e) => write!(f, "{}", e),
            Self::Stopped => write!(f, "Stopped"),
            Self::Closed => write!(f, "Closed"),
        }
    }
}

/// Represents errors parsing HTTP protocol data.
#[derive(Clone, Debug, PartialEq)]
pub enum ParsingError {
    RequestLineMissing,
    RequestLineInvalid,
    MethodInvalid,
    MethodTooLong,
    PathInvalid,
    PathTooLong,
    HeaderLineInvalid,
    HeaderValueTooLong,
    ExpectHeaderInvalid,
    TransferEncodingHeaderInvalid,
    ContentLengthHeaderInvalid,
    ChunkHeadInvalid,
    ChunkMissingFinalCRLF,
}

impl ParsingError {
    pub fn status(&self) -> HttpStatus {
        match self {
            Self::RequestLineMissing => HttpStatus::BadRequest400,
            Self::RequestLineInvalid => HttpStatus::BadRequest400,
            Self::MethodInvalid => HttpStatus::MethodNotAllowed405,
            Self::MethodTooLong => HttpStatus::MethodNotAllowed405,
            Self::PathInvalid => HttpStatus::NotFound404,
            Self::PathTooLong => HttpStatus::UriTooLong414,
            Self::HeaderLineInvalid => HttpStatus::BadRequest400,
            Self::HeaderValueTooLong => HttpStatus::RequestHeaderFieldsTooLarge431,
            Self::ExpectHeaderInvalid => HttpStatus::BadRequest400,
            Self::TransferEncodingHeaderInvalid => HttpStatus::BadRequest400,
            Self::ContentLengthHeaderInvalid => HttpStatus::BadRequest400,
            Self::ChunkHeadInvalid => HttpStatus::BadRequest400,
            Self::ChunkMissingFinalCRLF => HttpStatus::BadRequest400,
        }
    }
}

impl std::fmt::Display for ParsingError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.status())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn make_io_error() -> std::io::Error {
        std::io::Error::new(std::io::ErrorKind::UnexpectedEof, "err1")
    }

    #[test]
    fn test_derived() {
        assert_eq!(
            "Io(Custom { kind: UnexpectedEof, error: \"err1\" })",
            format!("{:?}", HttpError::Io(make_io_error()))
        );
        assert_eq!(
            "Parsing(PathTooLong)",
            format!("{:?}", HttpError::Parsing(ParsingError::PathTooLong))
        );
        assert_eq!(
            "Handling(PayloadTooLarge413)",
            format!("{:?}", HttpError::Handling(HttpStatus::PayloadTooLarge413))
        );
        assert_eq!("Closed", format!("{:?}", HttpError::Closed));
        assert_eq!("Stopped", format!("{:?}", HttpError::Stopped));
    }

    #[test]
    fn test_from_io_err() {
        assert_eq!(
            HttpError::Io(make_io_error()),
            HttpError::from_io_err(make_io_error())
        );
    }

    #[test]
    fn test_into_io_err() {
        {
            let err = HttpError::Io(make_io_error()).into_io_err();
            assert_eq!(std::io::ErrorKind::UnexpectedEof, err.kind());
            assert_eq!("err1", err.to_string());
        }
        {
            let err = HttpError::Parsing(ParsingError::PathTooLong).into_io_err();
            assert_eq!(std::io::ErrorKind::Other, err.kind());
            assert_eq!("Parsing(PathTooLong)", err.to_string());
        }
        {
            let err = HttpError::Handling(HttpStatus::PayloadTooLarge413).into_io_err();
            assert_eq!(std::io::ErrorKind::Other, err.kind());
            assert_eq!("Handling(PayloadTooLarge413)", err.to_string());
        }
        {
            let err = HttpError::Closed.into_io_err();
            assert_eq!(std::io::ErrorKind::Other, err.kind());
            assert_eq!("Closed", err.to_string());
        }
        {
            let err = HttpError::Stopped.into_io_err();
            assert_eq!(std::io::ErrorKind::Other, err.kind());
            assert_eq!("Stopped", err.to_string());
        }
    }

    #[test]
    fn test_internal500() {
        assert_eq!(
            HttpError::Handling(HttpStatus::InternalServerError500("err1".to_string())),
            HttpError::internal500("err1".to_string())
        );
    }

    #[test]
    fn test_clone() {
        {
            #[allow(clippy::redundant_clone)]
            if let HttpError::Io(io_error) = HttpError::Io(make_io_error()).clone() {
                assert_eq!(std::io::ErrorKind::UnexpectedEof, io_error.kind());
                assert_eq!("err1", io_error.to_string());
            } else {
                unreachable!()
            }
        }
        assert_eq!(
            HttpError::Parsing(ParsingError::PathTooLong),
            HttpError::Parsing(ParsingError::PathTooLong).clone()
        );
        assert_eq!(
            HttpError::Handling(HttpStatus::PayloadTooLarge413),
            HttpError::Handling(HttpStatus::PayloadTooLarge413).clone()
        );
        assert_eq!(HttpError::Closed, HttpError::Closed.clone());
        assert_eq!(HttpError::Stopped, HttpError::Stopped.clone());
    }

    #[test]
    fn test_partial_eq() {
        assert_eq!(
            HttpError::Io(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "err1"
            )),
            HttpError::Io(make_io_error())
        );
        assert_ne!(
            HttpError::Io(std::io::Error::new(std::io::ErrorKind::Other, "err1")),
            HttpError::Io(make_io_error())
        );
        assert_ne!(
            HttpError::Io(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "err2"
            )),
            HttpError::Io(make_io_error())
        );
        assert_eq!(
            HttpError::Parsing(ParsingError::PathTooLong),
            HttpError::Parsing(ParsingError::PathTooLong)
        );
        assert_ne!(
            HttpError::Parsing(ParsingError::PathTooLong),
            HttpError::Parsing(ParsingError::ContentLengthHeaderInvalid)
        );
        assert_eq!(
            HttpError::Handling(HttpStatus::PayloadTooLarge413),
            HttpError::Handling(HttpStatus::PayloadTooLarge413)
        );
        assert_ne!(
            HttpError::Handling(HttpStatus::PayloadTooLarge413),
            HttpError::Handling(HttpStatus::RequestHeaderFieldsTooLarge431)
        );
        assert_eq!(HttpError::Closed, HttpError::Closed);
        assert_ne!(HttpError::Closed, HttpError::Stopped);
        assert_eq!(HttpError::Stopped, HttpError::Stopped);
        assert_ne!(HttpError::Stopped, HttpError::Closed);
    }

    #[test]
    fn test_http_error_display() {
        assert_eq!(
            "Custom { kind: UnexpectedEof, error: \"err1\" }",
            format!("{}", HttpError::Io(make_io_error()))
        );
        assert_eq!(
            "414 \"uri too long\"",
            format!("{}", HttpError::Parsing(ParsingError::PathTooLong))
        );
        assert_eq!(
            "413 \"payload too large\"",
            format!("{}", HttpError::Handling(HttpStatus::PayloadTooLarge413))
        );
        assert_eq!("Closed", format!("{}", HttpError::Closed));
        assert_eq!("Stopped", format!("{}", HttpError::Stopped));
    }

    #[test]
    fn test_status() {
        let errors = [
            ParsingError::RequestLineMissing,
            ParsingError::RequestLineInvalid,
            ParsingError::MethodInvalid,
            ParsingError::MethodTooLong,
            ParsingError::PathInvalid,
            ParsingError::PathTooLong,
            ParsingError::HeaderLineInvalid,
            ParsingError::HeaderValueTooLong,
            ParsingError::ExpectHeaderInvalid,
            ParsingError::TransferEncodingHeaderInvalid,
            ParsingError::ContentLengthHeaderInvalid,
            ParsingError::ChunkHeadInvalid,
            ParsingError::ChunkMissingFinalCRLF,
        ];
        for error in errors.iter() {
            if !(400..499).contains(&error.status().code()) {
                panic!(
                    "{} code is {}, not in range 400-499",
                    error,
                    error.status().code()
                );
            }
        }
        assert_eq!(
            HttpStatus::BadRequest400,
            ParsingError::RequestLineMissing.status()
        );
    }

    #[test]
    fn test_parsing_error_debug() {
        assert_eq!(
            "RequestLineMissing",
            format!("{:?}", ParsingError::RequestLineMissing)
        );
        assert_eq!(
            "RequestLineInvalid",
            format!("{:?}", ParsingError::RequestLineInvalid)
        );
        assert_eq!(
            "MethodInvalid",
            format!("{:?}", ParsingError::MethodInvalid)
        );
        assert_eq!(
            "MethodTooLong",
            format!("{:?}", ParsingError::MethodTooLong)
        );
        assert_eq!("PathInvalid", format!("{:?}", ParsingError::PathInvalid));
        assert_eq!("PathTooLong", format!("{:?}", ParsingError::PathTooLong));
        assert_eq!(
            "HeaderLineInvalid",
            format!("{:?}", ParsingError::HeaderLineInvalid)
        );
        assert_eq!(
            "HeaderValueTooLong",
            format!("{:?}", ParsingError::HeaderValueTooLong)
        );
        assert_eq!(
            "ExpectHeaderInvalid",
            format!("{:?}", ParsingError::ExpectHeaderInvalid)
        );
        assert_eq!(
            "TransferEncodingHeaderInvalid",
            format!("{:?}", ParsingError::TransferEncodingHeaderInvalid)
        );
        assert_eq!(
            "ContentLengthHeaderInvalid",
            format!("{:?}", ParsingError::ContentLengthHeaderInvalid)
        );
        assert_eq!(
            "ChunkHeadInvalid",
            format!("{:?}", ParsingError::ChunkHeadInvalid)
        );
        assert_eq!(
            "ChunkMissingFinalCRLF",
            format!("{:?}", ParsingError::ChunkMissingFinalCRLF)
        );
    }

    #[test]
    fn test_parsing_error_display() {
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::RequestLineMissing)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::RequestLineInvalid)
        );
        assert_eq!(
            "405 \"method not allowed\"",
            format!("{}", ParsingError::MethodInvalid)
        );
        assert_eq!(
            "405 \"method not allowed\"",
            format!("{}", ParsingError::MethodTooLong)
        );
        assert_eq!(
            "404 \"not found\"",
            format!("{}", ParsingError::PathInvalid)
        );
        assert_eq!(
            "414 \"uri too long\"",
            format!("{}", ParsingError::PathTooLong)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::HeaderLineInvalid)
        );
        assert_eq!(
            "431 \"request header fields too large\"",
            format!("{}", ParsingError::HeaderValueTooLong)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::ExpectHeaderInvalid)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::TransferEncodingHeaderInvalid)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::ContentLengthHeaderInvalid)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::ChunkHeadInvalid)
        );
        assert_eq!(
            "400 \"bad request\"",
            format!("{}", ParsingError::ChunkMissingFinalCRLF)
        );
    }
}
