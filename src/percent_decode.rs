#![forbid(unsafe_code)]
/// Heap-less percent decoder.
///
/// I wrote this because the
/// [`percent-encoding`](https://crates.io/crates/percent-encoding) crate
/// performs tons of heap allocations and copies and has no tests.
///
/// "A percent-encoded byte is U+0025 (%), followed by two ASCII hex digits.
///  Sequences of percent-encoded bytes, percent-decoded, should not cause
///  UTF-8 decode without BOM or fail to return failure."
/// -- https://url.spec.whatwg.org/#percent-encoded-bytes
use crate::{MultiPeek, TryPush};

#[derive(Clone, Copy)]
enum Token {
    Char(char),
    Byte(u8),
}

impl Default for Token {
    fn default() -> Self {
        Token::Byte(0u8)
    }
}

fn decode_uppercase_hex_digit(c: char) -> Option<u8> {
    if !c.is_ascii_digit() && c.is_ascii_lowercase() {
        return None;
    }
    c.to_digit(16).map(|n| n as u8)
}

pub fn decode_uppercase_hex_byte(a: char, b: char) -> Option<u8> {
    let high = decode_uppercase_hex_digit(a)?;
    let low = decode_uppercase_hex_digit(b)?;
    Some((high << 4) | low)
}

pub fn uppercase_hex_digit(n: u8) -> char {
    match n {
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => 'A',
        11 => 'B',
        12 => 'C',
        13 => 'D',
        14 => 'E',
        15 => 'F',
        _ => {
            panic!("hex digit out of range: {}", n);
        }
    }
}

/// Behaves exactly as [`percent_decode`] but returns `String`.
pub fn percent_decode_to_string(s: &str) -> String {
    let mut result = String::new();
    percent_decode(s, &mut result).unwrap();
    result
}

/// Appends `s` to `out`, with percent-encoded sections decoded.  Performs no allocations.  Safe.
/// ```
/// let mut sw = string_wrapper::StringWrapper::new([0u8; 20]);
/// beatrice_http::percent_decode("%E2%82%AC4,99", &mut sw).unwrap();
/// assert_eq!("€4,99", &*sw);
/// ```
///
/// Does not decode malformed or non-UTF8 sections.  These get included as-is.
/// ```
/// assert_eq!("%E2", beatrice_http::percent_decode_to_string("%E2"));
/// assert_eq!("%A", beatrice_http::percent_decode_to_string("%A"));
/// assert_eq!("%", beatrice_http::percent_decode_to_string("%"));
/// ```
pub fn percent_decode<T: TryPush<char> + TryPush<str>>(
    s: &str,
    out: &mut T,
) -> Result<(), std::io::Error> {
    let mut chars = MultiPeek::new(s.chars(), ['.'; 3]);
    let tokens_fn = std::iter::from_fn(move || {
        let a = chars.next()?;
        if let ('%', Some(b), Some(c)) = (a, chars.peek(), chars.peek()) {
            if let Some(n) = decode_uppercase_hex_byte(b, c) {
                chars.next();
                chars.next();
                return Some(Token::Byte(n));
            }
            // else falls through
        }
        Some(Token::Char(a))
    });
    let mut tokens = MultiPeek::new(tokens_fn, [Token::Byte(0); 3]);
    while let Some(token) = tokens.next() {
        match token {
            Token::Char(c) => out.try_push(&c)?,
            Token::Byte(a) => {
                if let Ok(s) = std::str::from_utf8(&[a]) {
                    out.try_push(s)?;
                    continue;
                }
                if let Some(Token::Byte(b)) = tokens.peek() {
                    if let Ok(s) = std::str::from_utf8(&[a, b]) {
                        out.try_push(s)?;
                        tokens.next();
                        continue;
                    }
                    if let Some(Token::Byte(c)) = tokens.peek() {
                        if let Ok(s) = std::str::from_utf8(&[a, b, c]) {
                            out.try_push(s)?;
                            tokens.next();
                            tokens.next();
                            continue;
                        }
                        if let Some(Token::Byte(d)) = tokens.peek() {
                            if let Ok(s) = std::str::from_utf8(&[a, b, c, d]) {
                                out.try_push(s)?;
                                tokens.next();
                                tokens.next();
                                tokens.next();
                                continue;
                            }
                        }
                    }
                }
                out.try_push(&'%')?;
                out.try_push(&uppercase_hex_digit(a >> 4))?;
                out.try_push(&uppercase_hex_digit(a & 0x0F))?;
            }
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_decode_uppercase_hex_digit() {
        assert_eq!(Some(0), decode_uppercase_hex_digit('0'));
        assert_eq!(Some(9), decode_uppercase_hex_digit('9'));
        assert_eq!(Some(15), decode_uppercase_hex_digit('F'));
        assert_eq!(None, decode_uppercase_hex_digit('f'));
        assert_eq!(None, decode_uppercase_hex_digit('x'));
    }

    #[test]
    fn test_decode_uppercase_hex_byte() {
        assert_eq!(Some(0), decode_uppercase_hex_byte('0', '0'));
        assert_eq!(Some(1), decode_uppercase_hex_byte('0', '1'));
        assert_eq!(Some(16), decode_uppercase_hex_byte('1', '0'));
        assert_eq!(Some(15), decode_uppercase_hex_byte('0', 'F'));
        assert_eq!(Some(255), decode_uppercase_hex_byte('F', 'F'));
        assert_eq!(None, decode_uppercase_hex_byte('0', 'f'));
        assert_eq!(None, decode_uppercase_hex_byte('0', 'x'));
    }

    #[test]
    fn test_percent_decode() {
        assert_eq!("%", percent_decode_to_string("%"));
        assert_eq!("%H", percent_decode_to_string("%H"));
        assert_eq!("%HH", percent_decode_to_string("%HH"));
        assert_eq!("%HHH", percent_decode_to_string("%HHH"));
        assert_eq!("%2", percent_decode_to_string("%2"));
        assert_eq!("%2a", percent_decode_to_string("%2a"));
        assert_eq!("%2H", percent_decode_to_string("%2H"));
        assert_eq!("%2HH", percent_decode_to_string("%2HH"));
        assert_eq!("%%", percent_decode_to_string("%%"));
        assert_eq!("%%%", percent_decode_to_string("%%%"));
        assert_eq!("%%%%", percent_decode_to_string("%%%%"));

        assert_eq!("", percent_decode_to_string(""));
        assert_eq!("abc", percent_decode_to_string("abc"));
        assert_eq!("#", percent_decode_to_string("%23"));
        assert_eq!("%23", percent_decode_to_string("%25%32%33"));
        assert_eq!("abc#", percent_decode_to_string("abc%23"));
        assert_eq!("#abc", percent_decode_to_string("%23abc"));
        assert_eq!("abc#def", percent_decode_to_string("abc%23def"));
        assert_eq!("\x00abc", percent_decode_to_string("%00abc"));
        assert_eq!("\x7F", percent_decode_to_string("%7F"));

        assert_eq!("¢", percent_decode_to_string("%C2%A2"));
        assert_eq!("%C2", percent_decode_to_string("%C2"));
        assert_eq!("%C2%C2", percent_decode_to_string("%C2%C2"));
        assert_eq!("%C2¢", percent_decode_to_string("%C2%C2%A2"));

        assert_eq!("€", percent_decode_to_string("%E2%82%AC"));
        assert_eq!("%E2", percent_decode_to_string("%E2"));
        assert_eq!("%E2%82", percent_decode_to_string("%E2%82"));
        assert_eq!("#%E2%82", percent_decode_to_string("%23%E2%82"));
        assert_eq!("%E2%82#", percent_decode_to_string("%E2%82%23"));

        assert_eq!("𐍈", percent_decode_to_string("%F0%90%8D%88"));
        assert_eq!("%F0", percent_decode_to_string("%F0"));
        assert_eq!("%F0%90", percent_decode_to_string("%F0%90"));
        assert_eq!("%F0%90%8D", percent_decode_to_string("%F0%90%8D"));
        // D800-DFFF are permanently unassigned Unicode code points.
        assert_eq!(
            "%D8\x00%D8\x00%D8\x00",
            percent_decode_to_string("%D8%00%D8%00%D8%00")
        );
    }
}
