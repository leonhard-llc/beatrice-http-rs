use std::task::Context;
use tokio::io::ReadBuf;
use tokio::macros::support::{Pin, Poll};

/// A [`tokio::io::AsyncRead`] that returns bytes from the first reader and then the second.
pub struct ChainAsyncRead<'a> {
    option_a: Option<&'a mut (dyn tokio::io::AsyncRead + Unpin)>,
    b: &'a mut (dyn tokio::io::AsyncRead + Unpin),
}

impl<'a> ChainAsyncRead<'a> {
    pub fn new(
        a: &'a mut (dyn tokio::io::AsyncRead + Unpin),
        b: &'a mut (dyn tokio::io::AsyncRead + Unpin),
    ) -> ChainAsyncRead<'a> {
        Self {
            option_a: Some(a),
            b,
        }
    }
}

impl<'a, 'b> tokio::io::AsyncRead for ChainAsyncRead<'a> {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<Result<(), std::io::Error>> {
        let mut_self = self.get_mut();
        if let Some(ref mut a) = mut_self.option_a {
            let remaining_before = buf.remaining();
            match Pin::new(a).poll_read(cx, buf) {
                Poll::Ready(Ok(())) => {
                    let written = remaining_before - buf.remaining();
                    if written == 0 {
                        // EOF
                        mut_self.option_a = None;
                    // Fall through.
                    } else {
                        return Poll::Ready(Ok(()));
                    }
                }
                other => return other,
            }
        }
        Pin::new(&mut mut_self.b).poll_read(cx, buf)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{FakeAsyncRead, FakeAsyncReadAction};

    #[test]
    fn test_ok() {
        let mut dest = String::new();
        tokio_test::block_on(tokio::io::AsyncReadExt::read_to_string(
            &mut ChainAsyncRead::new(
                &mut FakeAsyncRead::new(vec![
                    FakeAsyncReadAction::Data(b"aaa".to_vec()),
                    FakeAsyncReadAction::Pending,
                    FakeAsyncReadAction::Data(b"bbb".to_vec()),
                    FakeAsyncReadAction::Pending,
                ]),
                &mut FakeAsyncRead::new(vec![
                    FakeAsyncReadAction::Data(b"ccc".to_vec()),
                    FakeAsyncReadAction::Pending,
                    FakeAsyncReadAction::Data(b"ddd".to_vec()),
                    FakeAsyncReadAction::Pending,
                ]),
            ),
            &mut dest,
        ))
        .unwrap();
        assert_eq!("aaabbbcccddd", dest);
    }

    #[test]
    fn test_first_empty() {
        let mut dest = String::new();
        tokio_test::block_on(tokio::io::AsyncReadExt::read_to_string(
            &mut ChainAsyncRead::new(
                &mut FakeAsyncRead::new(vec![
                    FakeAsyncReadAction::Pending,
                    FakeAsyncReadAction::EOF,
                    FakeAsyncReadAction::Data(b"bbb".to_vec()),
                ]),
                &mut FakeAsyncRead::new(vec![
                    FakeAsyncReadAction::Data(b"ccc".to_vec()),
                    FakeAsyncReadAction::Pending,
                    FakeAsyncReadAction::Data(b"ddd".to_vec()),
                    FakeAsyncReadAction::Pending,
                ]),
            ),
            &mut dest,
        ))
        .unwrap();
        assert_eq!("cccddd", dest);
    }

    #[test]
    fn test_second_empty() {
        let mut dest = String::new();
        tokio_test::block_on(tokio::io::AsyncReadExt::read_to_string(
            &mut ChainAsyncRead::new(
                &mut FakeAsyncRead::new(vec![
                    FakeAsyncReadAction::Data(b"aaa".to_vec()),
                    FakeAsyncReadAction::Pending,
                    FakeAsyncReadAction::Data(b"bbb".to_vec()),
                    FakeAsyncReadAction::Pending,
                ]),
                &mut FakeAsyncRead::new(vec![
                    FakeAsyncReadAction::Pending,
                    FakeAsyncReadAction::EOF,
                    FakeAsyncReadAction::Data(b"ccc".to_vec()),
                ]),
            ),
            &mut dest,
        ))
        .unwrap();
        assert_eq!("aaabbb", dest);
    }

    #[test]
    fn test_both_empty() {
        let mut dest = String::new();
        tokio_test::block_on(tokio::io::AsyncReadExt::read_to_string(
            &mut ChainAsyncRead::new(
                &mut std::io::Cursor::new(b""),
                &mut std::io::Cursor::new(b""),
            ),
            &mut dest,
        ))
        .unwrap();
        assert_eq!("", dest);
    }

    #[test]
    fn test_first_error() {
        let mut a = FakeAsyncRead::new(vec![
            FakeAsyncReadAction::Data(b"aaa".to_vec()),
            FakeAsyncReadAction::Pending,
            FakeAsyncReadAction::Error(std::io::Error::new(std::io::ErrorKind::Other, "err1")),
            FakeAsyncReadAction::Data(b"bbb".to_vec()),
            FakeAsyncReadAction::Pending,
        ]);
        let mut b = std::io::Cursor::new(b"ccc");
        let mut chain = ChainAsyncRead::new(&mut a, &mut b);
        let mut dest = [b'.'; 10];
        let num_read =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap();
        assert_eq!(3, num_read);
        assert_eq!("aaa.......", fixed_buffer::escape_ascii(&dest));
        let err =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap_err();
        assert_eq!(std::io::ErrorKind::Other, err.kind());
        assert_eq!("err1", err.to_string());
        assert_eq!("aaa.......", fixed_buffer::escape_ascii(&dest));
        let num_read =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap();
        assert_eq!(3, num_read);
        assert_eq!("bbb.......", fixed_buffer::escape_ascii(&dest));
    }

    #[test]
    fn test_second_error() {
        let mut a = std::io::Cursor::new(b"aaa");
        let mut b = FakeAsyncRead::new(vec![
            FakeAsyncReadAction::Data(b"ccc".to_vec()),
            FakeAsyncReadAction::Pending,
            FakeAsyncReadAction::Error(std::io::Error::new(std::io::ErrorKind::Other, "err1")),
            FakeAsyncReadAction::Data(b"ddd".to_vec()),
            FakeAsyncReadAction::Pending,
        ]);
        let mut chain = ChainAsyncRead::new(&mut a, &mut b);
        let mut dest = [b'.'; 10];
        let num_read =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap();
        assert_eq!(3, num_read);
        assert_eq!("aaa.......", fixed_buffer::escape_ascii(&dest));
        let num_read =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap();
        assert_eq!(3, num_read);
        assert_eq!("ccc.......", fixed_buffer::escape_ascii(&dest));
        let err =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap_err();
        assert_eq!(std::io::ErrorKind::Other, err.kind());
        assert_eq!("err1", err.to_string());
        assert_eq!("ccc.......", fixed_buffer::escape_ascii(&dest));
        let num_read =
            tokio_test::block_on(tokio::io::AsyncReadExt::read(&mut chain, &mut dest)).unwrap();
        assert_eq!(3, num_read);
        assert_eq!("ddd.......", fixed_buffer::escape_ascii(&dest));
    }
}
