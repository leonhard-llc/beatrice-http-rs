#![forbid(unsafe_code)]
use string_wrapper::StringWrapper;

/// A trait for trying to push (append) an item to a fixed-sized data structure.
///
/// It's useful for generating strings using `StringWrapper`.
///
/// ```
/// # pub trait TryPush<T: ?Sized> {
/// fn try_push(&mut self, s: &T) -> Result<(), std::io::Error>;
/// # }
/// ```
pub trait TryPush<T: ?Sized> {
    fn try_push(&mut self, s: &T) -> Result<(), std::io::Error>;
}

impl TryPush<str> for String {
    fn try_push(&mut self, s: &str) -> Result<(), std::io::Error> {
        self.push_str(s);
        Ok(())
    }
}

impl TryPush<char> for String {
    fn try_push(&mut self, c: &char) -> Result<(), std::io::Error> {
        self.push(*c);
        Ok(())
    }
}

fn full() -> std::io::Error {
    std::io::Error::new(std::io::ErrorKind::Other, "full")
}

impl<B: string_wrapper::Buffer> TryPush<str> for StringWrapper<B> {
    fn try_push(&mut self, s: &str) -> Result<(), std::io::Error> {
        self.push_partial_str(s).map_err(|_| full())
    }
}

impl<B: string_wrapper::Buffer> TryPush<char> for StringWrapper<B> {
    fn try_push(&mut self, c: &char) -> Result<(), std::io::Error> {
        self.push(*c).map_err(|_| full())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_string_wrapper() {
        let mut sw = StringWrapper::new([0u8; 4]);
        sw.try_push("abc").unwrap();
        sw.try_push(&'d').unwrap();
        sw.try_push("e").unwrap_err();
        sw.try_push(&'e').unwrap_err();
    }

    #[test]
    fn test_string() {
        let mut s = String::new();
        s.try_push("abc").unwrap();
        s.try_push(&'d').unwrap();
    }
}
