#![forbid(unsafe_code)]
/// Queue backed by a fixed-size buffer.  Safe.
///
/// Create it with [`new`] or [`default`].
///
/// Example:
/// ```
/// use beatrice_http::ArrayQueue;
/// let mut aq1 = ArrayQueue::new(['.'; 4]);
/// let mut aq2: ArrayQueue<char, [char; 4]> = ArrayQueue::default();
/// ```
///
/// I wrote this because the [array-queue](https://crates.io/crates/array-queue) crate
/// crashed my program (SIGILL).

#[derive(Debug, PartialEq)]
pub struct ArrayQueueFullError;

impl std::fmt::Display for ArrayQueueFullError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), core::fmt::Error> {
        write!(f, "ArrayQueue is full")
    }
}

impl std::error::Error for ArrayQueueFullError {}

pub struct ArrayQueue<T, B> {
    buffer: B,
    len: usize,
    phantom: std::marker::PhantomData<T>,
}

impl<T: Copy, B: AsRef<[T]> + AsMut<[T]>> ArrayQueue<T, B> {
    /// Create a queue using the provided buffer array.
    ///
    /// Example:
    /// ```
    /// use beatrice_http::ArrayQueue;
    /// let mut aq1 = ArrayQueue::new(['.'; 4]);
    /// ```
    ///
    /// See: [`default`]
    pub fn new(buffer: B) -> ArrayQueue<T, B> {
        Self {
            buffer,
            len: 0,
            phantom: std::marker::PhantomData,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    pub fn is_full(&self) -> bool {
        self.len == self.buffer.as_ref().len()
    }

    pub fn push_back(&mut self, value: T) -> Result<(), ArrayQueueFullError> {
        if self.len >= self.buffer.as_ref().len() {
            Err(ArrayQueueFullError {})
        } else {
            self.buffer.as_mut()[self.len] = value;
            self.len += 1;
            Ok(())
        }
    }

    pub fn push_front(&mut self, value: T) -> Result<(), ArrayQueueFullError> {
        if self.len == self.buffer.as_ref().len() {
            Err(ArrayQueueFullError {})
        } else if self.len == 0 {
            self.buffer.as_mut()[0] = value;
            self.len = 1;
            Ok(())
        } else {
            self.buffer.as_mut().copy_within(0..self.len, 1);
            self.buffer.as_mut()[0] = value;
            self.len += 1;
            Ok(())
        }
    }

    pub fn pop_front(&mut self) -> Option<T> {
        if self.len == 0 {
            None
        } else {
            let item = self.buffer.as_ref()[0];
            self.buffer.as_mut().copy_within(1..self.len, 0);
            self.len -= 1;
            Some(item)
        }
    }

    pub fn pop_back(&mut self) -> Option<T> {
        if self.len == 0 {
            None
        } else {
            self.len -= 1;
            let item = self.buffer.as_ref()[self.len];
            Some(item)
        }
    }
}

impl<T: Default + Copy, B: Default + AsRef<[T]> + AsMut<[T]>> Default for ArrayQueue<T, B> {
    fn default() -> Self {
        Self {
            buffer: B::default(),
            len: 0,
            phantom: std::marker::PhantomData,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_errors() {
        let mut aq = ArrayQueue::new(['.'; 1]);
        aq.push_front('a').unwrap();
        let result = aq.push_back('x');
        assert_eq!(Err(ArrayQueueFullError), result);
        let error = result.unwrap_err();
        assert_eq!("ArrayQueue is full", format!("{}", error));
        assert_eq!("ArrayQueue is full", error.to_string());
        assert_eq!("ArrayQueueFullError", format!("{:?}", error));
    }

    #[test]
    fn test_new() {
        let mut aq = ArrayQueue::new(['.'; 2]);
        aq.push_front('a').unwrap();
        assert_eq!(Some('a'), aq.pop_front());
    }

    #[test]
    fn test_default() {
        let mut aq: ArrayQueue<char, [char; 2]> = ArrayQueue::default();
        aq.push_front('a').unwrap();
        assert_eq!(Some('a'), aq.pop_front());
    }

    #[test]
    fn test_empty() {
        let mut aq: ArrayQueue<char, [char; 4]> = ArrayQueue::default();
        assert_eq!(None, aq.pop_front());
        assert_eq!(None, aq.pop_front());
        assert_eq!(true, aq.is_empty());
        assert_eq!(false, aq.is_full());
    }

    #[test]
    fn test_zero_len_buffer() {
        let mut aq: ArrayQueue<char, [char; 0]> = ArrayQueue::default();
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('e'));
        assert_eq!(None, aq.pop_front());
        assert_eq!(None, aq.pop_front());
        assert_eq!(true, aq.is_empty());
        assert_eq!(true, aq.is_full());
    }

    #[test]
    fn test_is_empty() {
        let mut aq: ArrayQueue<char, [char; 2]> = ArrayQueue::default();
        assert_eq!(true, aq.is_empty());
        aq.push_back('a').unwrap();
        assert_eq!(false, aq.is_empty());
        aq.push_back('b').unwrap();
        assert_eq!(false, aq.is_empty());
    }

    #[test]
    fn test_is_full() {
        let mut aq: ArrayQueue<char, [char; 2]> = ArrayQueue::default();
        assert_eq!(false, aq.is_full());
        aq.push_back('a').unwrap();
        assert_eq!(false, aq.is_full());
        aq.push_back('b').unwrap();
        assert_eq!(true, aq.is_full());
    }

    #[test]
    fn test_fifo() {
        let mut aq: ArrayQueue<char, [char; 4]> = ArrayQueue::default();
        aq.push_back('a').unwrap();
        aq.push_back('b').unwrap();
        aq.push_back('c').unwrap();
        aq.push_back('d').unwrap();
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('e'));
        assert_eq!(Some('a'), aq.pop_front());
        aq.push_back('e').unwrap();
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('f'));
        assert_eq!(Some('b'), aq.pop_front());
        assert_eq!(Some('c'), aq.pop_front());
        assert_eq!(Some('d'), aq.pop_front());
        assert_eq!(Some('e'), aq.pop_front());
        assert_eq!(None, aq.pop_front());
        assert_eq!(None, aq.pop_front());
    }

    #[test]
    fn test_lifo_front() {
        let mut aq: ArrayQueue<char, [char; 4]> = ArrayQueue::default();
        aq.push_front('a').unwrap(); // a
        aq.push_front('b').unwrap(); // ba
        aq.push_front('c').unwrap(); // cba
        aq.push_front('d').unwrap(); // dcba
        assert_eq!(Err(ArrayQueueFullError), aq.push_front('e'));
        assert_eq!(Some('d'), aq.pop_front()); // cba
        aq.push_front('e').unwrap(); // ecba
        assert_eq!(Err(ArrayQueueFullError), aq.push_front('f'));
        assert_eq!(Some('e'), aq.pop_front()); // cba
        assert_eq!(Some('c'), aq.pop_front()); // ba
        assert_eq!(Some('b'), aq.pop_front()); // b
        assert_eq!(Some('a'), aq.pop_front()); // []
        assert_eq!(None, aq.pop_front());
        assert_eq!(None, aq.pop_front());
    }

    #[test]
    fn test_lifo_back() {
        let mut aq: ArrayQueue<char, [char; 4]> = ArrayQueue::default();
        aq.push_back('a').unwrap(); // a
        aq.push_back('b').unwrap(); // ba
        aq.push_back('c').unwrap(); // cba
        aq.push_back('d').unwrap(); // dcba
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('e'));
        assert_eq!(Some('d'), aq.pop_back()); // cba
        aq.push_back('e').unwrap(); // ecba
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('f'));
        assert_eq!(Some('e'), aq.pop_back()); // cba
        assert_eq!(Some('c'), aq.pop_back()); // ba
        assert_eq!(Some('b'), aq.pop_back()); // b
        assert_eq!(Some('a'), aq.pop_back()); // []
        assert_eq!(None, aq.pop_back());
        assert_eq!(None, aq.pop_back());
    }

    #[test]
    fn test_all() {
        let mut aq: ArrayQueue<char, [char; 4]> = ArrayQueue::default();
        aq.push_front('b').unwrap(); // b
        aq.push_back('c').unwrap(); // bc
        aq.push_front('a').unwrap(); // abc
        aq.push_back('d').unwrap(); // abcd
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('x'));
        assert_eq!(Some('a'), aq.pop_front()); // bcd
        aq.push_back('e').unwrap(); // bcde
        assert_eq!(Err(ArrayQueueFullError), aq.push_back('x'));
        assert_eq!(Some('e'), aq.pop_back()); // bcd
        assert_eq!(Some('b'), aq.pop_front()); // cd
        assert_eq!(Some('d'), aq.pop_back()); // c
        assert_eq!(Some('c'), aq.pop_front()); // []
        assert_eq!(None, aq.pop_back());
        assert_eq!(None, aq.pop_front());
    }
}
