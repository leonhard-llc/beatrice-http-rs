#![forbid(unsafe_code)]
use crate::{HttpError, ParsingError};
use once_cell::sync::Lazy;
use regex::Regex;
use string_wrapper::StringWrapper;

/// An upper-case string starting with an English letter
/// and containing only English letters and digits.
/// Length is 1-16 bytes.
///
/// Examples: "GET", "HEAD", "CUSTOM123"
#[derive(Clone, Debug, PartialEq)]
pub enum HttpMethod {
    DELETE,
    GET,
    HEAD,
    POST,
    PUT,
    Other(string_wrapper::StringWrapper<[u8; 16]>),
}

impl HttpMethod {
    pub fn new(s: &str) -> Result<HttpMethod, HttpError> {
        // HTTP/1.1 Request Methods https://tools.ietf.org/html/rfc7231#section-4
        static METHOD_RE: Lazy<Regex> = Lazy::new(|| Regex::new("^[A-Z][A-Z0-9]*$").unwrap());
        if !METHOD_RE.is_match(s) {
            return Err(HttpError::Parsing(ParsingError::MethodInvalid));
        }
        match s {
            "DELETE" => Ok(HttpMethod::DELETE),
            "GET" => Ok(HttpMethod::GET),
            "HEAD" => Ok(HttpMethod::HEAD),
            "POST" => Ok(HttpMethod::POST),
            "PUT" => Ok(HttpMethod::PUT),
            s => StringWrapper::from_str_safe(s)
                .map(HttpMethod::Other)
                .ok_or(HttpError::Parsing(ParsingError::MethodTooLong)),
        }
    }

    pub fn as_str(&self) -> &str {
        match self {
            HttpMethod::DELETE => "DELETE",
            HttpMethod::GET => "GET",
            HttpMethod::HEAD => "HEAD",
            HttpMethod::POST => "POST",
            HttpMethod::PUT => "PUT",
            HttpMethod::Other(sw) => &sw,
        }
    }
}

impl std::fmt::Display for HttpMethod {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        assert_eq!(HttpMethod::DELETE, HttpMethod::new("DELETE").unwrap());
        assert_eq!(HttpMethod::GET, HttpMethod::new("GET").unwrap());
        assert_eq!(HttpMethod::HEAD, HttpMethod::new("HEAD").unwrap());
        assert_eq!(HttpMethod::POST, HttpMethod::new("POST").unwrap());
        assert_eq!(HttpMethod::PUT, HttpMethod::new("PUT").unwrap());
        assert_eq!(
            HttpMethod::Other(StringWrapper::from_str("METHOD1A")),
            HttpMethod::new("METHOD1A").unwrap()
        );
        // Shortest
        assert_eq!(
            HttpMethod::Other(StringWrapper::from_str("M")),
            HttpMethod::new("M").unwrap()
        );
        // Longest
        assert_eq!(
            HttpMethod::Other(StringWrapper::from_str("AAAABBBBCCCCDDDD")),
            HttpMethod::new("AAAABBBBCCCCDDDD").unwrap()
        );
        // Too long
        assert_eq!(
            HttpError::Parsing(ParsingError::MethodTooLong),
            HttpMethod::new("AAAABBBBCCCCDDDDE").unwrap_err()
        );
        // Lower-case chars not allowed
        assert_eq!(
            HttpError::Parsing(ParsingError::MethodInvalid),
            HttpMethod::new("invalid").unwrap_err()
        );
        // Non-alpha-numeric chars not allowed
        assert_eq!(
            HttpError::Parsing(ParsingError::MethodInvalid),
            HttpMethod::new("RPC1.2").unwrap_err()
        );
    }

    #[test]
    fn test_as_str() {
        assert_eq!("DELETE", HttpMethod::DELETE.as_str());
        assert_eq!("GET", HttpMethod::GET.as_str());
        assert_eq!("HEAD", HttpMethod::HEAD.as_str());
        assert_eq!("POST", HttpMethod::POST.as_str());
        assert_eq!("PUT", HttpMethod::PUT.as_str());
        let method = HttpMethod::Other(StringWrapper::from_str("METHOD1"));
        assert_eq!("METHOD1", method.as_str());
    }

    #[test]
    fn test_display() {
        assert_eq!("DELETE", format!("{}", HttpMethod::DELETE));
        assert_eq!("GET", format!("{}", HttpMethod::GET));
        assert_eq!("HEAD", format!("{}", HttpMethod::HEAD));
        assert_eq!("POST", format!("{}", HttpMethod::POST));
        assert_eq!("PUT", format!("{}", HttpMethod::PUT));
        let method = HttpMethod::Other(StringWrapper::from_str("METHOD1"));
        assert_eq!("METHOD1", format!("{}", method));
    }

    #[test]
    fn test_derived() {
        // Clone
        let _ = HttpMethod::GET.clone();
        // Debug
        assert_eq!("GET", format!("{:?}", HttpMethod::GET));
        let method = HttpMethod::Other(StringWrapper::from_str("METHOD1"));
        assert_eq!("Other(\"METHOD1\")", format!("{:?}", method));
        // PartialEq
        assert_ne!(HttpMethod::GET, HttpMethod::PUT);
    }
}
