#![forbid(unsafe_code)]
use beatrice_http::{
    id_task_scope, install_logging_tester_logger, Conn, HttpError, HttpMethod, HttpReaderWriter,
    HttpRequest, HttpStatus, StopperController,
};
use fixed_buffer::{escape_ascii, FixedBuf};

async fn handle_request(req: &HttpRequest, rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    match &*req.path {
        "/empty" => {
            req.check_method(HttpMethod::GET)?;
            rw.send(HttpStatus::Ok200).await
        }
        "/return400" => {
            req.check_method(HttpMethod::GET)?;
            Err(HttpError::Handling(HttpStatus::BadRequest400))
        }
        "/send400" => {
            req.check_method(HttpMethod::GET)?;
            rw.send(HttpStatus::BadRequest400).await
        }
        "/return500" => Err(HttpError::Handling(HttpStatus::InternalServerError500(
            "err1".to_string(),
        ))),
        "/send500" => {
            rw.send(HttpStatus::InternalServerError500("err1".to_string()))
                .await
        }
        "/small-response-payload" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_text(HttpStatus::Ok200, "body1").await
        }
        "/large-response-payload" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_text(HttpStatus::Ok200, &String::from("a").repeat(1024 * 1024))
                .await
        }
        "/echo-payload" => {
            req.check_method(HttpMethod::POST)?;
            let mut body = String::new();
            tokio::io::AsyncReadExt::read_to_string(rw, &mut body)
                .await
                .map_err(HttpError::from_io_err)?;
            rw.send_text(HttpStatus::Ok200, &body).await
        }
        "/put-payload" => {
            req.check_method(HttpMethod::PUT)?;
            let mut body = String::new();
            tokio::io::AsyncReadExt::read_to_string(rw, &mut body)
                .await
                .map_err(HttpError::from_io_err)?;
            rw.send(HttpStatus::Created201).await
        }
        "/put-payload-incomplete-read-201" => {
            req.check_method(HttpMethod::PUT)?;
            let mut buf = [0u8; 5];
            tokio::io::AsyncReadExt::read_exact(rw, &mut buf)
                .await
                .map_err(HttpError::from_io_err)?;
            rw.send(HttpStatus::Created201).await
        }
        "/put-payload-incomplete-read-send-400" => {
            req.check_method(HttpMethod::PUT)?;
            let mut buf = [0u8; 5];
            tokio::io::AsyncReadExt::read_exact(rw, &mut buf)
                .await
                .map_err(HttpError::from_io_err)?;
            rw.send(HttpStatus::BadRequest400).await
        }
        "/put-payload-incomplete-read-return-400" => {
            req.check_method(HttpMethod::PUT)?;
            let mut buf = [0u8; 5];
            tokio::io::AsyncReadExt::read_exact(rw, &mut buf)
                .await
                .map_err(HttpError::from_io_err)?;
            Err(HttpError::Handling(HttpStatus::BadRequest400))
        }
        "/download-stream-quick-short" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_and_start_streaming(HttpStatus::Ok200).await?;
            tokio::io::AsyncWriteExt::write_all(rw, b"abcdef")
                .await
                .map_err(HttpError::from_io_err)?;
            rw.finish_streaming().await
        }
        "/download-stream-slow" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_and_start_streaming(HttpStatus::Ok200).await?;
            for n in 0..5 {
                let mut buf = FixedBuf::new([0u8; 10]);
                buf.write_str(&n.to_string()).unwrap();
                buf.write_str(" ").unwrap();
                tokio::io::AsyncWriteExt::write_all(rw, buf.readable())
                    .await
                    .map_err(HttpError::from_io_err)?;
                tokio::time::sleep(tokio::time::Duration::from_millis(50)).await;
            }
            rw.finish_streaming().await
        }
        "/download-stream-unclosed-send-500" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_and_start_streaming(HttpStatus::Ok200).await?;
            tokio::io::AsyncWriteExt::write_all(rw, b"abcdef")
                .await
                .map_err(HttpError::from_io_err)?;
            rw.send(HttpStatus::InternalServerError500("err1".to_string()))
                .await
        }
        "/download-stream-unclosed-return-500" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_and_start_streaming(HttpStatus::Ok200).await?;
            tokio::io::AsyncWriteExt::write_all(rw, b"abcdef")
                .await
                .map_err(HttpError::from_io_err)?;
            Err(HttpError::Handling(HttpStatus::InternalServerError500(
                "err1".to_string(),
            )))
        }
        "/echo-stream" => {
            req.check_method(HttpMethod::GET)?;
            rw.send_and_start_streaming(HttpStatus::Ok200).await?;
            loop {
                let mut buf = FixedBuf::new([0u8; 10]);
                let num_read = tokio::io::AsyncReadExt::read(rw, buf.writable().unwrap())
                    .await
                    .map_err(HttpError::from_io_err)?;
                if num_read == 0 {
                    // EOF
                    break;
                }
                buf.wrote(num_read);
                tokio::io::AsyncWriteExt::write_all(rw, buf.readable())
                    .await
                    .map_err(HttpError::from_io_err)?;
            }
            rw.finish_streaming().await
        }
        // TODO(mleonhard) Add upoad-stream* handlers.
        _ => Err(HttpError::Handling(HttpStatus::NotFound404)),
    }
}

async fn start_server() -> (reqwest::Url, StopperController) {
    // logsley::configure_for_test("debug,beatrice_http=trace", logsley::OutputFormat::Full).unwrap();
    // logsley::configure_for_test("debug,beatrice_http=trace,beatrice_http::headers=info,beatrice_http::body=info,beatrice_http::wire=info", logsley::OutputFormat::Full).unwrap();
    // logsley::configure_for_test("info").unwrap();
    install_logging_tester_logger();
    let (mut accepter, stopper_ctl) = beatrice_http::listen("127.0.0.1:0").await.unwrap();
    let addr = accepter.local_addr().unwrap();
    tokio::spawn(async move {
        while let Some((tcp_stream, addr, stopper, now)) = accepter.next().await {
            tokio::spawn(async move {
                let mut rw = HttpReaderWriter::new(stopper, Conn::Tcp(tcp_stream), addr, now);
                while rw.ready_to_read_request() {
                    id_task_scope(async {
                        if let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                            let result = handle_request(&req, &mut rw).await;
                            beatrice_http::log_and_finish(&req, &mut rw, result).await;
                        }
                    })
                    .await;
                }
            });
        }
    });
    (
        reqwest::Url::parse(&format!("http://{}", addr)).unwrap(),
        stopper_ctl,
    )
}

#[tokio::test]
async fn test_empty() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/empty").unwrap()).await.unwrap();
    assert_eq!(200, response.status());
    assert_eq!("", response.text().await.unwrap());
}

#[tokio::test]
async fn test_return400() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/return400").unwrap()).await.unwrap();
    assert_eq!(400, response.status());
    assert_eq!("", response.text().await.unwrap());
}

#[tokio::test]
async fn test_send400() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/send400").unwrap()).await.unwrap();
    assert_eq!(400, response.status());
    assert_eq!("", response.text().await.unwrap());
}

#[tokio::test]
async fn test_return500() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/return500").unwrap()).await.unwrap();
    assert_eq!(500, response.status());
    assert_eq!("", response.text().await.unwrap());
}

#[tokio::test]
async fn test_send500() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/send500").unwrap()).await.unwrap();
    assert_eq!(500, response.status());
    assert_eq!("", response.text().await.unwrap());
}

#[tokio::test]
async fn test_small_response_payload() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/small-response-payload").unwrap())
        .await
        .unwrap();
    assert_eq!(200, response.status());
    assert_eq!("body1", response.text().await.unwrap());
}

#[tokio::test]
async fn test_large_response_payload() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::get(url.join("/large-response-payload").unwrap())
        .await
        .unwrap();
    assert_eq!(200, response.status());
    let expected_body = String::from("a").repeat(1024 * 1024);
    assert_eq!(expected_body, response.text().await.unwrap());
}

#[tokio::test]
async fn test_echo_payload() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::Client::new()
        .post(url.join("/echo-payload").unwrap())
        .body("body1")
        .send()
        .await
        .unwrap();
    assert_eq!(200, response.status());
    assert_eq!("body1", response.text().await.unwrap());
}

#[tokio::test]
async fn test_put_payload() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::Client::new()
        .put(url.join("/put-payload").unwrap())
        .body(String::from("a").repeat(1024 * 1024))
        .send()
        .await
        .unwrap();
    assert_eq!(201, response.status());
    assert_eq!("", response.text().await.unwrap());
}

// TODO(mleonharD) Add test_put_payload_truncated.

#[tokio::test]
async fn test_put_payload_incomplete_read_201() {
    let (url, _stopper_ctl) = start_server().await;
    // There's a race between the client receiving the response
    // and writing to the closed connection.
    match reqwest::Client::new()
        .put(url.join("/put-payload-incomplete-read-201").unwrap())
        .body(String::from("a").repeat(1024 * 1024))
        .send()
        .await
    {
        Ok(response) => {
            assert_eq!(201, response.status());
            assert_eq!("", response.text().await.unwrap());
        }
        Err(e) => {
            let err_string = format!("{:?}", e);
            assert!(err_string.contains("BodyWrite"));
            assert!(err_string.contains("BrokenPipe"));
        }
    }
}

#[tokio::test]
async fn test_put_payload_incomplete_read_send_400() {
    let (url, _stopper_ctl) = start_server().await;
    match reqwest::Client::new()
        .put(url.join("/put-payload-incomplete-read-send-400").unwrap())
        .body(String::from("a").repeat(1024 * 1024))
        .send()
        .await
    {
        Ok(response) => {
            assert_eq!(400, response.status());
            assert_eq!("", response.text().await.unwrap());
        }
        Err(e) => {
            let err_string = format!("{:?}", e);
            assert!(err_string.contains("BodyWrite"));
            assert!(err_string.contains("BrokenPipe"));
        }
    }
}

#[tokio::test]
async fn test_put_payload_incomplete_read_return_400() {
    let (url, _stopper_ctl) = start_server().await;
    match reqwest::Client::new()
        .put(url.join("/put-payload-incomplete-read-return-400").unwrap())
        .body(String::from("a").repeat(1024 * 1024))
        .send()
        .await
    {
        Ok(response) => {
            assert_eq!(400, response.status());
            assert_eq!("", response.text().await.unwrap());
        }
        Err(e) => {
            let err_string = format!("{:?}", e);
            assert!(err_string.contains("BodyWrite"));
            assert!(err_string.contains("BrokenPipe"));
        }
    }
}

#[tokio::test]
async fn test_download_stream_quick_short() {
    let (url, _stopper_ctl) = start_server().await;
    let response = reqwest::Client::new()
        .get(url.join("/download-stream-quick-short").unwrap())
        .send()
        .await
        .unwrap();
    assert_eq!(200, response.status());
    assert_eq!("abcdef", response.text().await.unwrap());
}

#[tokio::test]
async fn test_download_stream_slow() {
    // slog_scope_futures::SlogScope::new(
    //     slog_scope::logger().new(slog::o!("test" => "test_download_stream_slow")),
    //     async move {
    let (url, _stopper_ctl) = start_server().await;
    log::info!("test_download_stream_slow starting");
    let mut response = reqwest::Client::new()
        .get(url.join("/download-stream-slow").unwrap())
        .send()
        .await
        .unwrap();
    assert_eq!(200, response.status());
    for n in 0..5u8 {
        assert_eq!(
            format!("{} ", n),
            escape_ascii(&response.chunk().await.unwrap().unwrap())
        );
    }
}
