#![forbid(unsafe_code)]
// Example server which loads its certificate chain and private key from files.
use beatrice_http::{
    arbitrary_dns_name, read_cert_chain_pem_file, Conn, HttpError, HttpMethod, HttpReaderWriter,
    HttpRequest, HttpStatus, PinnedServerCertVerifier,
};
use fixed_buffer::escape_ascii;
use log::trace;
use std::sync::Arc;

async fn handle_request(req: &HttpRequest, rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    req.check_path("/path1")?;
    req.check_method(HttpMethod::GET)?;
    rw.send_text(HttpStatus::Ok200, "body1\n").await
}

//noinspection DuplicatedCode
async fn async_main() {
    // Start server task.
    let (mut accepter, mut stopper_ctl, tls_config) =
        beatrice_http::listen_tls_any_client("127.0.0.1:1690", "localhost.crt", "localhost.key")
            .await;
    tokio::spawn(async move {
        while let Some((tcp_stream, addr, stopper, now)) = accepter.next().await {
            let cfg = tls_config.clone();
            tokio::spawn(async move {
                if let Ok(tls_stream) = beatrice_http::tls_handshake(tcp_stream, cfg).await {
                    let mut rw = HttpReaderWriter::new(stopper, Conn::Tls(tls_stream), addr, now);
                    while let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                        let result = handle_request(&req, &mut rw).await;
                        beatrice_http::log_and_finish(&req, &mut rw, result).await;
                    }
                }
            });
        }
    });

    // Make a request to the server.
    let tcp_stream = tokio::net::TcpStream::connect("127.0.0.1:1690")
        .await
        .unwrap();
    let cert = read_cert_chain_pem_file("localhost.crt")
        .await
        .unwrap()
        .remove(0);
    let mut client_config = rustls::ClientConfig::new();
    client_config
        .dangerous()
        .set_certificate_verifier(Arc::new(PinnedServerCertVerifier::new(vec![cert])));
    let tls_connector = tokio_rustls::TlsConnector::from(Arc::new(client_config));
    let mut tls_stream = tls_connector
        .connect(arbitrary_dns_name().as_ref(), tcp_stream)
        .await
        .unwrap();
    tokio::io::AsyncWriteExt::write_all(&mut tls_stream, b"GET /path1 HTTP/1.1\r\n\r\n")
        .await
        .unwrap();
    let mut expected = &b"HTTP/1.1 200 OK\r\ncontent-type: text/plain; charset=UTF-8\r\ncontent-length: 6\r\n\r\nbody1\n"[..];
    loop {
        let mut buf = [0u8; 100];
        let num_read = tokio::io::AsyncReadExt::read(&mut tls_stream, &mut buf)
            .await
            .unwrap();
        trace!("test read {:?}", escape_ascii(&buf[..num_read]));
        assert_eq!(
            escape_ascii(&expected[..num_read.min(expected.len())]),
            escape_ascii(&buf[..num_read])
        );
        expected = &expected[num_read..];
        if expected.is_empty() {
            break;
        }
    }
    println!("Success");

    // // Fails with Error { code: -67843, message: "The certificate was not trusted." }
    // let url = reqwest::Url::parse("https://localhost:1690").unwrap();
    // let client = reqwest::ClientBuilder::new()
    //     .connection_verbose(true)
    //     .add_root_certificate(
    //         reqwest::Certificate::from_pem(
    //             read_file_to_string("localhost.crt")
    //                 .await
    //                 .unwrap()
    //                 .as_bytes(),
    //         )
    //         .unwrap(),
    //     )
    //     .build()
    //     .unwrap();
    // let response = client
    //     .get(url.join("/path1").unwrap())
    //     .send()
    //     .await
    //     .unwrap();
    // assert_eq!(200, response.status());
    // let body = response.text().await.unwrap();
    // assert_eq!("body1\n", body);

    beatrice_http::wait_for_shutdown_signal().await;
    stopper_ctl.graceful_shutdown().await;
}

//noinspection DuplicatedCode
pub fn main() {
    let _guard = logsley::configure("info");
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    runtime.block_on(async_main());
    runtime.shutdown_background();
}

// Make certificate and key files:
// $ cat localhost.openssl.cfg
// [req]
// distinguished_name=dn
// x509_extensions=ext
// [ dn ]
// CN=localhost
// [ ext ]
// subjectAltName = @alt_names
// [alt_names]
// DNS.1 = localhost
// IP.1 = 127.0.0.1
// IP.2 = ::1
// $ openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 \
//  -out localhost.crt -keyout localhost.key -subj '/CN=localhost' \
//  -config localhost.openssl.cfg
// Generating a 2048 bit RSA private key
// ....................................+++
// ..........................................+++
// writing new private key to 'localhost.key'
// -----

// In one terminal:
// $ DEV_LOG_FORMAT=plain cargo run --package beatrice_http --example server_cert
// 2020-11-24T21:40:10.125-08:00 INFO listening on 127.0.0.1:1690
// 2020-11-24T21:40:10.131-08:00 INFO 200 "ok"
// Success
// 2020-11-24T21:40:15.803-08:00 INFO 200 "ok"
// ^C

// In another terminal:
// $ curl -v --cacert localhost.crt https://localhost:1690/path1
// *   Trying ::1...
// * TCP_NODELAY set
// * Connection failed
// * connect to ::1 port 1690 failed: Connection refused
// *   Trying 127.0.0.1...
// * TCP_NODELAY set
// * Connected to localhost (127.0.0.1) port 1690 (#0)
// * ALPN, offering h2
// * ALPN, offering http/1.1
// * successfully set certificate verify locations:
// *   CAfile: localhost.crt
//   CApath: none
// * TLSv1.2 (OUT), TLS handshake, Client hello (1):
// * TLSv1.2 (IN), TLS handshake, Server hello (2):
// * TLSv1.2 (IN), TLS handshake, Certificate (11):
// * TLSv1.2 (IN), TLS handshake, Server key exchange (12):
// * TLSv1.2 (IN), TLS handshake, Server finished (14):
// * TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
// * TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
// * TLSv1.2 (OUT), TLS handshake, Finished (20):
// * TLSv1.2 (IN), TLS change cipher, Change cipher spec (1):
// * TLSv1.2 (IN), TLS handshake, Finished (20):
// * SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
// * ALPN, server did not agree to a protocol
// * Server certificate:
// *  subject: CN=localhost
// *  start date: Nov 25 05:39:45 2020 GMT
// *  expire date: Nov 23 05:39:45 2030 GMT
// *  subjectAltName: host "localhost" matched cert's "localhost"
// *  issuer: CN=localhost
// *  SSL certificate verify ok.
// > GET /path1 HTTP/1.1
// > Host: localhost:1690
// > User-Agent: curl/7.64.1
// > Accept: */*
// >
// < HTTP/1.1 200 OK
// < content-type: text/plain; charset=UTF-8
// < content-length: 6
// <
// body1
// * Connection #0 to host localhost left intact
// * Closing connection 0
