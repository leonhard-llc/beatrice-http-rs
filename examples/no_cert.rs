#![forbid(unsafe_code)]
// Example server which generates a self-signed TLS certificate for itself.
use beatrice_http::{Conn, HttpError, HttpMethod, HttpReaderWriter, HttpRequest, HttpStatus};

async fn handle_request(req: &HttpRequest, rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    req.check_path("/path1")?;
    req.check_method(HttpMethod::GET)?;
    rw.send_text(HttpStatus::Ok200, "body1\n").await
}

//noinspection DuplicatedCode
async fn async_main() {
    // Start server task.
    let (mut accepter, mut stopper_ctl, tls_config) =
        beatrice_http::listen_tls_self_signed("127.0.0.1:1690", "localhost").await;
    tokio::spawn(async move {
        while let Some((tcp_stream, addr, stopper, now)) = accepter.next().await {
            let cfg = tls_config.clone();
            tokio::spawn(async move {
                if let Ok(tls_stream) = beatrice_http::tls_handshake(tcp_stream, cfg).await {
                    let mut rw = HttpReaderWriter::new(stopper, Conn::Tls(tls_stream), addr, now);
                    while let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                        let result = handle_request(&req, &mut rw).await;
                        beatrice_http::log_and_finish(&req, &mut rw, result).await;
                    }
                }
            });
        }
    });

    // Make a request to the server.
    let url = reqwest::Url::parse("https://localhost:1690").unwrap();
    let client = reqwest::ClientBuilder::new()
        //.connection_verbose(true)
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();
    let response = client
        .get(url.join("/path1").unwrap())
        .send()
        .await
        .unwrap();
    assert_eq!(200, response.status());
    let body = response.text().await.unwrap();
    assert_eq!("body1\n", body);

    beatrice_http::wait_for_shutdown_signal().await;
    stopper_ctl.graceful_shutdown().await;
}

//noinspection DuplicatedCode
pub fn main() {
    let _guard = logsley::configure("info");
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    runtime.block_on(async_main());
    runtime.shutdown_background();
}

// In one terminal:
// $ DEV_LOG_FORMAT=plain cargo run --package beatrice_http --example no_cert
//     Finished dev [unoptimized + debuginfo] target(s) in 0.13s
//      Running `target/debug/examples/no_cert`
// 2020-11-24T17:59:02.696-08:00 INFO listening on 127.0.0.1:1690
// 2020-11-24T17:59:02.721-08:00 INFO 200 "ok"
// 2020-11-24T17:59:22.768-08:00 INFO 200 "ok"
// ^C

// In another terminal:
// $ $ curl -vk https://127.0.0.1:1690/path1
// *   Trying 127.0.0.1...
// * TCP_NODELAY set
// * Connected to 127.0.0.1 (127.0.0.1) port 1690 (#0)
// * ALPN, offering h2
// * ALPN, offering http/1.1
// * successfully set certificate verify locations:
// *   CAfile: /etc/ssl/cert.pem
//   CApath: none
// * TLSv1.2 (OUT), TLS handshake, Client hello (1):
// * TLSv1.2 (IN), TLS handshake, Server hello (2):
// * TLSv1.2 (IN), TLS handshake, Certificate (11):
// * TLSv1.2 (IN), TLS handshake, Server key exchange (12):
// * TLSv1.2 (IN), TLS handshake, Server finished (14):
// * TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
// * TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
// * TLSv1.2 (OUT), TLS handshake, Finished (20):
// * TLSv1.2 (IN), TLS change cipher, Change cipher spec (1):
// * TLSv1.2 (IN), TLS handshake, Finished (20):
// * SSL connection using TLSv1.2 / ECDHE-ECDSA-AES256-GCM-SHA384
// * ALPN, server did not agree to a protocol
// * Server certificate:
// *  subject: CN=rcgen self signed cert
// *  start date: Jan  1 00:00:00 1975 GMT
// *  expire date: Jan  1 00:00:00 4096 GMT
// *  issuer: CN=rcgen self signed cert
// *  SSL certificate verify result: self signed certificate (18), continuing anyway.
// > GET /path1 HTTP/1.1
// > Host: 127.0.0.1:1690
// > User-Agent: curl/7.64.1
// > Accept: */*
// >
// < HTTP/1.1 200 OK
// < content-type: text/plain; charset=UTF-8
// < content-length: 6
// <
// body1
// * Connection #0 to host 127.0.0.1 left intact
// * Closing connection 0
