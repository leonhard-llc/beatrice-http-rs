#![forbid(unsafe_code)]
//! Example server that receives uploads.
use beatrice_http::{Conn, HttpError, HttpMethod, HttpReaderWriter, HttpRequest, HttpStatus};

async fn handle_request(req: &HttpRequest, rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    req.check_path("/path1")?;
    req.check_method(HttpMethod::PUT)?;
    let mut body = String::new();
    tokio::io::AsyncReadExt::read_to_string(rw, &mut body)
        .await
        .map_err(HttpError::from_io_err)?;
    if body == String::from("a").repeat(1024 * 1024) {
        rw.send(HttpStatus::Ok200).await
    } else {
        rw.send(HttpStatus::BadRequest400).await
    }
}

//noinspection DuplicatedCode
async fn async_main() {
    let (mut accepter, mut stopper_ctl) = beatrice_http::listen("127.0.0.1:1690").await.unwrap();
    tokio::spawn(async move {
        while let Some((tcp_stream, addr, stopper, now)) = accepter.next().await {
            tokio::spawn(async move {
                let mut rw = HttpReaderWriter::new(stopper, Conn::Tcp(tcp_stream), addr, now);
                while let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                    let result = handle_request(&req, &mut rw).await;
                    beatrice_http::log_and_finish(&req, &mut rw, result).await;
                }
            });
        }
    });

    tokio::task::spawn_blocking(|| {
        // TODO(https://github.com/seanmonstar/reqwest/issues/1060) Switch to reqwest once it supports tokio 0.3.
        let response = ureq::put("http://127.0.0.1:1690/path1")
            .timeout(std::time::Duration::from_secs(10))
            .send_bytes(&[b'a'; 1024 * 1024]);
        assert_eq!(200, response.status());
        assert_eq!("", response.into_string().unwrap());
    })
    .await
    .unwrap();

    beatrice_http::wait_for_shutdown_signal().await;
    stopper_ctl.graceful_shutdown().await;
}

//noinspection DuplicatedCode
pub fn main() {
    let _guard = logsley::configure("info");
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    runtime.block_on(async_main());
    runtime.shutdown_background();
}

// $ DEV_LOG_FORMAT=plain \
//   RUST_LOG=beatrice_http=trace,beatrice_http::body::read=info \
//   cargo run --package beatrice_http --example upload
// 2020-11-17T21:23:19.132-08:00 INFO listening on 127.0.0.1:1690
// 2020-11-17T21:23:19.133-08:00 INFO sending request GET http://127.0.0.1:1690/upload
// 2020-11-17T21:23:19.133-08:00 TRCE 127.0.0.1:51132 read "GET /upload HTTP/1.1\\r\\nHost: 127.0.0.1:1690\\r\\nUser-Agent: ureq/1.5.2\\r\\nAccept: */*\\r\\nContent-Length: 1048576"
// 2020-11-17T21:23:19.301-08:00 TRCE 127.0.0.1:51132 write "HTTP/1.1 200 OK\\r\\ncontent-length: 0\\r\\n\\r\\n"
// 2020-11-17T21:23:19.301-08:00 INFO Some(Ok200)
// 2020-11-17T21:23:19.302-08:00 DEBG NotFound{eof with no data read}
// ^C

// In another terminal:
// $ perl -E 'print "a" x (1024*1024)' |curl -X PUT --data-binary @- -v http://127.0.0.1:1690/path1
// *   Trying 127.0.0.1...
// * TCP_NODELAY set
// * Connected to 127.0.0.1 (127.0.0.1) port 1690 (#0)
// > PUT /path1 HTTP/1.1
// > Host: 127.0.0.1:1690
// > User-Agent: curl/7.64.1
// > Accept: */*
// > Content-Length: 1048576
// > Content-Type: application/x-www-form-urlencoded
// > Expect: 100-continue
// >
// < HTTP/1.1 100 Continue
// * We are completely uploaded and fine
// < HTTP/1.1 200 OK
// < content-length: 0
// <
// * Connection #0 to host 127.0.0.1 left intact
// * Closing connection 0
