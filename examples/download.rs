#![forbid(unsafe_code)]
// Example server.
use beatrice_http::{Conn, HttpError, HttpMethod, HttpReaderWriter, HttpRequest, HttpStatus};

async fn handle_request(req: &HttpRequest, rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    req.check_method(HttpMethod::GET)?;
    req.check_path("/path1")?;
    let body = String::from("a").repeat(1024 * 1024);
    rw.send_bytes(HttpStatus::Ok200, body.as_bytes()).await
}

//noinspection DuplicatedCode
async fn async_main() {
    let (mut accepter, mut stopper_ctl) = beatrice_http::listen("127.0.0.1:1690").await.unwrap();
    tokio::spawn(async move {
        while let Some((tcp_stream, addr, stopper, now)) = accepter.next().await {
            tokio::spawn(async move {
                let mut rw = HttpReaderWriter::new(stopper, Conn::Tcp(tcp_stream), addr, now);
                while let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                    let result = handle_request(&req, &mut rw).await;
                    beatrice_http::log_and_finish(&req, &mut rw, result).await;
                }
            });
        }
    });

    tokio::task::spawn_blocking(|| {
        // TODO(https://github.com/seanmonstar/reqwest/issues/1060) Switch to reqwest once it supports tokio 0.3.
        let response = ureq::get("http://127.0.0.1:1690/path1")
            .timeout(std::time::Duration::from_secs(10))
            .call();
        assert_eq!(200, response.status());
        let body = response.into_string().unwrap();
        assert_eq!(1024 * 1024, body.len());
        assert!(body.chars().all(|c| c == 'a'));
    })
    .await
    .unwrap();

    beatrice_http::wait_for_shutdown_signal().await;
    stopper_ctl.graceful_shutdown().await;
}

//noinspection DuplicatedCode
pub fn main() {
    let _guard = logsley::configure("info");
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    runtime.block_on(async_main());
    runtime.shutdown_background();
}

// $ DEV_LOG_FORMAT=plain \
//   RUST_LOG=beatrice_http=trace,beatrice_http::body::write=info \
//   cargo run --package beatrice_http --example download
// 2020-11-17T16:15:14.946-08:00 INFO listening on 127.0.0.1:1690
// 2020-11-17T16:15:14.947-08:00 INFO sending request GET http://127.0.0.1:1690/path1
// 2020-11-17T16:15:14.947-08:00 TRCE 127.0.0.1:50588 read "GET /path1 HTTP/1.1\\r\\nHost: 127.0.0.1:1690\\r\\nUser-Agent: ureq/1.5.2\\r\\nAccept: */*"
// 2020-11-17T16:15:14.949-08:00 TRCE 127.0.0.1:50588 write "HTTP/1.1 200 OK\\r\\ncontent-length: 1048576\\r\\n\\r\\n"
// 2020-11-17T16:15:15.113-08:00 INFO Some(Ok200)
// 2020-11-17T16:15:15.114-08:00 DEBG NotFound{eof with no data read}
// ^C
