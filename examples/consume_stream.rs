#![forbid(unsafe_code)]
//! Example server that receives streaming uploads.
use beatrice_http::{
    Conn, FakeRead, FakeReadAction, HttpError, HttpMethod, HttpReaderWriter, HttpRequest,
    HttpStatus,
};
use fixed_buffer::FixedBuf;
use log::info;

async fn handle_request(req: &HttpRequest, mut rw: &mut HttpReaderWriter) -> Result<(), HttpError> {
    req.check_path("/path1")?;
    req.check_method(HttpMethod::PUT)?;
    let mut buf = FixedBuf::new([0u8; 256]);
    loop {
        let line_bytes = match buf
            .read_delimited(&mut rw, b"\n")
            .await
            .map_err(HttpError::from_io_err)?
        {
            Some(data) => data,
            None => return rw.send(HttpStatus::Ok200).await,
        };
        let line = std::str::from_utf8(line_bytes)
            .map_err(|_| HttpError::Handling(HttpStatus::BadRequest400))?;
        info!("{:?} body {:?}", rw.addr(), line);
    }
}

//noinspection DuplicatedCode
async fn async_main() {
    let (mut accepter, mut stopper_ctl) = beatrice_http::listen("127.0.0.1:1690").await.unwrap();
    tokio::spawn(async move {
        while let Some((tcp_stream, addr, stopper, now)) = accepter.next().await {
            tokio::spawn(async move {
                let mut rw = HttpReaderWriter::new(stopper, Conn::Tcp(tcp_stream), addr, now);
                while let Ok(req) = beatrice_http::log_if_err(rw.read_request().await) {
                    let result = handle_request(&req, &mut rw).await;
                    beatrice_http::log_and_finish(&req, &mut rw, result).await;
                }
            });
        }
    });

    tokio::task::spawn_blocking(|| {
        // TODO(https://github.com/seanmonstar/reqwest/issues/1060) Switch to reqwest once it supports tokio 0.3.
        let response = ureq::put("http://127.0.0.1:1690/path1")
            .timeout(std::time::Duration::from_secs(10))
            .send(FakeRead::new(vec![
                FakeReadAction::Data(b"abc\nd".to_vec()),
                FakeReadAction::SleepMillis(std::time::Duration::from_millis(1000)),
                FakeReadAction::Data(b"ef\n".to_vec()),
                FakeReadAction::SleepMillis(std::time::Duration::from_millis(1000)),
                FakeReadAction::Data(b"ghi\n".to_vec()),
                FakeReadAction::SleepMillis(std::time::Duration::from_millis(1000)),
            ]));
        assert_eq!(200, response.status());
        assert_eq!("", response.into_string().unwrap());
    })
    .await
    .unwrap();

    beatrice_http::wait_for_shutdown_signal().await;
    stopper_ctl.graceful_shutdown().await;
}

//noinspection DuplicatedCode
pub fn main() {
    let _guard = logsley::configure("info");
    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap();
    runtime.block_on(async_main());
    runtime.shutdown_background();
}

// $ DEV_LOG_FORMAT=plain \
//   RUST_LOG=beatrice_http=trace \
//   cargo run --package beatrice_http --example consume_stream
// 2020-11-20T03:06:52.484-08:00 INFO listening on 127.0.0.1:1690
// 2020-11-20T03:06:52.485-08:00 INFO sending request PUT http://127.0.0.1:1690/path1
// 2020-11-20T03:06:52.485-08:00 TRCE Ready -> ReadingHead
// 2020-11-20T03:06:52.485-08:00 TRCE read "PUT /path1 HTTP/1.1\\r\\nHost: 127.0.0.1:1690\\r\\nUser-Agent: ureq/1.5.2\\r\\nAccept: */*\\r\\nTransfer-Encoding: chunked\\r\\n\\r\\n"
// 2020-11-20T03:06:52.485-08:00 TRCE read headers "PUT /path1 HTTP/1.1\\r\\nHost: 127.0.0.1:1690\\r\\nUser-Agent: ureq/1.5.2\\r\\nAccept: */*\\r\\nTransfer-Encoding: chunked"
// 2020-11-20T03:06:52.487-08:00 TRCE ReadingHead -> ReadingChunks
// 2020-11-20T03:06:52.487-08:00 TRCE poll_read ReadingChunks Ready
// 2020-11-20T03:06:55.493-08:00 TRCE poll_read ReadingChunks Ready
// 2020-11-20T03:06:55.493-08:00 TRCE read "c\\r\\nabc\\ndef\\nghi\\n\\r\\n0\\r\\n\\r\\n"
// 2020-11-20T03:06:55.493-08:00 TRCE ReadingChunks -> ReadingChunk(12)
// 2020-11-20T03:06:55.493-08:00 TRCE -> ReadingChunk(0)
// 2020-11-20T03:06:55.493-08:00 TRCE read body "abc\\ndef\\nghi\\n"
// 2020-11-20T03:06:55.493-08:00 INFO 127.0.0.1:63703 body "abc"
// 2020-11-20T03:06:55.493-08:00 INFO 127.0.0.1:63703 body "def"
// 2020-11-20T03:06:55.493-08:00 INFO 127.0.0.1:63703 body "ghi"
// 2020-11-20T03:06:55.493-08:00 TRCE poll_read ReadingChunk(0) Ready
// 2020-11-20T03:06:55.493-08:00 TRCE ReadingChunk(0) -> ReadingChunks
// 2020-11-20T03:06:55.494-08:00 TRCE ReadingChunks -> ReadingChunkedBodyFoot
// 2020-11-20T03:06:55.494-08:00 TRCE ReadingChunkedBodyFoot -> Ready
// 2020-11-20T03:06:55.494-08:00 TRCE Ready -> WritingHeadAndPayload
// 2020-11-20T03:06:55.494-08:00 TRCE write headers "HTTP/1.1 200 OK\\r\\ncontent-length: 0\\r\\n\\r\\n"
// 2020-11-20T03:06:55.494-08:00 TRCE write "HTTP/1.1 200 OK\\r\\ncontent-length: 0\\r\\n\\r\\n"
// 2020-11-20T03:06:55.494-08:00 TRCE WritingHeadAndPayload -> Ready
// 2020-11-20T03:06:55.494-08:00 INFO Some(Ok200)
// 2020-11-20T03:06:55.494-08:00 TRCE Ready -> ReadingHead
// 2020-11-20T03:06:55.494-08:00 TRCE read ""
// 2020-11-20T03:06:55.494-08:00 TRCE Closed
// ^C
