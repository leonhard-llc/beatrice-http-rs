# beatrice-http
This is a Rust library to talk HTTP, with mutual-TLS, on tokio.

This library is for rust HTTP servers deployed to production,
directly on the Internet,
without reverse proxies like nginx or expensive cloud load balancers.

## Features
- Fixed per-client memory usage
- No macros
- tokio AsyncRead and AsyncWrite for message bodies
- Long-running streams, for pub-sub without polling, websockets, etc.
- Easy to learn & use.  Easy to maintain code that uses it.
- Works with Rust `latest`, `beta`, and `nightly`

## Limitations

## Examples
Runnable examples:
- [examples/hello.rs](examples/hello.rs)
- [examples/download.rs](examples/download.rs)
- [examples/upload.rs](examples/upload.rs)
- [examples/produce_stream.rs](examples/produce_stream.rs)
- [examples/consume_stream.rs](examples/consume_stream.rs)
- [examples/no_cert.rs](examples/no_cert.rs)
- [examples/server_cert.rs](examples/server_cert.rs)
- TODO - [examples/client_cert.rs](examples/client_cert.rs)

Read and handle requests from a remote client:
```rust
```

## Documentation
https://docs.rs/beatrice-http

Logs HTTP protocol data at TRACE level in these modules:
- beatrice_http::headers::read
- beatrice_http::headers::write
- beatrice_http::body::read
- beatrice_http::body::write
- beatrice_http::wire::read
- beatrice_http::wire::write

Website for inspecting certificate files: https://lapo.it/asn1js/

## Alternatives
- [tide](https://crates.io/crates/tide)
  - mature, maintained, concise, async-std
  - `async fn` handlers
  - no mTLS
  - Streaming request body is `AsyncRead`.  Nice.
  - Streaming response body is `AsyncRead` returned by handler.  Awkward.
  - file uploads with third-party
    [multipart-async](https://crates.io/crates/multipart-async) crate
- [actix-web](https://crates.io/crates/actix-web)
  - mature, maintained, lots of features, widely used, tokio
  - uses macros heavily
  - `async fn` handlers
  - no mTLS
  - [WebSockets](https://actix.rs/docs/websockets/)
  - [streaming file uploads](https://docs.rs/actix-multipart/0.3.0/actix_multipart/struct.Multipart.html)
  - [Streaming request body](https://docs.rs/actix-web/3.2.0/actix_web/web/struct.Payload.html)
    is `futures::stream::Stream<Bytes,_>`.  Nice.
  - [Streaming response body](https://actix.rs/docs/handlers/)
    is `futures::stream::Stream<Bytes,_>` returned by handler.  Awkward.
- [warp](https://crates.io/crates/warp)
  - mature, maintained, concise, tokio
  - `async fn` handlers
  - uses macros heavily
  - no mTLS
  - [Streaming request bodies](https://docs.rs/warp/0.2.5/warp/filters/body/fn.stream.html)
    bodies are `futures::stream::Stream<bytes::Buf,_>`.  Nice.
  - [Streaming response bodies](https://docs.rs/warp/0.2.5/warp/reply/index.html)
    are `futures::stream::Stream` provided by handler.  Awkward.
  - [buffers file uploads in memory](https://github.com/seanmonstar/warp/issues/323)
- [rocket](https://crates.io/crates/rocket)
  - mature, maintained, tokio, nightly-only
  - uses macros heavily
  - no mTLS
  - `fn` handlers.
    `async fn` handler support is [incomplete](https://github.com/SergioBenitez/Rocket/issues/1065).
  - [Streaming request body](https://api.rocket.rs/v0.4/rocket/data/struct.DataStream.html)
    is blocking `std::io::Read`.
  - [Streaming response body](https://api.rocket.rs/v0.4/rocket/response/struct.ResponseBuilder.html#method.chunked_body)
    is blocking `std::io::Read` returned by handler.  Awkward.  `AsyncWrite` support [may arrive someday](https://github.com/SergioBenitez/Rocket/issues/1066).
  - [no file uploads](https://github.com/SergioBenitez/Rocket/issues/106)
- [hyper](https://crates.io/crates/hyper)
  - mature, maintained, widely used, fast, tokio
  - no mTLS
  - low level library
  - provides no documented DoS protection
  - [Streaming response bodies](https://docs.rs/hyper/0.13.9/hyper/struct.Response.html)
    are `futures::stream::Stream<Bytes,_>`.  Awkward.
- [Are we web yet? - Web Framworks](http://www.arewewebyet.org/topics/frameworks/)

## Release Process
1. Edit `Cargo.toml` and bump version number.
1. Run `./release.sh`

## Changelog
- v0.1.0 - First published version

## TODO
- DONE - basic request & response
- DONE - connection reuse
- DONE - Set up public repo on Gitlab, using CI config from [fixed-buffer](https://gitlab.com/leonhard-llc/fixed-buffer-rs)
- DONE - chunked transfer-encoding
- mutual TLS with rustls
- library and CLI tool to generate self-signed certs, since certainly is broken.  This will probably be a separate crate.
- remove footguns from API
- DoS mitigation
    - per-connection request limits
    - concurrent request limits
    - throughput limits (min & max), adjustable per-request
    - connection limits
    - example custom DoS quick-drop handler
    - other things that nginx does
- more unit tests
- feature tests
- benchmarks
- example client that uses mutual-TLS to connect, using reqwest if possible
- Try to make this crate comply with the [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/).
- Add some documentation tests
  - https://doc.rust-lang.org/rustdoc/documentation-tests.html
  - https://doc.rust-lang.org/stable/rust-by-example/testing/doc_testing.html
- Publish first version to creates.io
- Get a code review from an experienced rustacean
- Support async-std

